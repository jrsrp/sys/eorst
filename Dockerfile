ARG PROD_CONTAINER_REGISTRY
FROM ${PROD_CONTAINER_REGISTRY}/builder_rust:latest as builder

ADD . eorst/
RUN cargo install --locked --path eorst
RUN mv /eorst/target/release/*.* /opt/
# RUN ldd /opt/cargo/bin/eorst | tr -s '[:blank:]' '\n' | grep '^/' | xargs -I % sh -c 'mkdir -p /opt/deps; cp % /opt/deps/;'

# FROM ${PROD_CONTAINER_REGISTRY}/base_ubuntu:latest-gdallatest
# COPY --from=builder  /opt/deps /usr/lib
# COPY --from=builder  /opt/cargo/bin/eorst /opt/bin/eorst
# COPY --from=builder /usr/lib/ /usr/lib/
# COPY --from=builder /usr/lib/x86_64-linux-gnu/ /usr/lib/
# COPY --from=builder /lib/ /lib/
