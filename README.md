 # An earth observation and remote sensig tookit written in rust.

 ## Summary
 This crate offers a library aiming to simplify the writing of raster processing pipelines in rust.

 ## Highlights

  - Parallel read/write.
  - On-the-fly image reprojection.
  - Point raster sampling extraction.
  - Time series analysis.
  - Band math.
  - Basic opencv integrtion
  - Easy to integrate with LigthGBM and XGBoost.


 ## Crate Status

 - Still iterating on and evolving the crate.
   + The crate is continuously developing, and breaking changes are expected
     during evolution from version to version.

 ## Details

 EORST is an open-source library aiming to fill a void in the rust ecosystem, that is the lack of a library
 to simplify writing geospatial processing code. It takes inspiration from well-established python libraries such as rasterio, rios, and dask and implements some
 of them in rust, enabling the programmer to focus on the geospatial process rather than the implementation details while taking advantage of the benefits of
 the rust programming language.

  Despite being in early stages of development eorst already offers a rich set of tools such as simple reading and writing of geospatial data
  on-the-fly projection and alignment of input data, partition input data into small blocks allowing computation on arrays larger than memory concurrently,
  efficient raster point sampling, simple band math, time series analysis and integration with 3rd party libraries like such as opencv to perform computer
  vision task and LightGBM and XGBoost for machine learning. The library is available in the official creates registry <https://crates.io/crates/eorst>.

  The main focus of this library is to support the work of the [JRSRP](https://www.jrsrp.org.au/), but I expect it to be  useful for a wider audience.
  Most of the current functionalities were developed to support the [Spatial Biocondition project](https://www.qld.gov.au/__data/assets/pdf_file/0015/230019/spatial-biocondition-vegetation-condition-map-for-queensland.pdf), developed jointly by Quenslands Remote Sensing Centre
  and the Quensland Herbarium.

  Future work in the short term will be focused on the stabilization of the API and extending the documentation and examples, collaboration is welcome.

 ## How to use:

 ### As a library:

 Just add eotrs to your dependencies in `cargo.toml` like:

 ```toml
 [dependencies]
 eotrs = {git = https://gitlab.com/leohardtke/eotrs"}
 ```

 ### As a cli tool

 If you want to use the comman line interface see [install](standalonde_docs/install/index.html) for instructions.


 - Examples:

 ```bash
 eorst extract data/cemsre_t55jfm_20200614_sub_abam5.tif\
         data/cemsre_t55jfm_20200614_sub_abam5.gpkg\
         id\
         test.csv\
         -b 1,2,3\
         -s 256\
         -n 8
