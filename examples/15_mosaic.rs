extern crate eorst;
use eorst::{
    utils::{init_logger, BlockSize},
    RasterDatasetBuilder,
};

use log::info;

use std::path::PathBuf;

use anyhow::Result;
use chrono::NaiveDate;
#[cfg(feature = "use_rss")]
use rss::{query::ImageQueryBuilder, qvf::Collection, utils::*, DEA};
use stac::ItemCollection;

use std::ops::Deref;
#[tokio::main]
async fn main() -> Result<()> {
    init_logger();

    // Query imagery from DEA (could be apollo, planetary computer or element84)
    // by scene name (could be binding box or polygon)
    // by date range
    // and maximum percentage of clouds
    let query = ImageQueryBuilder::new(
        DEA.deref().clone(),
        Collection::Sentinel2,
        Intersects::Scene(vec!["55jgk", "56jkq"]),
        &["nbart_red", "nbart_green"],
    )
    .start_date(NaiveDate::parse_from_str("20220101", "%Y%m%d")?)
    .end_date(NaiveDate::parse_from_str("20220210", "%Y%m%d").unwrap())
    .cloudcover((Cmp::Less, 5))
    .build();

    // Download the images asyncronically.
    // Could also just get the metadata and read the actuall image block
    // when doing the mosaic.
    let feature_collection: ItemCollection = query
        .get_async(&PathBuf::from("/tmp/mosiac"), 6, None, None)
        .await
        .unwrap();

    // Create a RasterDataset from the images in the query.
    // This is lazy, it only reads the metadata.
    let rds = RasterDatasetBuilder::from_stac_query(&feature_collection)
        .block_size(BlockSize {
            rows: 4098,
            cols: 4098,
        })
        .set_epsg(32755)
        .build();

    info!("Processing: {}", rds);
    // Should output something like:
    //  Dataset shape:    ( times: 4, layers: 1, rows: 30978, cols: 30978 )
    //  Time indices:     0: 2022-01-04 00:04:07 UTC+00:00,  1: 2022-01-14 06:04:22 UTC+00:00,  2: 2022-02-08 18:04:07 UTC+00:00,  3: 2022-02-09 23:54:32 UTC+00:00
    //  Layer indices:   0: nbart_red
    //  block_size:      ( 2048, 2048 )
    //  n_blocks:      256
    //  geo_transform:    GeoTransform { x_ul: 300000.0, x_res: 10.0, x_rot: 0.0, y_ul: 7100020.0, y_rot: 0.0, y_res: -10.0 }
    //  epsg_code:      32756

    // This will create 1 mosaic per date in the raster dataset.
    // In this case 4 COG mosaics of 30978x30978
    let n_cpus = 6;
    rds.mosiac(n_cpus);

    Ok(())
}
