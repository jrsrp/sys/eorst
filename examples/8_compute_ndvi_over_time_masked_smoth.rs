extern crate eorst;

use anyhow::{bail, Result};
// use bacon_sci::interp::spline_free;
use eorst::{
    utils::{init_logger, BlockSize, Dimension, RasterData},
    DataSource, DataSourceBuilder, RasterDatasetBuilder,
};
use glob::glob;
use log::info;
use ndarray::{s, Array1, Array2, Array3, Array4, ArrayView3, Axis, Zip};
use signalo_filters::convolve::savitzky_golay::SavitzkyGolay;
use signalo_filters::convolve::Convolve;
use signalo_filters::signalo_traits::Filter;
use std::path::PathBuf;

fn interpolate(data: Vec<i16>, null_val: usize) -> Result<Vec<i16>> {
    let xs: Vec<usize> = (0..data.len()).collect();
    let mut data = data.to_owned();

    // interpolate doesn't like a 0 at the start or end of the ts, so replace with the first non-null value!
    let l = &data.len() - 1;
    if data[l] == 0 {
        let pos = data.iter().position(|&x| x != null_val as i16);
        match pos {
            Some(p) => data[l] = data[p],
            None => bail!("Not enough data in the time series!"),
        }
    }
    if data[0] == 0 {
        let pos = data.iter().position(|&x| x != null_val as i16);
        match pos {
            Some(p) => data[0] = data[p],
            None => bail!("Not enough data in the time series!"),
        }
    }

    let y_valid: Vec<i16> = xs
        .iter()
        .zip(data.iter())
        .filter(|(_, y)| **y != null_val as i16)
        .map(|(_, y)| *y)
        .collect();
    let x_valid: Vec<usize> = xs
        .iter()
        .zip(data.iter())
        .filter(|(_, y)| **y != null_val as i16)
        .map(|(x, _)| *x)
        .collect();
    let _x_invalid: Vec<usize> = xs
        .iter()
        .zip(data.iter())
        .filter(|(_, y)| **y == null_val as i16)
        .map(|(x, _)| *x)
        .collect();
    let _x: Vec<f32> = x_valid.iter().map(|v| *v as f32).collect();
    let _y: Vec<f32> = y_valid.iter().map(|v| *v as f32).collect();
    // let poly = spline_free(&x, &y, 1.).unwrap();
    // for x in x_invalid {
    //    let y_interp = poly.evaluate(x as f32).unwrap();
    //    data[x as usize] = y_interp as i16
    // }

    Ok(data)
}

fn get_file_with_stage(in_folder: &str, stage: &str) -> Vec<PathBuf> {
    let pattern = format!("{in_folder}/*{stage}*.img");
    let source_files: Result<Vec<_>, _> = glob(&pattern)
        .expect("Failed to read glob pattern")
        .collect();
    let mut source_files = source_files.unwrap();
    // sort by 3rd field in the filename
    source_files.sort_by(|a, b| {
        a.to_str().unwrap().split('_').collect::<Vec<&str>>()[2].cmp({
            let c = b.to_str().unwrap().split('_').collect::<Vec<&str>>()[2];
            c
        })
    });
    source_files
}

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = &reflectance_data
        .slice(s![0_isize, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1_isize, .., ..])
        .mapv(|elem| elem as f32);
    ((nir - red) / (nir + red))
        .mapv(|elem| (elem * 10000.) as i16)
        .to_owned()
        .mapv(|v| v)
}

// For a Array4 with time in the first axis, and red in the 1st index of the second
// axis, and nir in the second index of the second axis, it will compute
// ndvi for each time step.
fn ndvi_over_time(reflectance_data: &Array4<i16>) -> Array3<i16> {
    let t = reflectance_data.shape()[0];
    let r = reflectance_data.shape()[2];
    let c = reflectance_data.shape()[3];

    let mut result: Array3<i16> = Array3::zeros((t, r, c));

    for (idx, ref_time) in reflectance_data.axis_iter(Axis(0)).enumerate() {
        let ref_time = ref_time.to_owned();

        let ndvi_t = ndvi(&ref_time.view());
        result.slice_mut(s![idx, .., ..]).assign(&ndvi_t);
    }
    result
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, mask: &RasterData<u8>, _: Dimension) -> Array2<i16> {
    let nt = data.shape()[0];
    let nr = data.shape()[2];
    let nc = data.shape()[3];

    let mut result: Array2<i16> = Array2::zeros((nr * nc, nt)); // 1 time series per row
    let ndvi = ndvi_over_time(data); // this is still Array3 (t,nr,nc)
    let ndvi_trow = ndvi.t().into_shape((nr * nc, nt)).unwrap();
    let mask_trow = mask.t().into_shape((nr * nc, nt)).unwrap();
    Zip::from(result.rows_mut())
        .and(ndvi_trow.rows())
        .and(mask_trow.rows())
        .for_each(|mut r, ndvi, mask| {
            let mut ts = ndvi.to_vec();
            ts.iter_mut().zip(mask).for_each(|(d, m)| {
                if *m != 1 {
                    *d = 0
                }
            });
            let l = ts.len();
            let ts = interpolate(ts, 0).unwrap_or(vec![0; l]);
            let filter: Convolve<f32, 13> = Convolve::savitzky_golay();
            let input: Vec<f32> = ts.iter().map(|v| *v as f32).collect();
            let ts_sg: Vec<_> = input
                .iter()
                .scan(filter, |filter, &input| Some(filter.filter(input)))
                .collect();
            let ts_final = Array1::from_iter(ts_sg.iter().map(|v| *v as i16));
            r.assign(&ts_final)
        });

    result
}

fn main() {
    init_logger();
    let source_files = get_file_with_stage("data/time_series", "aba");
    // create the sources for the raster dataset
    let data_sources: Vec<DataSource> = source_files
        .iter()
        .map(|s| DataSourceBuilder::from_file(s).bands(vec![3, 4]).build())
        .collect();

    let rds = RasterDatasetBuilder::from_sources(&data_sources)
        .block_size(BlockSize {
            rows: 512,
            cols: 512,
        })
        .build();
    info!("Processing RasterDataset: {rds}");
    let source_files = get_file_with_stage("data/time_series", "ad2");
    // create the sources for the raster dataset
    let data_sources: Vec<DataSource> = source_files
        .iter()
        .map(|s| DataSourceBuilder::from_file(s).bands(vec![1]).build())
        .collect();

    let mask = RasterDatasetBuilder::from_sources(&data_sources)
        .block_size(BlockSize {
            rows: 512,
            cols: 512,
        })
        .build();
    let out_fn = PathBuf::from("8_ndvi_smooth.tif");

    rds.reduce_row_pixel_with_mask::<i16, u8>(&mask, worker, 65536, Dimension::Layer, 4, &out_fn);
}
