use anyhow::Result;

use eorst::utils::BlockSize;
use eorst::DataSourceBuilder;
use eorst::RasterDatasetBuilder;
use std::path::PathBuf;

fn main() -> Result<()> {
    // We test zonal statistics using a small raster with 7 zones (1 to 7)
    // as both the zones and the data. We expect that zone 1 would have all
    // the highest count in the 1-2 bin; zone 2 in the 2-3 bin and so on
    let bs = 1024;
    let n_bins = 10;
    let _n_zones = 7; // the zones on the raster. Not a parameter.

    let data_source = DataSourceBuilder::from_file(&PathBuf::from(
        "data/cemsre_t55jfm_20200614_abam5_zones.tif",
    ))
    .build();
    let rds_1 = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize { rows: bs, cols: bs })
        .build();

    let data_source = DataSourceBuilder::from_file(&PathBuf::from(
        "data/cemsre_t55jfm_20200614_abam5_zones.tif",
    ))
    .build();
    let rds_2 = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize { rows: bs, cols: bs })
        .build();
    let _zonal_histograms = rds_1.zonal_histograms_raster(&rds_2, n_bins, 0., 10.)?;

    Ok(())
}
