extern crate eorst;

use eorst::{
    utils::{BlockSize, Dimension, RasterData},
    DataSourceBuilder, RasterDatasetBuilder,
};
use ndarray::{s, Array2, Array3, ArrayView3, Axis};
use std::path::PathBuf;

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = &reflectance_data
        .slice(s![0_isize, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1_isize, .., ..])
        .mapv(|elem| elem as f32);
    ((nir - red) / (nir + red))
        .mapv(|elem| (elem * 10000.) as i16)
        .to_owned()
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, _: Dimension) -> Array3<i16> {
    let ndvi = ndvi(&data.slice(s![0_usize, .., .., ..]));
    ndvi.insert_axis(Axis(0))
}

fn main() {
    let data_source =
        DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
            .bands(vec![3, 4])
            .build();
    let rds = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize {
            rows: 256,
            cols: 256,
        })
        .build();
    let out_fn = PathBuf::from("4_ndvi.tif");
    rds.reduce::<i16, i16>(worker, Dimension::Layer, 4, &out_fn, 65536);
}
