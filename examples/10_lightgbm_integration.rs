use eorst::{
    utils::{init_logger, BlockSize},
    DataSourceBuilder, RasterDatasetBuilder,
};
use log::info;
use serde_json::json;
use std::path::PathBuf;

fn main() {
    init_logger();
    let training_data = PathBuf::from("data/training.gpkg");
    let data_source = DataSourceBuilder::from_file(&PathBuf::from("data/ts_ndvi.tif")).build();
    let rds = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize {
            rows: 256,
            cols: 256,
        })
        .build();
    info!("{rds}");
    let params = json! {
    {
        "max_depth" : 4,
        "boosting_type": "gbdt",
        "num_iterations": 100,
        "objective": "multiclass",
        "reg_alpha" : 0.25,
        "num_class": 5,
        "num_threads" : 1,
        "learning_rate" : 0.01,
        "min_data_in_leaf" : 10,
        "num_leaves": 20,
        "verbose" : -1,
        }
    };

    rds.lightgbm_fit_classifier(&training_data, params, "class");
    let out_fn = PathBuf::from("prediction.tif");
    rds.lightgbm_predict(&out_fn);
}
