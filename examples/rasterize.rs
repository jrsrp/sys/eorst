use eorst::utils::init_logger;
use eorst::Extent;
use eorst::{DataSourceBuilder, RasterDatasetBuilder};
use gdal::raster::RasterizeOptions;
use log::info;
use ndarray::Array4;
use std::ops::RangeToInclusive;
use std::path::PathBuf;

fn worker<T>(data: &Array4<T>) -> Array4<T>
where
    T: gdal::raster::GdalType + Clone,
{
    data.to_owned()
}

fn main() {
    init_logger();
    info!("Starting rasterize");
    // define the extent of the new raster dataset.
    let xmin = 404190.;
    let xmax = 2299800.;
    let ymin = -3408210.;
    let ymax = -855420.;
    let extent = Extent {
        xmin,
        xmax,
        ymin,
        ymax,
    };
    let resolution = 30.;
    let epsg_code = 3577;
    let rasterize_options = RasterizeOptions {
        all_touched: true,
        source: gdal::raster::BurnSource::UserSupplied,
        merge_algorithm: gdal::raster::MergeAlgorithm::Replace,
        chunk_y_size: 0,
        optimize: gdal::raster::OptimizeMode::Automatic,
    };

    let rds = RasterDatasetBuilder::from_scratch(extent, resolution, epsg_code); //, rasterize_options);
    let out_fn = PathBuf::from("./rasterized.tif");
    let vector_fn = PathBuf::from("/scratch/rsc8/hardtkel/biocondition_inputs/vector/Biodiversity_status_of_pre_clearing_regional_ecosystems_3577.gpkg");
    let column_name = "DBVG1m_id";
    rds.rasterize::<u8>(&vector_fn, column_name, 8, Some(rasterize_options), &out_fn);
}
