use anyhow::Result;
use chrono::NaiveDate;
use eorst::save_zonal_histograms;
use eorst::swap_coordinates;
use eorst::utils::init_logger;
use eorst::utils::BlockSize;

use eorst::RasterDatasetBuilder;
use gdal::spatial_ref::CoordTransform;
use gdal::spatial_ref::SpatialRef;
use gdal::vector::Feature;
use gdal::vector::Geometry;
use gdal::vector::Layer;
use gdal::vector::LayerAccess;
use gdal::Dataset;
#[cfg(feature = "use_rss")]
use rss::{query::ImageQueryBuilder, qvf::Collection, utils::*, DEA};
use stac::ItemCollection;
use std::ops::Deref;
use std::path::PathBuf;
// cargo run --release --example 13_zonal_stat_stac_query_json --features=use_polars --features=use_rss
//

#[tokio::main]
async fn main() -> Result<()> {
    init_logger();

    // Create a raster dataset for the data from a geotiff source.
    // Could be a stac query as well!
    // Open the dataset
    let polygons_fn = "/home/hardtkel/sources/rust/rss_workspace/eorst/examples/example_14.geojson";
    let dataset = Dataset::open(polygons_fn).unwrap();

    // Get the first layer
    let mut layer: Layer = dataset.layer(0).unwrap();

    // Get the features in the layer
    let features: Vec<Feature> = layer.features().collect();

    // Get the geometries from the features
    let geometries: Vec<&Geometry> = features
        .iter()
        .map(|feature| feature.geometry().unwrap())
        .collect();
    let source_srs = SpatialRef::from_epsg(32754).unwrap();
    let target_srs = SpatialRef::from_epsg(4326).unwrap();
    let coord_transform = CoordTransform::new(&source_srs, &target_srs).unwrap();

    let mut projected_geometries = Vec::new();
    for geom in geometries {
        let projected_geometry = geom.transform(&coord_transform).unwrap();
        let projected_geometry = swap_coordinates(&projected_geometry);
        projected_geometries.push(projected_geometry)
    }

    let mut min_x = f64::INFINITY;
    let mut min_y = f64::INFINITY;
    let mut max_x = f64::NEG_INFINITY;
    let mut max_y = f64::NEG_INFINITY;

    for geometry in &projected_geometries {
        let envelope = geometry.envelope();
        min_x = min_x.min(envelope.MinX);
        min_y = min_y.min(envelope.MinY);
        max_x = max_x.max(envelope.MaxX);
        max_y = max_y.max(envelope.MaxY);
    }
    let bounding_box_4326 = Geometry::bbox(min_x, min_y, max_x, max_y).unwrap();
    let bbox = Bbox {
        xmax: max_x as f32,
        xmin: min_x as f32,
        ymax: max_y as f32,
        ymin: min_y as f32,
    };

    let collection = Collection::Sentinel2;
    let intersect = Intersects::Polygon(bounding_box_4326);
    let data_bands = &["nbart_red", "nbart_nir_1"];
    let start_date = NaiveDate::parse_from_str("20210101", "%Y%m%d")?;
    let end_date = NaiveDate::parse_from_str("20210110", "%Y%m%d")?;

    let query = ImageQueryBuilder::new(DEA.deref().clone(), collection, intersect, data_bands)
        .start_date(start_date)
        .end_date(end_date)
        .cloudcover((Cmp::Less, 5))
        .build();

    let feature_collection: ItemCollection = query
        .get_async(&PathBuf::from("/tmp/zs"), 8, Some(bbox), Some("4326"))
        .await
        .expect("Unable to get the queried data");

    let rds_1 = RasterDatasetBuilder::from_stac_query(&feature_collection)
        .block_size(BlockSize {
            rows: 128,
            cols: 128,
        })
        .build();
    println!("Rds: {rds_1}");
    let n_bins = 100;
    let min = 0.;
    let max = 1000.;
    let mut zonal_histograms = rds_1.zonal_histograms_polygons(polygons_fn, n_bins, min, max)?;
    println!("Zonal histograms: \n {:?}", zonal_histograms);
    // // the zonal histogram dataframe can then be saved
    save_zonal_histograms(
        &mut zonal_histograms,
        &PathBuf::from("/tmp/test_histo.parquet"),
    );

    Ok(())
}
