extern crate eorst;

use eorst::{
    utils::{BlockSize, Dimension, ImageResolution},
    DataSourceBuilder, RasterDatasetBuilder,
};
use ndarray::Array4;
use num_traits::FromPrimitive;
use std::{ops::Add, path::PathBuf};

fn worker<T>(data: &Array4<T>) -> Array4<T>
where
    T: gdal::raster::GdalType + FromPrimitive + ndarray::ScalarOperand + Add<Output = T>,
{
    let val = T::from_i16(10).unwrap();
    data + val
}

fn main() {
    let data_source =
        DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
            .build();
    let rds = RasterDatasetBuilder::from_source(&data_source)
        .set_resolution(ImageResolution { x: 45., y: 45. })
        .block_size(BlockSize {
            rows: 256,
            cols: 256,
        })
        .band_composition_dimension(Dimension::Time)
        .build();
    let out_fn = PathBuf::from("3_process_image.tif");
    rds.process::<i16, i16>(worker, 6, &out_fn);
}
