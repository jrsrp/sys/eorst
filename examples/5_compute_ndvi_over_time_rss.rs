extern crate eorst;

use eorst::{
    utils::{init_logger, BlockSize, Dimension, RasterData},
    DataSource, DataSourceBuilder, RasterDatasetBuilder,
};

use ndarray::{s, Array2, Array3, Array4, ArrayView3, Axis};
use std::path::PathBuf;

#[cfg(feature = "use_rss")]
use rss_core::qvf::QvfFilenamesBuilder;

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = &reflectance_data
        .slice(s![0_isize, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1_isize, .., ..])
        .mapv(|elem| elem as f32);
    ((nir - red) / (nir + red))
        .mapv(|elem| (elem * 10000.) as i16)
        .to_owned()
        .mapv(|v| v)
}

// For a Array4 with time in the first axis, and red in the 1st index of the second
// axis, and nir in the second index of the second axis, it will compute
// ndvi for each time step.
fn ndvi_over_time(reflectance_data: &Array4<i16>) -> Array3<i16> {
    let t = reflectance_data.shape()[0];
    let r = reflectance_data.shape()[2];
    let c = reflectance_data.shape()[3];

    let mut result: Array3<i16> = Array3::zeros((t, r, c));
    for (idx, ref_time) in reflectance_data.axis_iter(Axis(0)).enumerate() {
        let ndvi_t = ndvi(&ref_time);
        result.slice_mut(s![idx, .., ..]).assign(&ndvi_t);
    }
    result
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, _: Dimension) -> Array3<i16> {
    ndvi_over_time(data)
}

fn main() {
    init_logger();
    let mut source_files =
        QvfFilenamesBuilder::from_folder(&PathBuf::from("data/time_series/")).build();
    source_files.filter_by_stage("aba");

    // create the sources for the raster dataset
    let data_sources: Vec<DataSource> = source_files
        .qvf_filenames
        .iter()
        .map(|s| {
            DataSourceBuilder::from_file(&s.to_pathbuff())
                .bands(vec![3, 4])
                .build()
        })
        .collect();

    let rds = RasterDatasetBuilder::from_sources(&data_sources)
        .block_size(BlockSize {
            rows: 512,
            cols: 512,
        })
        .build();
    let out_fn = PathBuf::from("5_ndvi_over_time.tif");

    rds.reduce::<i16, i16>(worker, Dimension::Layer, 4, &out_fn, 65536);
}
