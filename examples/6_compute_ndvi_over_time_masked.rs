extern crate eorst;

use eorst::{
    utils::{init_logger, BlockSize, Dimension, RasterData},
    DataSource, DataSourceBuilder, RasterDatasetBuilder,
};
use glob::glob;
use log::info;
use ndarray::{s, Array2, Array3, ArrayView3, Axis, Zip};
use std::path::PathBuf;

fn get_file_with_stage(in_folder: &str, stage: &str) -> Vec<PathBuf> {
    let pattern = format!("{in_folder}/*{stage}*.img");
    let source_files: Result<Vec<_>, _> = glob(&pattern)
        .expect("Failed to read glob pattern")
        .collect();
    let mut source_files = source_files.unwrap();
    // sort by 3rd field in the filename
    source_files.sort_by(|a, b| {
        a.to_str().unwrap().split('_').collect::<Vec<&str>>()[2].cmp({
            let c = b.to_str().unwrap().split('_').collect::<Vec<&str>>()[2];
            c
        })
    });
    source_files
}

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = &reflectance_data
        .slice(s![0_isize, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1_isize, .., ..])
        .mapv(|elem| elem as f32);
    ((nir - red) / (nir + red))
        .mapv(|elem| (elem * 10000.) as i16)
        .to_owned()
        .mapv(|v| v)
}

// For a Array4 with time in the first axis, and red in the 1st index of the second
// axis, and nir in the second index of the second axis, it will compute
// ndvi for each time step.
fn ndvi_over_time(reflectance_data: &RasterData<i16>, mask: &RasterData<u8>) -> Array3<i16> {
    let t = reflectance_data.shape()[0];
    let r = reflectance_data.shape()[2];
    let c = reflectance_data.shape()[3];

    let mut result: Array3<i16> = Array3::zeros((t, r, c));

    Zip::from(result.axis_iter_mut(Axis(0))) //layer, row, col
        .and(reflectance_data.axis_iter(Axis(0))) //layer, row, col
        .and(mask.axis_iter(Axis(0))) //layer, row, col
        .for_each(|mut res, r, m| {
            let mut ndvi = ndvi(&r); //row,col
            let mask = m.slice(s![0_usize, .., ..]); //row,col
            ndvi.iter_mut().zip(mask.iter()).for_each(|(n, m)| {
                if *m != 1 {
                    *n = 0
                }
            });
            res.assign(&ndvi);
        });

    result
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, mask: &RasterData<u8>, _: Dimension) -> Array3<i16> {
    ndvi_over_time(data, mask)
}

fn main() {
    init_logger();
    let source_files = get_file_with_stage("data/time_series", "aba");
    // create the sources for the raster dataset
    let data_sources: Vec<DataSource> = source_files
        .iter()
        .map(|s| DataSourceBuilder::from_file(s).bands(vec![3, 4]).build())
        .collect();

    let rds = RasterDatasetBuilder::from_sources(&data_sources)
        .block_size(BlockSize {
            rows: 512,
            cols: 512,
        })
        .build();
    info!("Processing RasterDataset: {rds}");
    let _out_fn = PathBuf::from("5_ndvi_over_time.tif");
    let source_files = get_file_with_stage("data/time_series", "ad2");
    // create the sources for the raster dataset
    let data_sources: Vec<DataSource> = source_files
        .iter()
        .map(|s| DataSourceBuilder::from_file(s).bands(vec![1]).build())
        .collect();

    let mask = RasterDatasetBuilder::from_sources(&data_sources)
        .block_size(BlockSize {
            rows: 512,
            cols: 512,
        })
        .build();
    let out_fn = PathBuf::from("6_ndvi_over_time_masked.tif");

    rds.reduce_with_mask::<i16, u8>(&mask, worker, Dimension::Layer, 4, &out_fn);
}
