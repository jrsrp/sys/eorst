extern crate eorst;

use eorst::{
    utils::{init_logger, BlockSize, Dimension, RasterData},
    RasterDatasetBuilder,
};

use log::info;
use ndarray::{s, Array2, Array3, Array4, ArrayView3, Axis};
use std::path::PathBuf;

use anyhow::Result;
use chrono::NaiveDate;
#[cfg(feature = "use_rss")]
use rss_core::{query::ImageQueryBuilder, qvf::Collection, utils::*, DEA};
use stac::ItemCollection;
use std::ops::Deref;

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = &reflectance_data
        .slice(s![0, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1, .., ..])
        .mapv(|elem| elem as f32);
    ((nir - red) / (nir + red))
        .mapv(|elem| (elem * 10000.) as i16)
        .to_owned()
        .mapv(|v| v)
}

// For a Array4 with time in the first axis, and red in the 1st index of the second
// axis, and nir in the second index of the second axis, it will compute
// ndvi for each time step.
fn ndvi_over_time(reflectance_data: &Array4<i16>) -> Array3<i16> {
    let t = reflectance_data.shape()[0];
    let r = reflectance_data.shape()[2];
    let c = reflectance_data.shape()[3];

    let mut result: Array3<i16> = Array3::zeros((t, r, c));
    for (idx, ref_time) in reflectance_data.axis_iter(Axis(0)).enumerate() {
        let ndvi_t = ndvi(&ref_time);
        result.slice_mut(s![idx, .., ..]).assign(&ndvi_t);
    }
    result
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, _: Dimension) -> Array3<i16> {
    ndvi_over_time(data)
}
fn main() -> Result<()> {
    init_logger();

    // This will yield 52 items
    let query = ImageQueryBuilder::new(
        DEA.deref().clone(),
        Collection::Sentinel2,
        Intersects::Scene(vec!["55kvu"]),
        &["nbart_red", "nbart_nir_1"],
    )
    .start_date(NaiveDate::parse_from_str("20210101", "%Y%m%d").unwrap())
    .end_date(NaiveDate::parse_from_str("20210111", "%Y%m%d").unwrap())
    .cloudcover((Cmp::Less, 5))
    .build();

    let tmp_dir = PathBuf::from("/scratch/rsc8/hardtkel/tmp2");
    let feature_collection: ItemCollection = query
        .get(&tmp_dir, None, None)
        .expect("Could not download files");

    // produces a 49 bands GeoTiff
    let rds = RasterDatasetBuilder::from_stac_query(&feature_collection)
        .block_size(BlockSize {
            rows: 2048,
            cols: 2048,
        })
        .build();
    info!("Processing: {}", rds);
    let out_fn = PathBuf::from("/scratch/rsc8/hardtkel/tmp/5_ndvi_over_time.vrt");
    rds.reduce::<i16, i16>(worker, Dimension::Layer, 32, &out_fn, 65536);
    // takes about 6 minutes including download if storing as vrt. Does not include overview creation.
    Ok(())
}
