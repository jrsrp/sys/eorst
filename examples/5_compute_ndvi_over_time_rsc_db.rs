extern crate eorst;
use eorst::{
    utils::{init_logger, BlockSize, Dimension, RasterData},
    RasterDatasetBuilder,
};

use ndarray::{s, Array2, Array3, Array4, ArrayView3, Axis};
use std::{collections::HashMap, path::PathBuf};

use anyhow::Result;
use chrono::NaiveDate;
use std::ops::Deref;

#[cfg(feature = "use_rss")]
use rss::{query::ImageQueryBuilder, qvf::Collection, utils::*, APOLLO};

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = &reflectance_data
        .slice(s![0_isize, .., ..])
        .mapv(|elem| elem as f32);
    let nir = &reflectance_data
        .slice(s![1_isize, .., ..])
        .mapv(|elem| elem as f32);
    ((nir - red) / (nir + red))
        .mapv(|elem| (elem * 10000.) as i16)
        .to_owned()
}

// For a Array4 with time in the first axis, and red in the 1st index of the second
// axis, and nir in the second index of the second axis, it will compute
// ndvi for each time step.
fn ndvi_over_time(reflectance_data: &Array4<i16>) -> Array3<i16> {
    let t = reflectance_data.shape()[0];
    let r = reflectance_data.shape()[2];
    let c = reflectance_data.shape()[3];

    let mut result: Array3<i16> = Array3::zeros((t, r, c));
    for (idx, ref_time) in reflectance_data.axis_iter(Axis(0)).enumerate() {
        let ndvi_t = ndvi(&ref_time);
        result.slice_mut(s![idx, .., ..]).assign(&ndvi_t);
    }
    result
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, _: Dimension) -> Array3<i16> {
    ndvi_over_time(data)
}
#[tokio::main]
async fn main() -> Result<()> {
    init_logger();
    // Query metadb. This will return a list of files matching the query parameters
    let query = ImageQueryBuilder::new(
        APOLLO.deref().clone(),
        Collection::Sentinel2,
        Intersects::Scene(vec!["t56jmr", "t56jlr"]),
        &["aba"],
    )
    .start_date(NaiveDate::parse_from_str("20220101", "%Y%m%d").unwrap())
    .end_date(NaiveDate::parse_from_str("20220401", "%Y%m%d").unwrap())
    .landcover((Cmp::Greater, 10))
    .cloudcover((Cmp::Less, 10))
    .build();

    let tmp_dir = PathBuf::from("/lustre/scratch/rsc9/hardtkel/tmp");
    let local_stac = query
        .get(&tmp_dir, None, None)
        .expect("Could not recall files");
    let mut bands = HashMap::new();
    bands.insert("aba".to_string(), vec![3, 4]);

    let rds = RasterDatasetBuilder::from_stac_query(&local_stac)
        .block_size(BlockSize {
            rows: 5125,
            cols: 512,
        })
        .bands(bands)
        .build();
    println!("Will process: \n{}", rds);
    let out_fn = PathBuf::from("/scratch/rsc8/hardtkel/tmp/5_ndvi_over_time.vrt");
    rds.reduce::<i16, i16>(worker, Dimension::Layer, 32, &out_fn, 65536);
    Ok(())
}

#[test]
fn test_this() {
    let x = 10;
}
