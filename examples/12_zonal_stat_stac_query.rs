use anyhow::Result;
use chrono::NaiveDate;
use eorst::utils::init_logger;
use eorst::utils::save_zonal_histograms;
use eorst::utils::BlockSize;
use eorst::DataSourceBuilder;
use eorst::RasterDatasetBuilder;
#[cfg(feature = "use_rss")]
use rss::{query::ImageQueryBuilder, qvf::Collection, utils::*, DEA};
use stac::ItemCollection;
use std::ops::Deref;
use std::path::PathBuf;

// cargo run --release --example 12_zonal_stat_stac_query --features=use_polars --features=use_rss
//
// Data Attributes:
// 	Dataset shape:  	( times: 1, layers: 2, rows: 10980, cols: 10980 )
// 	Time indices: 		0: 2021-02-02 00:32:22 UTC+00:00
// 	Layer indices: 	0: nbart_nir_1, 1: nbart_red
// 	block_size:  		( 2048, 2048 )
// 	n_blocks:  		36
// 	geo_transform:  	GeoTransform { x_ul: 300000.0, x_res: 10.0, x_rot: 0.0, y_ul: 7900000.0, y_rot: 0.0, y_res: -10.0 }
// 	epsg_code:  		32755

// Zones Attributes:
// 	Dataset shape:  	( times: 1, layers: 1, rows: 10980, cols: 10980 )
// 	Time indices: 		0
// 	Layer indices: 	0: Layer_0
// 	block_size:  		( 2048, 2048 )
// 	n_blocks:  		36
// 	geo_transform:  	GeoTransform { x_ul: 300000.0, x_res: 10.0, x_rot: 0.0, y_ul: 7900000.0, y_rot: 0.0, y_res: -10.0 }
// 	epsg_code:  		32755
//
// Zonal histograms:
// shape: (400, 6)
// ┌──────┬──────┬───────┬──────────┬─────────┬───────┐
// │ zone ┆ time ┆ layer ┆ bin_star ┆ bin_end ┆ count │
// │ ---  ┆ ---  ┆ ---   ┆ ---      ┆ ---     ┆ ---   │
// │ i64  ┆ str  ┆ str   ┆ f32      ┆ f32     ┆ i64   │
// ╞══════╪══════╪═══════╪══════════╪═════════╪═══════╡
// │ 1    ┆ 2021 ┆ 02    ┆ 360.0    ┆ 370.0   ┆ 4610  │
// │ 1    ┆ 2021 ┆ 02    ┆ 450.0    ┆ 460.0   ┆ 1850  │
// │ 1    ┆ 2021 ┆ 02    ┆ 570.0    ┆ 580.0   ┆ 29    │
// │ 1    ┆ 2021 ┆ 02    ┆ 650.0    ┆ 660.0   ┆ 16    │
// │ …    ┆ …    ┆ …     ┆ …        ┆ …       ┆ …     │
// │ 3    ┆ 2021 ┆ 02    ┆ 490.0    ┆ 500.0   ┆ 135   │
// │ 3    ┆ 2021 ┆ 02    ┆ 540.0    ┆ 550.0   ┆ 298   │
// │ 3    ┆ 2021 ┆ 02    ┆ 740.0    ┆ 750.0   ┆ 261   │
// │ 3    ┆ 2021 ┆ 02    ┆ 880.0    ┆ 890.0   ┆ 128   │
// └──────┴──────┴───────┴──────────┴─────────┴───────┘

#[tokio::main]
async fn main() -> Result<()> {
    init_logger();

    // Create a raster dataset for the data from a geotiff source.
    // Could be a stac query as well!
    let source = DEA.deref().clone();
    let collection = Collection::Sentinel2;
    let intersect = Intersects::Scene(vec!["55kcu"]);
    let data_bands = &["nbart_red", "nbart_nir_1"];
    let start_date = NaiveDate::parse_from_str("20210101", "%Y%m%d")?;
    let end_date = NaiveDate::parse_from_str("20210301", "%Y%m%d")?;

    let query = ImageQueryBuilder::new(source, collection, intersect, data_bands)
        .start_date(start_date)
        .end_date(end_date)
        .cloudcover((Cmp::Less, 5))
        .build();
    let feature_collection: ItemCollection = query
        .get_async(&PathBuf::from("/tmp"), 8, None, None)
        .await
        .expect("Unable to get the queried data");

    // produces a 49 bands GeoTiff
    let rds_1 = RasterDatasetBuilder::from_stac_query(&feature_collection)
        .block_size(BlockSize {
            rows: 2048,
            cols: 2048,
        })
        .build();
    println!("Data {}", rds_1);

    // Create a raster dataset for the zones.
    let data_source = DataSourceBuilder::from_file(&PathBuf::from(
        "/home/hardtkel/sources/rust/rss_workspace/eorst/data/zones_stac_histogram.tif",
    ))
    .build();
    let rds_2 = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize {
            rows: 2048,
            cols: 2048,
        })
        .build();
    println!("Zones {}", rds_2);

    // Create the zonal histograms and save to a parquet file.
    let n_bins = 100;
    let min = 0.;
    let max = 1000.;
    let mut zonal_histograms = rds_1.zonal_histograms_raster(&rds_2, n_bins, min, max)?;
    println!("Zonal histograms: \n {:?}", zonal_histograms);
    // the zonal histogram dataframe can then be saved
    save_zonal_histograms(&mut zonal_histograms, &PathBuf::from("test_histo.parquet"));

    Ok(())
}
