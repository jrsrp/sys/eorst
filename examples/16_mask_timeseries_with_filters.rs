use eorst::{
    utils::{init_logger, BlockSize, RasterData},
    Filters, RasterDatasetBuilder,
};

#[cfg(feature = "use_rss")]
use rss::{query::ImageQueryBuilder, qvf::Collection, utils::*, DEA};

use anyhow::Result;
use chrono::NaiveDate;
use log::info;
use ndarray::{s, Array4, Axis, Zip};

use stac::ItemCollection;
use std::ops::Deref;
use std::path::PathBuf;

// To run this example:
//cargo run --release --example 16_mask_timeseries_with_filters --features use_rss --features use_opencv

//This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, mask: &RasterData<u8>) -> Array4<i16> {
    // make a mut copy of data to update the cloud/shadow pixel by na.
    let mut result = data.to_owned();
    // use zip to iterate simultaneusly over result and mask.
    result
        .axis_iter_mut(Axis(0)) // loop result over time
        .zip(mask.axis_iter(Axis(0))) // loop mask over time
        .for_each(|(mut r, m)| {
            // now r and m are Arraview3 in a particular timr.
            // erode and dilate filters can only be applied to Array2;
            // so we take the first band of mask.
            let m = m.slice(s![0, .., ..]).to_owned();
            // apply erode and dilate filters
            let eroded = m.erode(3);
            let dilated = eroded.dilate(2);
            // use zip to iterate over r and a broadcaset (Array3) view of dilated mask.
            Zip::from(&mut r).and_broadcast(&dilated).for_each(|v, m| {
                // compare mask
                if m == &2 || m == &4 {
                    *v = 32764;
                }
            });
        });

    result
}

fn main() -> Result<()> {
    init_logger();
    let start_date = "20230601";
    let end_date = "20231031";
    let scene = "55jhm";
    info!("Starting example");
    // This will yield 19 items
    let query = ImageQueryBuilder::new(
        DEA.deref().clone(),
        Collection::Sentinel2,
        Intersects::Scene(vec![scene]),
        &["nbart_red", "nbart_blue", "nbart_nir_1"],
    )
    .start_date(NaiveDate::parse_from_str(start_date, "%Y%m%d").unwrap())
    .end_date(NaiveDate::parse_from_str(end_date, "%Y%m%d").unwrap())
    .cloudcover((Cmp::Less, 100))
    .build();

    let tmp_dir = PathBuf::from("/test/tmp/");
    info!("Getting input data");
    let feature_collection: ItemCollection = query
        .get(&tmp_dir, None, None)
        .expect("Could not download files");

    let rds = RasterDatasetBuilder::from_stac_query(&feature_collection)
        .block_size(BlockSize {
            rows: 2048,
            cols: 2048,
        })
        .build();

    let query = ImageQueryBuilder::new(
        DEA.deref().clone(),
        Collection::Sentinel2,
        Intersects::Scene(vec![scene]),
        &["oa_fmask"],
    )
    .start_date(NaiveDate::parse_from_str(start_date, "%Y%m%d").unwrap())
    .end_date(NaiveDate::parse_from_str(end_date, "%Y%m%d").unwrap())
    .cloudcover((Cmp::Less, 100))
    .build();

    let tmp_dir = PathBuf::from("/tmp/ex16");
    let feature_collection: ItemCollection = query
        .get(&tmp_dir, None, None)
        .expect("Could not download files");

    let mask = RasterDatasetBuilder::from_stac_query(&feature_collection)
        .block_size(BlockSize {
            rows: 2048,
            cols: 2048,
        })
        .set_geo_transform(rds.metadata.geo_transform)
        .set_resolution(eorst::ImageResolution { x: 10., y: 10. })
        .set_image_size(eorst::ImageSize {
            rows: rds.metadata.shape.rows,
            cols: rds.metadata.shape.cols,
        })
        .build();

    info!("Processing: {}", rds);

    let out_fn = PathBuf::from("/tmp/test.vrt");

    rds.process_with_mask::<i16, u8, i16>(&mask, worker, 4, &out_fn);

    Ok(())
}
