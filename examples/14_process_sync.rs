extern crate eorst;

use eorst::{
    utils::{BlockSize, Dimension, RasterData},
    DataSourceBuilder, RasterDatasetBuilder,
};
use ndarray::{s, Array2, Array3, ArrayView3, Axis, Zip};
use std::path::PathBuf;

// compute ndvi over an Array3 with nir in the second index of the first axis,
// and red in the second index of the dirst axis.
fn ndvi(reflectance_data: &ArrayView3<i16>) -> Array2<i16> {
    let red = reflectance_data.slice(s![0_isize, .., ..]);
    let nir = reflectance_data.slice(s![1_isize, .., ..]);

    let mut result = Array2::zeros(red.raw_dim());

    Zip::from(&mut result)
        .and(&red)
        .and(&nir)
        .for_each(|result, &red_elem, &nir_elem| {
            let ndvi_value = ((nir_elem as f32 - red_elem as f32)
                / (nir_elem as f32 + red_elem as f32))
                * 10000.0;
            *result = ndvi_value as i16;
        });

    result
}

// This is the function that is applied to each block.
pub fn worker(data: &RasterData<i16>, _: Dimension) -> Array3<i16> {
    let ndvi = ndvi(&data.slice(s![0_usize, .., .., ..]));
    ndvi.insert_axis(Axis(0))
}

fn main() {
    let data_source =
        DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t54lxk_20200623_abam4.tif"))
            .bands(vec![3, 4])
            .build();
    let rds = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize {
            rows: 1024,
            cols: 1024,
        })
        .build();
    let out_fn = PathBuf::from("4_ndvi.tif");
    rds.reduce::<i16, i16>(worker, Dimension::Layer, 8, &out_fn, 65536);
}
