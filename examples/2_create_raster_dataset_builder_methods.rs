use eorst::{
    utils::{init_logger, BlockSize, ImageResolution},
    DataSourceBuilder, RasterDatasetBuilder,
};
use std::path::PathBuf;

fn main() {
    init_logger();
    let data_source =
        DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
            .build();

    println!("Change blocks: \n ");

    let rds = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize {
            cols: 256,
            rows: 256,
        })
        .build();
    println!("raster dataset with 256x256 blocks: \n {rds}");

    let rds = RasterDatasetBuilder::from_source(&data_source)
        .set_resolution(ImageResolution { x: 30., y: 30. })
        .build();
    println!("raster with 30x30 resolution: \n {rds}");

    let rds = RasterDatasetBuilder::from_source(&data_source)
        .set_epsg(32756)
        .build();
    println!("raster projected on the fly: \n {rds}");

    let rds = RasterDatasetBuilder::from_source(&data_source)
        .set_epsg(32756)
        .set_resolution(ImageResolution { x: 15., y: 15. })
        .build();
    println!("raster projected on the fly and with changed resolution: \n {rds}");
}
