# This Nix flake builds Rust and Python projects (rss), supports container builds,
# and provides an easy way to run tests, lint code and build
# Python bindings using maturin.
# Available commands:
#  - `nix build`: Builds the rss binary.
#  - `nix build .#test`: Builds and runs the test suite.
#  - `nix build .#clippy`: Runs clippy to lint the code.
#  - `nix build .#container`: Builds a Docker container image including rss and runtime deps.
#  - `nix build .#pythonBinding`: Builds Python bindings using maturin.
#  - `nix develop`: Opens a development shell with all dependencies and code editor.

# Outputs:
#  - By default, the build output is placed in the Nix store and linked to the ./result folder. After building, you can run:
#  - Package build: `nix build` will produce ./result/lib/rss
#  - Python bindings (via `nix build .#pythonBinding`) will produce a `.whl` in ./result/dist.
#  - Docker container output (via `nix build .#container`) will build the container image, which can be loaded with:
#      docker load < result

{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils"; # Utility functions for flakes
    naersk.url = "github:nix-community/naersk"; # Builds Rust projects using Nix
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable"; # Latest Nix packages
  };

  outputs = { self, flake-utils, naersk, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # Import the package set for the specified system (e.g., x86_64-linux)
        pkgs = (import nixpkgs) {
          inherit system;
        };

        # Import naersk for building Rust projects
        naersk' = pkgs.callPackage naersk {};

        # Define build-time dependencies
        commonNativeBuildInputs = with pkgs; [
          rustc
          cargo
          pkg-config
          clang
          opencv
          cmake
          python3 # Python is needed for the Python bindings
        ];

        # Define both build-time and runtime dependencies
        commonBuildInputs = with pkgs; [
          cacert
          gdal
          openssl
          llvmPackages.libclang.lib # Required for linking with LLVM/Clang
        ];

        # Function to build the package with a specific mode (e.g., "build", "test", "clippy")
        buildWithMode = mode: naersk'.buildPackage {
          src = ./.;
          nativeBuildInputs = commonNativeBuildInputs;
          buildInputs = commonBuildInputs;
          preBuild = ''
            export https_proxy=http://web-espproxy-usr.dmz:80;
            export LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
            export TMPDIR=$(mktemp -d)
            ls -lahtr $TMPDIR
            export OUT_DIR=$TMPDIR
          
          '';
          
          cargoBuildOptions = oldOpts: oldOpts ++ [  "--no-default-features" ];
          inherit mode; # Pass the mode (build, test, clippy) to the build system
        };

        # Example: Build the project using the "build" mode
        containerBuild = buildWithMode "build";

      in rec {
        packages = {

          # Default build command, run with `nix build` or `nix run`
          default = buildWithMode "build";

          # Run tests using the "test" mode: `nix build .#test`
          test = buildWithMode "test";

          # Lint the code using clippy: `nix build .#clippy`
          clippy = buildWithMode "clippy";

          # Build a Docker container image: `nix build .#container`
          container = pkgs.dockerTools.buildImage {
            name = "eorst-container"; # Change this to your desired container name
            tag = "latest"; # You can change the tag if needed
            copyToRoot = pkgs.buildEnv {
              name = "my-env";
              paths = [
                containerBuild # Include the built project in the container
                pkgs.bash
                pkgs.gdal
                pkgs.openssl
                pkgs.cacert
              ];
            };
            config = {
              Cmd = [ "eorst" ]; 
            };
          };
        };

        # Development shell environment: `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = commonNativeBuildInputs ++ [pkgs.maturin];
          buildInputs = commonBuildInputs ++ [
            pkgs.helix # Helix editor (or replace with your preferred editor)
            pkgs.rust-analyzer
            pkgs.python312Packages.ipython
            pkgs.python312Packages.arrow
            pkgs.python312Packages.pyarrow
            pkgs.python312Packages.geopandas
           ];

          # Set up the development shell with the necessary environment variables
          shellHook = ''
            export https_proxy=http://web-espproxy-usr.dmz:80;
            export LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
          '';
        };
      }
    );
}
