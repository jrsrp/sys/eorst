//! # Install
//!
//! To build this crate from source you need to have libgdal and rust installed in your system.
//!
//! ## Install rust
//!
//! Type the following command and follow the instructions.
//!
//! ```bash
//! curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
//! ```
//!
//! ## Install dependencies in debian based systems
//!
//! It supports gdal 2.x or 3.x thanks to the underlaying [rust-gdal crate](https://github.com/georust/gdal).
//!
//! ```bash
//! sudo apt install libgdal-dev
//! ```
//!
//! ## Grab the code and build
//!
//! ```bash
//! git clone https://gitlab.com/leohardtke/eotrs/
//! cargo install --path eotrs
//! ```
//!
//! ## Test
//!
//! ```bash
//! cargo test
//! ```
