#[cfg(test)]
mod test {
    use crate::*;

    
    use num_traits::FromPrimitive;

    use std::ops::{Add, Div};

    #[test]
    fn test_align() {
        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .build();
        let rds_1 = RasterDatasetBuilder::from_source(&data_source).build();

        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_suba_abam5.tif",
        ))
        .build();
        let rds_2 = RasterDatasetBuilder::from_source(&data_source)
            .set_template(&rds_1)
            .build();
        assert_eq!(rds_1.metadata.shape, rds_2.metadata.shape);
        assert_eq!(rds_1.metadata.geo_transform, rds_2.metadata.geo_transform);

        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_suba_abam6.tif",
        ))
        .build();
        let rds_2 = RasterDatasetBuilder::from_source(&data_source)
            .set_template(&rds_1)
            .build();
        println!(
            "rds_1 {:?} rds_2{:?}",
            rds_1.metadata.geo_transform, rds_2.metadata.geo_transform
        );
        assert_eq!(rds_1.metadata.shape, rds_2.metadata.shape);
        assert_eq!(rds_1.metadata.geo_transform, rds_2.metadata.geo_transform);
    }

    #[test]
    fn test_iter() {
        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .build();
        let rds = RasterDatasetBuilder::from_source(&data_source).build();
        for iter in rds.iter() {
            let _ = iter.parent.read_block::<i32>(iter.iter_index);
        }
    }
    #[test]
    fn test_stacked_dims() {
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let e = RasterDataShape {
            times: 1,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        shp_1.stack(shp_2, Dimension::Layer);
        assert_eq!(shp_1, e);
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 1,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        let e = RasterDataShape {
            times: 1,
            layers: 3,
            rows: 1024,
            cols: 1024,
        };
        shp_1.stack(shp_2, Dimension::Layer);
        assert_eq!(shp_1, e);
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let e = RasterDataShape {
            times: 2,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        shp_1.stack(shp_2, Dimension::Time);
        assert_eq!(shp_1, e);
    }

    #[test]
    fn test_shape_extend() {
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let e = RasterDataShape {
            times: 2,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        shp_1.extend(shp_2);
        assert_eq!(shp_1, e);

        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 1,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        let e = RasterDataShape {
            times: 2,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        shp_1.extend(shp_2);
        assert_eq!(shp_1, e);
    }
    #[test]
    #[should_panic(expected = "Unable to extend layers")]
    fn test_shape_extend_diff_layer() {
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 1,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        let _e = RasterDataShape {
            times: 2,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        shp_1.extend(shp_2);
    }

    #[test]
    #[should_panic(expected = "Unable to stack layers")]
    fn test_shape_stack_diff_time() {
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 2,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        shp_1.stack(shp_2, Dimension::Layer);
    }

    #[test]
    #[should_panic(expected = "Unable to stack layers")]
    fn test_shape_stack_diff_time2() {
        let mut shp_1 = RasterDataShape {
            times: 1,
            layers: 1,
            rows: 1024,
            cols: 1024,
        };
        let shp_2 = RasterDataShape {
            times: 2,
            layers: 2,
            rows: 1024,
            cols: 1024,
        };
        shp_1.stack(shp_2, Dimension::Layer);
    }

    #[test]
    fn test_layer_stack_position() {
        let layer_1: Layer = Layer {
            source: PathBuf::from("image_1.img"),
            layer_pos: 0,
            time_pos: 0,
        };
        let mut layer_2: Layer = Layer {
            source: PathBuf::from("image_1.img"),
            layer_pos: 0,
            time_pos: 0,
        };
        let max_time_rds_1 = 3;
        layer_2.stack_position(layer_1, Dimension::Layer, max_time_rds_1);
        assert_eq!(layer_2.layer_pos, 3);
    }

    #[test]
    fn test_reduce() {
        std::env::set_var("RAYON_NUM_THREADS", "1");

        fn worker<T>(data: &RasterData<T>, dimension: Dimension) -> Array3<T>
        where
            T: gdal::raster::GdalType
                + num_traits::identities::Zero
                + Copy
                + FromPrimitive
                + Add<Output = T>
                + Div<Output = T>,
        {
            data.sum_dimension(dimension)
        }
        // Create a ds with 2 time steps
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2].to_vec(),
        };
        let source_2 = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2].to_vec(),
        };

        let sources = vec![source, source_2];

        let rds = RasterDatasetBuilder::from_sources(&sources)
            .block_size(BlockSize {
                rows: 1024,
                cols: 1024,
            })
            .source_composition_dimension(Dimension::Time)
            .band_composition_dimension(Dimension::Layer)
            .build();

        let out_file = PathBuf::from("/tmp/tmp.tif");
        let dimension = Dimension::Layer;
        rds.reduce::<i16, i16>(worker, dimension, 1, &out_file, 65536);
    }

    #[test]
    fn test_overlap() {
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1].to_vec(),
        };
        let rds_over_0: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(0)
            .build();

        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1].to_vec(),
        };
        let rds_over_1: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(1)
            .build();
        println!("rds_over: {rds_over_1}");

        // The idea is that reading with no overlap should give the same result
        // as reading with overlap + trimming the excess
        let n_blocks = 4;
        for block_id in 0..n_blocks {
            //println!("block_id {block_id:?}");
            let array_with_overlap = rds_over_1.read_block::<i32>(block_id);
            //println!("read 1 {array_with_overlap:?}");
            let overlap = rds_over_1.blocks[block_id].overlap;
            //println!("array_with overlap {array_with_overlap:?}");

            let array_with_overlap_trimmed = trimm_array4_owned(&array_with_overlap, &overlap);
            //println!("array_trimmed {array_with_overlap_trimmed:?}");

            // should be the same than with no overlaps!
            let array_with_no_overlap = rds_over_0.read_block::<i32>(block_id);
            //println!("read 2 {array_with_no_overlap:?}");
            assert_eq!(array_with_no_overlap, array_with_overlap_trimmed);
        }
    }
    #[test]
    fn test_get_overlap() {
        let image_size = ImageSize {
            rows: 1580,
            cols: 1514,
        };
        let block_size = BlockSize {
            rows: 1024,
            cols: 1024,
        };
        let overlap_size = 1;
        let block_col_rows = vec![(0, 0), (1, 0), (0, 1), (1, 1)];
        for block_col_row in block_col_rows {
            get_overlap(image_size, block_size, block_col_row, overlap_size);
        }
    }

    #[test]
    fn test_extract_blockwise() {
        let mut e: BTreeMap<i16, Vec<i16>> = BTreeMap::new();
        e.insert(1, vec![1910]);
        e.insert(2, vec![957]);
        e.insert(3, vec![1680]);
        e.insert(4, vec![1847]);
        e.insert(5, vec![1746]);
        e.insert(6, vec![624]);
        e.insert(7, vec![818]);
        e.insert(8, vec![1103]);

        let raster_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif");
        let vector_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.gpkg");
        let bls: Vec<usize> = vec![16, 32, 64, 128, 256, 512];
        bls.iter().for_each(|b| {
            let data_source = DataSourceBuilder::from_file(&raster_path)
                .bands(vec![1])
                .build();
            let rds = RasterDatasetBuilder::from_source(&data_source)
                .block_size(utils::BlockSize { rows: *b, cols: *b })
                .build();
            let data = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);
            assert_eq!(e, data);
        });
    }

    #[test]
    #[cfg(feature = "use_polars")]
    fn extract_blockwise_time() {
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 3].to_vec(),
        };

        let vector_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.gpkg");
        let overlap_size = 0;
        // two band,
        let rds: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(overlap_size)
            .build();
        let _extracted_2b =
            rds.extract_blockwise_time(&vector_path, "id", SamplingMethod::Value, None);
        println!("Extracted: {_extracted_2b:?}");
    }

    #[test]
    fn test_extract_blockwise_padding() {
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 3].to_vec(),
        };

        let vector_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.gpkg");
        let overlap_size = 0;
        // two band,
        let rds: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(overlap_size)
            .build();
        let _extracted_2b = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);
        println!("Extracted: {_extracted_2b:?}");

        // Using value
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 3].to_vec(),
        };
        let rds: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(overlap_size)
            .build();
        let extracted_1 = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);

        let overlap_size = 1;
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 3].to_vec(),
        };
        let rds: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(overlap_size)
            .build();
        let extracted_2 = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);
        assert_eq!(extracted_1, extracted_2);

        // Using mode
        let overlap_size = 0;
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 3].to_vec(),
        };
        let rds: RasterDataset = RasterDatasetBuilder::from_source(&source)
            .overlap_size(overlap_size)
            .build();
        let extracted_mode = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);
        assert_eq!(extracted_1, extracted_mode);
    }

    #[test]
    fn test_stack_datasets() {
        let src_fn = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif");
        let source_1 = DataSourceBuilder::from_file(&src_fn).build();
        let source_2 = source_1.clone();
        let mut rds_1 = RasterDatasetBuilder::from_source(&source_1)
            .band_composition_dimension(Dimension::Layer)
            .build();
        let rds_2 = RasterDatasetBuilder::from_source(&source_2)
            .band_composition_dimension(Dimension::Layer)
            .build();
        rds_1.stack(&rds_2, Dimension::Layer);

        let block_id = 0;
        let data = rds_1.read_block::<i32>(block_id);
        assert_eq!(data.shape(), [1, 12, 1024, 1024]);
    }

    #[test]
    fn test_extend_datasets() {
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3, 4, 5, 6].to_vec(),
        };
        let source_2 = source.clone();
        let mut rds_1 = RasterDatasetBuilder::from_source(&source)
            .band_composition_dimension(Dimension::Layer)
            .build();
        let rds_2 = RasterDatasetBuilder::from_source(&source_2)
            .band_composition_dimension(Dimension::Layer)
            .build();
        rds_1.extend(&rds_2);
        let block_id = 0;
        let data = rds_1.read_block::<i32>(block_id);
        assert_eq!(data.shape(), [2, 6, 1024, 1024]);
    }

    #[test]
    fn test_epsg() {
        fn worker(data: &Array4<i16>) -> Array4<i16> {
            data.to_owned()
        }

        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_suba_abam5.tif"),
            bands: [1, 2].to_vec(),
        };

        let rds = RasterDatasetBuilder::from_source(&source)
            .set_epsg(3577)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 1,
                layers: 2,
                rows: 1419,
                cols: 1613
            }
        );

        rds.process(worker, 1, &PathBuf::from("test.tif"));

        let rds = RasterDatasetBuilder::from_source(&source)
            .band_composition_dimension(Dimension::Time)
            .set_epsg(4326)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 2,
                layers: 1,
                rows: 1179,
                cols: 1559
            }
        );
    }

    #[test]
    fn test_layers_from_source() {
        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3, 4, 5, 6].to_vec(),
        };

        let rds = RasterDatasetBuilder::from_source(&source)
            .band_composition_dimension(Dimension::Layer)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 1,
                layers: 6,
                rows: 1580,
                cols: 1514
            }
        );

        let source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3, 4, 5, 6].to_vec(),
        };

        let rds = RasterDatasetBuilder::from_source(&source)
            .band_composition_dimension(Dimension::Time)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 6,
                layers: 1,
                rows: 1580,
                cols: 1514
            }
        );
    }

    #[test]
    fn test_zip() {
        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .build();

        let rds_1 = RasterDatasetBuilder::from_source(&data_source).build();
        let rds_2 = RasterDatasetBuilder::from_source(&data_source).build();

        for (b_1, b_2) in rds_1.iter().zip(rds_2.iter()) {
            let id = b_1.iter_index;
            let b1_data = b_1.parent.read_block::<i16>(id);
            let b2_data = b_2.parent.read_block::<i16>(id);
            let mask = b2_data.mapv(|v| (v > 1000) as i16);
            let _result = b1_data * mask;
        }
    }

    #[test]
    fn test_raster_data_source_builder() {
        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .build();

        let _ = RasterDatasetBuilder::from_source(&data_source).build();
    }

    #[test]
    fn test_data_source_builder() {
        let _data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .build();
        let _data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .bands(vec![1, 3])
        .build();
        let _e = [
            "cemsre_t55jfm_20200614_sub_abam5_1",
            "cemsre_t55jfm_20200614_sub_abam5_3",
        ];

        let _data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .bands(vec![1, 4])
        .build();
    }

    #[test]
    fn test_compositions() {
        let data_source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let data_source_2 = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let rds = RasterDatasetBuilder::from_sources(&vec![data_source_2, data_source]).build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 2,
                layers: 3,
                rows: 1580,
                cols: 1514
            }
        );

        let data_source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let data_source_2 = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let rds = RasterDatasetBuilder::from_sources(&vec![data_source_2, data_source])
            .band_composition_dimension(Dimension::Layer)
            .source_composition_dimension(Dimension::Layer)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 1,
                layers: 6,
                rows: 1580,
                cols: 1514
            }
        );

        let data_source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let data_source_2 = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let rds = RasterDatasetBuilder::from_sources(&vec![data_source_2, data_source])
            .band_composition_dimension(Dimension::Time)
            .source_composition_dimension(Dimension::Time)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 6,
                layers: 1,
                rows: 1580,
                cols: 1514
            }
        );

        let data_source = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let data_source_2 = DataSource {
            source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
            bands: [1, 2, 3].to_vec(),
        };
        let rds = RasterDatasetBuilder::from_sources(&vec![data_source_2, data_source])
            .band_composition_dimension(Dimension::Time)
            .source_composition_dimension(Dimension::Layer)
            .build();
        assert_eq!(
            rds.metadata.shape,
            RasterDataShape {
                times: 3,
                layers: 2,
                rows: 1580,
                cols: 1514
            }
        );
    }
}
