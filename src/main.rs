use crate::eorst::{utils, utils::write_csv_array, utils::SamplingMethod, RasterDatasetBuilder};
use clap::{error::ErrorKind, CommandFactory};
use clap::{Args, Parser, Subcommand};

use eorst::{self, DataSourceBuilder};
use log::debug;
use std::path::PathBuf;

#[derive(Parser)]
#[clap(about, version, long_about = None)]
#[clap(propagate_version = true)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Extracts raster values at specific locations
    #[clap(name = "extract")]
    Extract(Extract),
}

#[derive(Args, Clone)]
struct Extract {
    #[clap(short, long)]
    /// Select bands
    bands: String,
    #[clap(short = 's', long, default_value = "256")]
    block_size: usize,
    #[clap(short, long, default_value = "4")]
    n_threads: usize,
    #[clap(short, long, value_enum, default_value = "value")]
    method: SamplingMethod,
    #[clap(short = 'u', long)]
    buffer_size: Option<usize>,
    /// Raster file to extract the values from
    raster: PathBuf,
    /// Poits vector file with the location
    points_vector: PathBuf,
    // id column
    points_vector_id: String,
    /// csv output file
    output: PathBuf,
}

fn main() {
    let cli = Cli::parse();

    
            utils::init_logger();

    match &cli.command {
        Commands::Extract(args) => {
            if (args.method == SamplingMethod::Value) & args.buffer_size.is_some() {
                let mut cmd = Cli::command();
                cmd.error(
                    ErrorKind::ArgumentConflict,
                    "buffer_size only aplicable to method=mode",
                )
                .exit();
            }
            if (args.method == SamplingMethod::Mode) & args.buffer_size.is_none() {
                let mut cmd = Cli::command();
                cmd.error(
                    ErrorKind::ArgumentConflict,
                    "buffer_size is required when using method=mode",
                )
                .exit();
            }
            let args = args.clone();
            let bands: Vec<usize> = args.bands.split(',').map(|b| b.parse().unwrap()).collect();
            let raster_path = args.raster;
            let vector_path = args.points_vector;
            let data_source = DataSourceBuilder::from_file(&raster_path)
                .bands(bands)
                .build();

            let rds = RasterDatasetBuilder::from_source(&data_source)
                .block_size(utils::BlockSize {
                    rows: args.block_size,
                    cols: args.block_size,
                })
                .build();

            let data =
                rds.extract_blockwise(&vector_path, &args.points_vector_id, args.method, None);
            debug!(
                "
                data: {data:?}"
            );
            let vals: Vec<Vec<i16>> = data.values().cloned().collect();
            write_csv_array(vals, args.output);
        }
    }
}
