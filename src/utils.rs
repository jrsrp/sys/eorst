use clap::ValueEnum;
use csv::WriterBuilder;
use env_logger::Env;
use gdal::{Dataset, DriverManager};
use ndarray_csv::Array2Writer;

use gdal::{raster::RasterCreationOptions, spatial_ref::SpatialRef};
use ndarray::Array2;
use ndarray::{s, Array, Array3, Array4, ArrayView2, ArrayView3, Axis};
use num_traits::FromPrimitive;
#[cfg(feature = "use_opencv")]
use opencv::{core, prelude::MatTraitConst, prelude::MatTraitConstManual};
#[cfg(feature = "use_polars")]
use polars::prelude::ParquetWriter;
use rand::Rng;
use rand::SeedableRng;
use rand_chacha::ChaCha8Rng;
use rayon::iter::IntoParallelIterator;
use serde::{Deserialize, Serialize};
use std::cmp::max;
use std::fs::File;
use std::ops::{Index, IndexMut};
use std::{
    env,
    fmt::Debug,
    ops::{Add, Div},
    path::{Path, PathBuf},
    process::Command,
};
use uuid::Uuid;
pub type RasterData<T> = Array4<T>;

#[derive(Debug, PartialEq)]
pub enum CoordType {
    EInt(i64),
    EFloat(f32),
    EString(String),
}

use crate::{DateType, Extent, RasterDataBlock};
use anyhow::Result;
//#[cfg(feature = "use_polars")]
//use polars::prelude::*;

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct RasterDataShape {
    pub times: usize,
    pub layers: usize,
    pub rows: usize,
    pub cols: usize,
}
//enum definitions
#[derive(Debug, std::cmp::PartialEq, Clone, Copy, Default)]
pub enum Dimension {
    #[default]
    Layer,
    Time,
}

#[derive(Debug, Default, PartialEq, Clone, Copy)]
pub struct ImageResolution {
    pub x: f64,
    pub y: f64,
}

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub struct ImageSize {
    pub rows: usize,
    pub cols: usize,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Offset {
    pub rows: isize,
    pub cols: isize,
}
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Size {
    pub rows: isize,
    pub cols: isize,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct BlockSize {
    pub rows: usize,
    pub cols: usize,
}

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub struct GeoTransform {
    pub x_ul: f64,
    pub x_res: f64,
    pub x_rot: f64,
    pub y_ul: f64,
    pub y_rot: f64,
    pub y_res: f64,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct ReadWindow {
    pub offset: Offset,
    pub size: Size,
}

#[derive(Debug, PartialEq, Clone, Copy, Default)]
pub struct Overlap {
    // Describes how many cells have to be added to the read array
    // to produce a window. For example if overlap_size=1, then the top left
    // block will need one row to the left and one to the top to be added.
    // overlap should be {1,1,0,0}; for the lower left corner in general
    // overlap shoud be {0,0,1,1}
    pub left: usize,
    pub top: usize,
    pub right: usize,
    pub bottom: usize,
}

impl Default for BlockSize {
    fn default() -> Self {
        BlockSize {
            rows: 1024,
            cols: 1024,
        }
    }
}

pub fn create_temp_file(ext: &str) -> String {
    let dir = env::var("TMP_DIR").unwrap_or("/tmp".to_string());
    let dir = Path::new(&dir);
    let file_name = format!("{}.{}", Uuid::new_v4(), ext);
    let file_name = dir.join(file_name);
    file_name.into_os_string().into_string().unwrap()
}

//this function will warp a layer source to a different srs
pub(crate) fn warp(
    source: PathBuf,
    target_resolution: Option<ImageResolution>,
    target_epsg: u32,
) -> PathBuf {
    let new_source = create_temp_file("vrt");
    // make a vrt with the new epsg. I also need to think about the resampling algorithm.
    let mut cmd = Command::new("gdalwarp");
    cmd.arg("-q");
    if target_resolution.is_some() {
        cmd.args([
            "-tr",
            &format!("{}", target_resolution.unwrap().x),
            &format!("{}", target_resolution.unwrap().y),
        ]);
    }
    cmd.args(["-t_srs", &format!("EPSG:{}", target_epsg)])
        .arg(source)
        .arg(&new_source)
        //.arg("-tap")
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    PathBuf::from(new_source)
}

//this function will warp a layer source to a different srs
pub(crate) fn warp_with_te_tr(
    source: PathBuf,
    target_te: &Extent,
    target_resolution: ImageResolution,
) -> PathBuf {
    let new_source = create_temp_file("vrt");
    // make a vrt with the new epsg. I also need to think about the resampling algorithm.
    let mut cmd = Command::new("gdalwarp");
    cmd.arg("-q");

    cmd.args([
        "-te",
        &format!("{}", (target_te.xmin * 100.).round() / 100.),
        &format!("{}", (target_te.ymin * 100.).round() / 100.),
        &format!("{}", (target_te.xmax * 100.).round() / 100.),
        &format!("{}", (target_te.ymax * 100.).round() / 100.),
    ])
    .args([
        "-tr",
        &format!("{}", (target_resolution.x * 100.).round() / 100.),
        &format!("{}", (target_resolution.y * 100.).round() / 100.),
    ])
    .args(["-r", "nearest"])
    //.arg("-tap")
    .arg(source)
    .arg(&new_source);

    cmd.spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    PathBuf::from(new_source)
}

pub(crate) fn change_res(source: PathBuf, target_resolution: ImageResolution) -> PathBuf {
    let new_source = create_temp_file("vrt");
    Command::new("gdalwarp")
        .arg("-q")
        .args([
            "-tr",
            &format!("{}", target_resolution.x),
            &format!("{}", target_resolution.y),
        ])
        .arg(source)
        .arg(&new_source)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    PathBuf::from(new_source)
}
// This function is used internally to create a vrt
// to be used as layer source.
pub(crate) fn extract_band(source: &Path, band: usize) -> PathBuf {
    let new_source = create_temp_file("vrt");
    let argv = &[
        "gdal_translate",
        "-b",
        &format!("{}", band),
        "-q",
        source.to_str().unwrap(),
        &new_source,
    ];

    std::process::Command::new(argv[0])
        .args(&argv[1..])
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    PathBuf::from(new_source)
}

// Creates and empty raster to store the ouptuts of a BlockWorker
pub fn raster_from_size<T>(
    file_name: &PathBuf,
    geo_transform: GeoTransform,
    epsg_code: u32,
    block_size: BlockSize,
    n_bands: isize,
    na_value: f64,
) where
    T: gdal::raster::GdalType + Copy + num_traits::identities::Zero,
{
    let parent_path = file_name.parent().unwrap();
    std::fs::create_dir(parent_path).unwrap_or(());

    let size_x = block_size.cols;
    let size_y = block_size.rows;
    let driver = DriverManager::get_driver_by_name("GTIFF").unwrap();
    let options =
        RasterCreationOptions::from_iter(["COMPRESS=LZW", "BLOCKXSIZE=512", "BLOCKYSIZE=512"]);

    let mut dataset = driver
        .create_with_band_type_with_options::<T, &PathBuf>(
            file_name,
            size_x,
            size_y,
            n_bands as usize,
            &options,
        )
        .unwrap();
    dataset
        .set_geo_transform(&geo_transform.to_array())
        .unwrap();
    let srs = SpatialRef::from_epsg(epsg_code).unwrap();
    dataset.set_spatial_ref(&srs).unwrap();
    for band_index in 1..n_bands + 1 {
        let mut raster_band = dataset.rasterband(band_index as usize).unwrap();

        raster_band.set_no_data_value(Some(na_value)).unwrap();
    }
}

use kdam::{rayon::prelude::*, TqdmIterator, TqdmParallelIterator};
pub fn mosaic(collected: &[PathBuf], tmp_file: &Path, epsg_code: u32) -> Result<()> {
    // First, warp them to the same projection in parallel.

    let collected_reproj: Vec<PathBuf> = collected
        .par_iter()
        .tqdm() // Rayon parallel iterator
        .map(|image| {
            let new_source = create_temp_file("vrt"); // Assuming this is a thread-safe function.
            let mut cmd = Command::new("gdalwarp");
            cmd.arg("-q")
                .args(["-t_srs", &format!("EPSG:{}", epsg_code)])
                .arg(image)
                .arg(&new_source);

            cmd.spawn()
                .expect("failed to start creating vrt")
                .wait()
                .expect("failed to wait for the vrt");

            PathBuf::from(new_source)
        })
        .collect(); // Collect results after parallel processing.

    // Build a VRT with all the collected files.
    Command::new("gdalbuildvrt")
        .arg("-q")
        .arg(tmp_file)
        .args(&collected_reproj)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");

    Ok(())
}
use log::debug;

pub fn translate_with_driver(tmp_fn: &Path, image_fn: &Path, driver_name: &str) -> Result<()> {
    // translate to gtiff
    Command::new("gdal_translate")
        .arg("-q")
        .args(["-of", format!("{driver_name}").as_str()])
        .args(["-co", "BIGTIFF=YES"])
        .args(["-co", "COMPRESS=DEFLATE"])
        .arg(tmp_fn)
        .arg(image_fn)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    let mut dataset = Dataset::open(image_fn).unwrap();
    //let mut band = dataset.rasterband(0).unwrap();

    Ok(())
}

pub fn translate(tmp_fn: &Path, image_fn: &Path) -> Result<()> {
    translate_with_driver(&tmp_fn, &image_fn, "COG");
    Ok(())
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize, ValueEnum)]
pub enum SamplingMethod {
    Value,
    Mode,
    Min,
}

// A location on an array
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Index2d {
    pub col: usize,
    pub row: usize,
}

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub(crate) struct Coordinates {
    // x and y coordinates of a location.
    pub(crate) x: f64,
    pub(crate) y: f64,
}

pub fn init_logger() {
    // Setup logger
    let env = Env::default().filter_or("LOG_LEVEL", "info");
    env_logger::init_from_env(env);
}

impl GeoTransform {
    pub fn to_array(&self) -> [f64; 6] {
        let array: [f64; 6] = [
            self.x_ul, self.x_res, self.x_rot, self.y_ul, self.y_rot, self.y_res,
        ];
        array
    }
}
pub fn rect_view<'a, T>(
    array: &'a ArrayView2<T>,
    rect: Rectangle,
    indices: Index2d,
) -> ArrayView2<'a, T> {
    let max_row = indices.row + rect.bottom;
    let min_row = max(0, indices.row as i32 - rect.top as i32) as usize;

    let max_col = indices.col + rect.right;
    let min_col = max(0, indices.col as i32 - rect.left as i32) as usize;

    array.slice(s![min_row..max_row, min_col..max_col])
}

#[derive(Debug, Copy, Clone)]
pub struct Rectangle {
    // I need better names for this!
    pub left: usize,
    pub top: usize,
    pub right: usize,
    pub bottom: usize,
}

pub fn write_csv_array(data: Vec<Vec<i16>>, csv_filename: PathBuf) {
    // Writes the array into the file.

    let rows = data.len();
    let cols = data[0].len();
    let flat: Vec<i16> = data.into_iter().flatten().collect();
    let array = Array::from(flat)
        .into_shape((rows, cols))
        .expect("Unable to reshape");
    {
        let file = File::create(csv_filename).expect("Unable to create file");
        let mut writer = WriterBuilder::new().has_headers(true).from_writer(file);
        writer
            .serialize_array2(&array)
            .expect("Unable to serialize array to file");
    }
}

pub fn trimm_array3<T>(array: &Array3<T>, overlap_size: usize) -> ArrayView3<'_, T> {
    let min_row = overlap_size;
    let max_row = array.shape()[1] - overlap_size;

    let min_col = overlap_size;
    let max_col = array.shape()[2] - overlap_size;

    array.slice(s![.., min_row..max_row, min_col..max_col])
}

pub fn argmax<T: PartialOrd>(xs: &[T]) -> usize {
    if xs.len() == 1 {
        0
    } else {
        let mut maxval = &xs[0];
        let mut max_ixs: Vec<usize> = vec![0];
        for (i, x) in xs.iter().enumerate().skip(1) {
            if x > maxval {
                maxval = x;
                max_ixs = vec![i];
            } else if x == maxval {
                max_ixs.push(i);
            }
        }
        max_ixs[0]
    }
}

#[cfg(feature = "use_opencv")]
use opencv::boxed_ref::BoxedRef;

#[cfg(feature = "use_opencv")]
use opencv::prelude::MatTraitManual;
// translates an ArrayView2 to an opencv Mat.
#[cfg(feature = "use_opencv")]
/// Converts an `ndarray::ArrayView2` into an OpenCV `Mat`
pub fn arrayview2_to_mat<T>(data: ArrayView2<T>) -> Result<BoxedRef<'_, core::Mat>>
where
    T: opencv::prelude::DataType + 'static,
{
    // Convert ndarray's shape to OpenCV-compatible dimensions
    let rows = data.dim().0 as i32;
    let cols = data.dim().1 as i32;

    // Create an empty OpenCV Mat with the appropriate type
    let mut mat = unsafe {
        core::Mat::new_rows_cols(
            rows,
            cols,
            T::opencv_type(), // Use the correct OpenCV type for T
        )?
    };

    // Copy data from the ndarray into the Mat
    mat.data_typed_mut::<T>()?
        .copy_from_slice(data.as_slice().unwrap());

    // Convert the Mat into BoxedRef and return
    Ok(mat.into())
}

#[cfg(feature = "use_opencv")]
/// Converts an OpenCV `Mat` back into an `ndarray::Array2<T>`
pub fn mat_to_array2<T>(mat: &core::Mat) -> Result<Array2<T>>
where
    T: opencv::prelude::DataType + Clone,
{
    let rows = mat.rows() as usize;
    let cols = mat.cols() as usize;

    let mat_slice: &[T] = mat.data_typed()?;

    let array = ArrayView2::from_shape((rows, cols), mat_slice)
        .unwrap()
        .to_owned();

    Ok(array)
}

pub trait Stack {
    fn stack(&mut self, other: RasterDataShape, dim_to_stack: Dimension) -> &mut RasterDataShape;
    fn extend(&mut self, other: RasterDataShape) -> &mut RasterDataShape;
}

impl Stack for RasterDataShape {
    fn extend(&mut self, other: RasterDataShape) -> &mut RasterDataShape {
        let mut extendable = true;

        for dim_loc in 1..4 {
            if self[dim_loc] != other[dim_loc] {
                extendable = false
            }
        }

        if extendable {
            self[0] += other[0];
            self
        } else {
            panic!("Unable to extend layers");
        }
    }

    fn stack(&mut self, other: RasterDataShape, dim_to_stack: Dimension) -> &mut RasterDataShape {
        let dimension_axis = dim_to_stack.get_axis();
        let mut stackable = true;
        for dim_loc in 0..4 {
            if dim_loc != dimension_axis && self[dim_loc] != other[dim_loc] {
                stackable = false;
            }
        }

        if stackable {
            self[dimension_axis] += other[dimension_axis];
            self
        } else {
            panic!("Unable to stack layers");
        }
    }
}

impl Dimension {
    pub fn get_axis(&self) -> usize {
        match self {
            Dimension::Layer => 1,
            Dimension::Time => 0,
        }
    }
}

impl Index<usize> for RasterDataShape {
    type Output = usize;

    fn index(&self, index: usize) -> &usize {
        match index {
            0 => &self.times,
            1 => &self.layers,
            2 => &self.rows,
            3 => &self.cols,
            n => panic!("Invalid index: {}", n),
        }
    }
}

impl IndexMut<usize> for RasterDataShape {
    fn index_mut(&mut self, index: usize) -> &mut usize {
        match index {
            0 => &mut self.times,
            1 => &mut self.layers,
            2 => &mut self.rows,
            3 => &mut self.cols,
            n => panic!("Invalid index: {}", n),
        }
    }
}

pub trait SumDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn sum_dimension(&self, dimension: Dimension) -> Array3<T>;
}

pub trait Select<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn select_layer(&self, layer_name: &str) -> Array3<T>;
    fn select_time(&self, time_index: &DateType) -> Array3<T>;
}

impl<T> Select<T> for RasterDataBlock<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + std::fmt::Debug,
{
    fn select_layer(&self, layer_name: &str) -> Array3<T> {
        let index =
            find_str_position(&self.metadata.layer_indices, layer_name).unwrap_or_else(|| {
                panic!(
                    "Layer {layer_name} is not available. Select from {:?}",
                    self.metadata.layer_indices
                )
            });
        let result = self.data.slice(s![.., index, .., ..]).to_owned();
        println!("result {result:?}");
        //result
        todo!()
    }
    fn select_time(&self, _date_index: &DateType) -> Array3<T> {
        todo!();
    }
}

impl<T> SumDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn sum_dimension(&self, dimension: Dimension) -> Array3<T> {
        match dimension {
            Dimension::Layer => self.sum_axis(Axis(1)),
            Dimension::Time => self.sum_axis(Axis(0)),
        }
    }
}

#[allow(dead_code)]
pub(crate) trait MeanDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn mean_dimension(&self, dimension: Dimension) -> Array3<T>;
}

impl<T> MeanDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn mean_dimension(&self, dimension: Dimension) -> Array3<T> {
        let mean = match dimension {
            Dimension::Layer => self.mean_axis(Axis(1)),
            Dimension::Time => self.mean_axis(Axis(0)),
        };
        mean.unwrap()
    }
}
pub trait VarDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn var_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T>;
}

impl<T> VarDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn var_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T> {
        match dimension {
            Dimension::Layer => self.var_axis(Axis(1), ddof),
            Dimension::Time => self.var_axis(Axis(0), ddof),
        }
    }
}

#[allow(dead_code)]
pub(crate) trait StdDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn std_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T>;
}

impl<T> StdDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn std_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T> {
        match dimension {
            Dimension::Layer => self.std_axis(Axis(1), ddof),
            Dimension::Time => self.std_axis(Axis(0), ddof),
        }
    }
}

/// Trim an array4 border by overlap_size pixels.
/// The memory layout of the output is not garanteed.
/// If the subsequent use of .into_shape fails, then make
/// sure to use as_standard_layout!
pub fn trimm_array4<T>(array: &Array4<T>, overlap_size: usize) -> ndarray::ArrayView4<T> {
    let min_row = overlap_size;
    let max_row = array.shape()[2] - overlap_size;

    let min_col = overlap_size;
    let max_col = array.shape()[3] - overlap_size;
    let slice = s![.., .., min_row..max_row, min_col..max_col];
    array.slice(slice)
}

pub fn trimm_array4_owned<T: std::clone::Clone + std::fmt::Debug>(
    array: &Array4<T>,
    overlap: &Overlap,
) -> ndarray::Array4<T> {
    let min_row = overlap.top;
    let max_row = array.shape()[2] - overlap.bottom;

    let min_col = overlap.left;
    let max_col = array.shape()[3] - overlap.right;
    let slice = s![.., .., min_row..max_row, min_col..max_col];
    let trimmed = array.slice(slice);
    let result = trimmed.as_standard_layout().to_owned();
    result
}

pub fn create_clustered_array(size: usize, num_values: u32, cluster_size: usize) -> Array2<u32> {
    let mut array: Array2<u32> = Array::zeros((size, size));

    let loop_size = size * size / cluster_size / cluster_size;
    for idx in 0..loop_size {
        let mut rng = ChaCha8Rng::seed_from_u64(idx.try_into().unwrap());
        let value = rng.gen_range(1..=num_values);
        let row = rng.gen_range(0..size);
        let col = rng.gen_range(0..size);

        for i in 0..cluster_size {
            for j in 0..cluster_size {
                let x = (row + i) % size;
                let y = (col + j) % size;
                array[[x, y]] = value;
            }
        }
    }

    array
}

#[cfg(feature = "use_polars")]
pub fn save_zonal_histograms(zonal_histograms: &mut polars::frame::DataFrame, file_name: &PathBuf) {
    let histogram_file = std::fs::File::create(file_name).unwrap();

    let writer = ParquetWriter::new(histogram_file);
    writer.finish(zonal_histograms).unwrap();
}

fn find_str_position(vec: &[String], target: &str) -> Option<usize> {
    vec.iter().position(|s| s == target)
}
#[cfg(test)]
mod tests {

    use crate::{DataSourceBuilder, RasterDatasetBuilder};
    use anyhow::Result;
    #[cfg(feature = "use_polars")]
    use polars::datatypes::AnyValue;
    #[cfg(feature = "use_polars")]
    use polars::prelude::col;
    #[cfg(feature = "use_polars")]
    use polars::prelude::IntoLazy;
    use std::path::PathBuf;

    #[test]
    #[cfg(feature = "use_polars")]
    fn test_zonal_stats_data() -> Result<()> {
        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_sub_abam5.tif",
        ))
        .bands(vec![1])
        .build();
        let rds_1 = RasterDatasetBuilder::from_source(&data_source).build();

        let data_source = DataSourceBuilder::from_file(&PathBuf::from(
            "data/cemsre_t55jfm_20200614_abam5_zones.tif",
        ))
        .build();
        let rds_2 = RasterDatasetBuilder::from_source(&data_source).build();
        let zonal_histograms = rds_1.zonal_histograms_raster(&rds_2, 10, 0., 2000.)?;
        // test on know values.
        let df_know_val = zonal_histograms
            .clone()
            .lazy()
            .filter(col("zone").eq(3))
            .filter(col("bin_star").eq(800.0))
            .collect()?;

        let value = &df_know_val.get(0).unwrap()[5];

        if let AnyValue::Int32(v) = value {
            assert_eq!(v, &1067);
        }

        let df_know_val = zonal_histograms
            .clone()
            .lazy()
            .filter(col("zone").eq(6))
            .filter(col("bin_star").eq(400.0))
            .collect()?;
        let value = &df_know_val.get(0).unwrap()[5];
        if let AnyValue::Int32(v) = value {
            assert_eq!(v, &14477);
        }
        Ok(())
    }
}
