pub mod impl_methods;

use crate::utils::{BlockSize, Dimension, GeoTransform, ImageResolution, ImageSize};
use crate::{RasterBlock, RasterMetadata};

/// Main data structure of eorst. Stores the raster dataset [metadata](crate::RasterMetadata) and [blocks](crate::RasterBlock).
///
/// Example:
/// ```
/// use std::path::PathBuf;
/// use eorst::{utils::BlockSize, RasterDatasetBuilder,DataSourceBuilder};
///
/// let data_source = DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
///     .build();
/// let rds = RasterDatasetBuilder::from_source(&data_source)
///     .block_size(BlockSize {
///        cols: 256,
///        rows: 256,
///    })
///    .build();
/// ```
/// 
/// Operation can be performed over RasterDataset by manually iterating over the [blocks](crate::RasterBlock) like:
/// 
/// ```rust
/// use std::path::PathBuf;
/// use eorst::{utils::BlockSize, RasterDatasetBuilder,DataSourceBuilder};
///
/// let data_source = DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
///     .build();
/// let rds = RasterDatasetBuilder::from_source(&data_source)
///     .block_size(BlockSize {
///        cols: 256,
///        rows: 256,
///    })
///    .build();
/// for iter in rds.iter() {
///     let block_data = iter.
/// }
/// 
/// ```
/// 
/// or by using some of the 

#[derive(Debug, Clone, PartialEq)]
pub struct RasterDataset {
    /// Metadata of a RasterDataset
    pub metadata: RasterMetadata,
    /// Blocks of a RasterDataset
    pub blocks: Vec<RasterBlock>,
    /// Temporaty layers, removed when the RasterDataset is Dropped.
    pub(crate) tmp_layers: Vec<String>,
}

/// A builder for RasterDataset.
#[derive(Default)]
pub struct RasterDatasetBuilder {
    pub sources: Vec<String>,
    pub bands: Vec<Vec<usize>>,
    pub epsg: u32,
    pub overlap_size: usize,
    pub block_size: BlockSize,
    pub composition_bands: Dimension,
    pub composition_sources: Dimension,
    pub image_size: ImageSize,
    pub geo_transform: GeoTransform,
    pub date_indices: Vec<DateType>,
    pub resolution: ImageResolution,
}
