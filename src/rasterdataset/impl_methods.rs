use crate::{DateType, RasterDataset};
use colored::*;

impl std::fmt::Display for DateType {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        let str = match self {
            DateType::Date(date) => {
                format!("{}", date.format("%Y-%m-%d %H:%M:%S"))
            }
            DateType::Index(index) => format!("{}", index),
        };
        write!(fmt, "{}", str)
        //writeln!(fmt, "")
    }
}

impl std::fmt::Display for RasterDataset {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        // writeln!(fmt, "{} \t {}", "Source: ".bold(), self.source).unwrap();

        // writeln!(fmt, "{}", "Coordinates: ".bold()).unwrap();

        // writeln!(fmt, "\t {}", "x ".bold()).unwrap();
        // writeln!(fmt, "\t {}", "y ".bold()).unwrap();
        // writeln!(fmt, "\t {} \t \t \t {:?}", "Bands".bold(), self.bands).unwrap();
        let layer_indices_str = self
            .metadata
            .layer_indices
            .iter()
            .enumerate()
            .map(|(i, val)| format!("{}: {}", i, val))
            .collect::<Vec<String>>()
            .join(", ");

        let time_indices = self
            .metadata
            .date_indices
            .iter()
            .enumerate()
            .map(|(i, val)| match val {
                DateType::Date(date) => {
                    format!(" {}: {}", i, date.format("%Y-%m-%d %H:%M:%S UTC%Z"))
                }
                DateType::Index(index) => format!(" {} ", index),
            })
            .collect::<Vec<String>>()
            .join(", ");

        writeln!(fmt, "{}", "Attributes: ".bold()).unwrap();
        writeln!(
            fmt,
            "\t {} \t ( times: {:?}, layers: {:?}, rows: {:?}, cols: {:?} )",
            "Dataset shape: ".bold(),
            self.metadata.shape.times,
            self.metadata.shape.layers,
            self.metadata.shape.rows,
            self.metadata.shape.cols,
        )
        .unwrap();
        writeln!(fmt, "\t {} \t \t{}", "Time indices:".bold(), time_indices).unwrap();

        writeln!(
            fmt,
            "\t {} \t {}",
            "Layer indices:".bold(),
            layer_indices_str
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t \t ( {:?}, {:?} )",
            "block_size: ".bold(),
            self.metadata.block_size.rows,
            self.metadata.block_size.cols
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t \t {:?}",
            "n_blocks: ".bold(),
            self.blocks.len(),
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t {:?}",
            "geo_transform: ".bold(),
            self.metadata.geo_transform
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t \t {:?} ",
            "epsg_code: ".bold(),
            self.metadata.epsg_code
        )
    }
}
