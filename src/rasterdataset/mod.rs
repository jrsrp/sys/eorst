pub mod impl_methods;

use std::collections::HashMap;
use std::path::PathBuf;

use stac::ItemCollection;

use crate::utils::{BlockSize, Dimension, GeoTransform, ImageResolution, ImageSize};
use crate::{DateType, RasterBlock, RasterMetadata};

/// Main data structure of eorst. Stores the raster dataset [metadata](crate::RasterMetadata) and [blocks](crate::RasterBlock).
///
/// The easiest wasy to create a RasterDataset is by using RasterDatasetBuilder as follows.
///
/// ```rust
/// use std::path::PathBuf;
/// use eorst::{utils::BlockSize, RasterDatasetBuilder,DataSourceBuilder};
///
/// let data_source = DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
///     .build();
/// let rds = RasterDatasetBuilder::from_source(&data_source)
///     .block_size(BlockSize {
///        cols: 256,
///        rows: 256,
///    })
///    .build();
/// ```
///
/// Operation over RasterDataset can be performed by manually iterating over the [blocks](crate::RasterBlock) and
/// using low-level functions like:
/// ```rust
/// use std::path::PathBuf;
/// use eorst::{utils::BlockSize, RasterDatasetBuilder,DataSourceBuilder};
///
/// let data_source = DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
///     .build();
/// let rds = RasterDatasetBuilder::from_source(&data_source)
///     .block_size(BlockSize {
///        cols: 256,
///        rows: 256,
///    })
///    .build();
/// for iter in rds.iter() {
///    let block = iter.block;
///    let id = iter.iter_index;
///    let data = iter.parent.read_block::<i32>(id);
///    // .. do something with data.
///    // block.write3(data.slice(s![0,..,..,..]).to_owned(), &format!("processed_block_{}.tif", i.iter_index));
/// }
///
/// ```
/// or by using some of the helper functions like (RasterDataset::Process)[RasterDataset::Process]
///
/// ```rust
/// use eorst::{utils::BlockSize, RasterDatasetBuilder,DataSourceBuilder};
/// use num_traits::FromPrimitive;
/// use std::{ops::Add, path::PathBuf};
/// use ndarray::Array4;
///
/// fn worker<T>(data: &Array4<T>) -> Array4<T>
/// where
///     T: gdal::raster::GdalType + FromPrimitive + ndarray::ScalarOperand + Add<Output = T>,
/// {
///     let val = T::from_i16(10).unwrap();
///     data + val
/// }
///
/// let data_source =
///     DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
///     .build();
/// let rds = RasterDatasetBuilder::from_source(&data_source)
///     .build();
/// let out_fn = PathBuf::from("3_process_image.tif");
/// rds.process::<i16,i16>(worker, 6, &out_fn);
///```

#[derive(Debug, Clone, PartialEq)]
pub struct RasterDataset {
    /// Metadata of a RasterDataset
    pub metadata: RasterMetadata,
    /// Blocks of a RasterDataset
    pub blocks: Vec<RasterBlock>,
    /// Temporaty layers, removed when the RasterDataset is Dropped.
    pub(crate) tmp_layers: Vec<PathBuf>,
}

/// A builder for RasterDataset.
#[derive(Default)]
pub struct RasterDatasetBuilder {
    pub sources: Vec<PathBuf>,
    pub bands: HashMap<String, Vec<usize>>,
    pub bands_vec: Vec<Vec<usize>>,
    pub epsg: u32,
    pub overlap_size: usize,
    pub block_size: BlockSize,
    pub composition_bands: Dimension,
    pub composition_sources: Dimension,
    pub image_size: ImageSize,
    pub geo_transform: GeoTransform,
    pub date_indices: Vec<DateType>,
    pub resolution: ImageResolution,
    pub feature_collection: Option<ItemCollection>,
}
