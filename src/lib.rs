//! # An earth observation and remote sensing toolkit
//! written in rust.
//!
//! ## Summary
//! This crate offers a library aiming to simplify the writing of raster processing pipelines in rust.
//!
//! ## Highlights
//!
//!  - Parallel read/write.
//!  - On-the-fly image reprojection.
//!  - Point raster sampling extraction.
//!  - Time series analysis.
//!  - Band math.
//!  - Basic opencv integrtion
//!  - Easy to integrate with LigthGBM and XGBoost.
//!
//!
//! ## Crate Status
//!
//! - Still iterating on and evolving the crate.
//!   + The crate is continuously developing, and breaking changes are expected
//!     during evolution from version to version.
//!
//! ## Details
//!
//! EORST is an 1pen-sou1ce library aiming to fill a void in the rust ecosystem, that is the lack of a library
//! to simplify writing geospatial processing code. It takes inspiration from well-established python libraries such as rasterio, rios, and dask and implements some
//! of them in rust, enabling the programmer to focus on the geospatial process rather than the implementation details while taking advantage of the benefits of
//! the rust programming language.
//!
//!  Despite being in early stages of development eorst already offers a rich set of tools such as simple reading and writing of geospatial data
//!  on-the-fly projection and alignment of input data, partition input data into small blocks allowing computation on arrays larger than memory concurrently,
//!  efficient raster point sampling, simple band math, time series analysis and integration with 3rd party libraries like such as opencv to perform computer
//!  vision task and LightGBM and XGBoost for machine learning. The library is available in the official creates registry <https://crates.io/crates/eorst>.
//!
//!  The main focus of this library is to support the work of the [JRSRP](https://www.jrsrp.org.au/), but I expect it to be  useful for a wider audience.
//!  Most of the current functionalities were developed to support the [Spatial Biocondition project](https://www.qld.gov.au/__data/assets/pdf_file/0015/230019/spatial-biocondition-vegetation-condition-map-for-queensland.pdf), developed jointly by Quenslands Remote Sensing Centre
//!  and the Quensland Herbarium.
//!
//!  Future work in the short term will be focused on the stabilization of the API and extending the documentation and examples, collaboration is welcome.
//!
//! ## How to use:
//!
//! ### As a library:
//!
//! Just add eotrs to your dependencies in `cargo.toml` like:
//!
//! ```toml
//! [dependencies]
//! eotrs = {git = https://gitlab.com/leohardtke/eotrs"}
//! ```
//!
//! [RasterDatasets](RasterDataset) are the main data structre and functionality provider of the crate.
//!
//! ### As a cli tool
//!
//! If you want to use the comman line interface see [install](standalonde_docs/install/index.html) for instructions.
//!
//!
//! - Examples:
//!
//! ```bash
//! eorst extract data/cemsre_t55jfm_20200614_sub_abam5.tif\
//!         data/cemsre_t55jfm_20200614_sub_abam5.gpkg\
//!         id\
//!         test.csv\
//!         -b 1,2,3\
//!         -s 256\
//!         -n 8
//!
//!
use std::collections::hash_set::SymmetricDifference;
use std::time::{Instant, Duration};
use crate::utils::argmax;
use gdal::raster::RasterizeOptions;
use glob::glob;
use anyhow::Result;
use ndarray::Array0;
#[cfg(feature = "use_polars")]
use ndhistogram::ndhistogram;
#[cfg(feature = "use_polars")]
use ndarray::azip;
#[cfg(feature = "use_polars")]
use ndhistogram::axis::UniformNoFlow;
#[cfg(feature = "use_polars")]
use ndarray::ArrayView3;
use num_traits::{NumCast, ToPrimitive};
#[cfg(feature = "use_polars")]
use uuid::Uuid;
// should be behind feature
use ndhistogram::AxesTuple;
use ndhistogram::VecHistogram;
use ndarray::ArrayView2;
//
#[cfg(feature = "use_polars")]
use ndhistogram::Histogram;
use colored:: *;
use std::sync::Mutex;
use std::process::Command;
#[cfg(feature = "use_polars")]
use polars::prelude::*;
use std::collections::HashSet;
use std::sync::Arc;
use tokio::sync::{mpsc, Semaphore, OwnedSemaphorePermit};
use tokio::task;

use kdam::{tqdm, Bar, BarExt};

use std::{
    cmp::min,
    collections::{BTreeMap, HashMap},
    ffi::OsStr,
    fmt::Debug,
    fs,
    path::{Path, PathBuf},
    vec,
};
pub mod utils;
use chrono::{DateTime, FixedOffset};
use gdal::vector::{Geometry, LayerAccess};
use gdal::{raster::Buffer, vector::FieldValue, Dataset, DatasetOptions, GdalOpenFlags};
use math::round;
use ndarray::{s, Axis};
use ndarray::{Array, Array2, Array3, Array4};
use utils::{translate_with_driver, Dimension as UtilsDimension};
use crate::utils::Overlap;
use crate::utils::BlockSize;
use crate::utils::ImageSize;
use crate::utils::raster_from_size;
#[cfg(feature = "use_opencv")]
use crate::utils::mat_to_array2;
#[cfg(feature = "use_opencv")]
use crate::utils::arrayview2_to_mat;
use crate::utils::RasterData;
use crate::utils::SamplingMethod;
use crate::utils::rect_view;
use crate::utils::Index2d;
use crate::utils::Rectangle;
use crate::utils::Coordinates;
use crate::utils::translate;
use crate::utils::mosaic;
use crate::utils::create_temp_file;
use crate::utils::trimm_array3;
use crate::utils::GeoTransform;
use crate::utils::RasterDataShape;
use crate::utils::ImageResolution;
use crate::utils::warp;
use crate::utils::ReadWindow;
use crate::utils::Offset;
use crate::utils::Size;
use crate::utils::change_res;
use crate::utils::warp_with_te_tr;
use crate::utils::extract_band;
#[cfg(feature = "use_polars")]
use crate::utils::save_zonal_histograms;
use crate::utils::Stack;
mod tests;
use gdal::spatial_ref::SpatialRef;
use itertools::Itertools;
use rayon::iter::{
    IndexedParallelIterator, IntoParallelIterator, IntoParallelRefIterator, ParallelIterator,
};
use kdam::TqdmParallelIterator;

pub mod rasterdataset;
pub mod standalonde_docs;

pub use rasterdataset::{RasterDataset, RasterDatasetBuilder};

#[derive(Debug, PartialEq, Clone)]
pub struct Extent {
    pub xmin: f64,
    pub ymin: f64,
    pub xmax: f64,
    pub ymax: f64,
}

use std::str::FromStr;

impl FromStr for Extent {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(',').collect();
        if parts.len() != 4 {
            return Err("Extent must be in the format xmin,ymin,xmax,ymax".to_string());
        }

        let xmin = parts[0].parse().map_err(|_| "Invalid xmin")?;
        let ymin = parts[1].parse().map_err(|_| "Invalid ymin")?;
        let xmax = parts[2].parse().map_err(|_| "Invalid xmax")?;
        let ymax = parts[3].parse().map_err(|_| "Invalid ymax")?;

        Ok(Extent { xmin, ymin, xmax, ymax })
    }
}

use log::{debug, warn,info};
// use env_logger;
// use gdal::vector::LayerAccess;

#[cfg(feature = "use_lightgbm")]
use lightgbm3::{Booster, Dataset as LgbmDataset};

#[cfg(feature = "use_lightgbm")]
use serde_json::Value;

use stac::Asset;
pub use stac::ItemCollection;

#[derive(Debug, Clone, PartialEq)]
pub struct Layer {
    source: PathBuf,
    pub(crate) layer_pos: usize,
    pub(crate) time_pos: usize,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterDataBlock<T> {
    pub data: RasterData<T>,
    pub metadata: RasterMetadata,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterBlock {
    pub block_index: usize,
    pub read_window: ReadWindow,
    pub overlap_size: usize,
    pub geo_transform: GeoTransform,
    pub overlap: Overlap,
    pub shape: RasterDataShape,
    pub epsg_code: usize,
    pub raster_metadata: RasterMetadata,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterMetadata {
    pub layers: Vec<Layer>,
    pub shape: RasterDataShape,
    pub(crate) block_size: BlockSize,
    pub epsg_code: u32,
    pub geo_transform: GeoTransform,
    pub overlap_size: usize,
    pub date_indices: Vec<DateType>,
    pub layer_indices: Vec<String>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterDatasetIter<'a> {
    pub parent: &'a RasterDataset,
    pub block: &'a RasterBlock,
    pub iter_index: usize,
}

impl Drop for RasterDataset {
    fn drop(&mut self) {
        for tmp_layer in &self.tmp_layers {
            let to_remove = tmp_layer;
            fs::remove_file(to_remove).unwrap();
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct DataSource {
    pub source: PathBuf,
    pub bands: Vec<usize>,
    // pub names: Vec<String>,
}

pub struct DataSourceBuilder {
    pub source: PathBuf,
    pub bands: Vec<usize>,
    pub layer_names: Vec<String>,
    pub date_indices: Vec<DateType>,
}

impl DataSourceBuilder {
    
    pub fn from_file(file_name: &PathBuf) -> Self {
        let source = file_name;

        let n_bands = DataSourceBuilder::get_n_bands(file_name);
        let bands: Vec<usize> = (1..n_bands + 1).collect();

        let layer_names = (0..n_bands)
            .map(|layer_index| format!("layer_{:?}", layer_index))
            .collect::<Vec<String>>();
        let date_indices = vec![DateType::Index(0)];

        DataSourceBuilder {
            source: source.clone(),
            bands,
            layer_names,
            date_indices,
        }
    }
    pub fn band_dimension(self, dimension: UtilsDimension) -> Self {
        let n_bands = DataSourceBuilder::get_n_bands(&self.source);

        let layer_names = match dimension {
            UtilsDimension::Layer => self.layer_names,
            UtilsDimension::Time => vec!["Layer_0".to_string()],
        };

        let date_indices = match dimension {
           UtilsDimension::Layer => self.date_indices,
            UtilsDimension::Time => (0..n_bands)
                .map(DateType::Index)
                .collect::<Vec<DateType>>(),
        };

        DataSourceBuilder {
            source: self.source.clone(),
            bands: self.bands,
            layer_names,
            date_indices,
        }
    }

    pub fn set_dates(self, dates: Vec<DateType>) -> Self {
        let n_dates = self.date_indices.len();
        let n_dates_new = dates.len();
        assert_eq!(n_dates, n_dates_new);

        DataSourceBuilder {
            source: self.source.clone(),
            bands: self.bands,
            layer_names: self.layer_names,
            date_indices: dates,
        }
    }
    pub fn set_names(self, names: Vec<&str>) -> Self {
        let n_names = self.layer_names.len();
        let n_names_new = names.len();
        assert_eq!(n_names, n_names_new);

        DataSourceBuilder {
            source: self.source.clone(),
            bands: self.bands,
            layer_names: names.iter().map(|n| n.to_string()).collect(),
            date_indices: self.date_indices,
        }
    }
    

    pub fn build(self) -> DataSource {
        DataSource {
            source: self.source,
            bands: self.bands,
        }
    }
    pub fn bands(mut self, bands: Vec<usize>) -> Self {
        self.bands = bands;
        self
    }
    fn get_n_bands(source: &PathBuf) -> usize {
        let ds = Dataset::open(source).unwrap();
        ds.raster_count() 
    }
}

use num_traits::Float;
use std::ops::Div;



impl<'a> RasterDatasetBuilder {
    pub fn from_scratch<T>(extent: Extent, resolution: T, epsg_code: u32, block_size: BlockSize) -> RasterDataset
    where T: ToPrimitive + Debug,
          
    {
        
        debug!("Working here...");
        debug!("Extent: {:?}", extent);
        let resolution = resolution.to_f64().expect("Unable to convert");
        debug!("Resolution {:?}", resolution);
        let layers = Vec::new();
        let rows = ((extent.ymax - extent.ymin)/resolution) as usize;
        let cols = ((extent.xmax - extent.xmin)/resolution) as usize;
        debug!("Creating raster with rows: {rows:?} and cols: {cols:?}");
        
        let shape = RasterDataShape{times: 1, layers: 1, rows,cols};
        let geo_transform = GeoTransform { 
        x_ul: extent.xmin ,
        x_res: resolution,
        x_rot: 0.,
        y_ul: extent.ymin,
        y_rot: 0.,
        y_res: resolution,
        };
        let overlap_size = 0;
        let date_indices = Vec::new();
        let layer_indices = Vec::new();
        let metadata = RasterMetadata { layers: layers.clone() , shape , block_size , epsg_code , geo_transform , overlap_size , date_indices , layer_indices  };
        let tmp_layers = Vec::new();
        let image_size = ImageSize {rows, cols};
        
        let n_block_cols = round::ceil(image_size.cols as f64 / block_size.cols as f64, 0) as usize;
        let n_block_rows = round::ceil(image_size.rows as f64 / block_size.rows as f64, 0) as usize;
        let n_blocks = n_block_rows*n_block_cols;
        let mut blocks: Vec<RasterBlock> = Vec::new();
        (0..n_blocks).for_each(|i| {
            let block_attributes = block_from_id_(i, shape, &layers, epsg_code as usize, image_size, block_size, overlap_size, geo_transform);
            blocks.push(block_attributes)
        });
        
        
        
        let rds = RasterDataset{
             metadata,
             blocks,
             tmp_layers
         };
        
        //debug!("rds has {} cols {} rows", n_block_cols, n_block_cols);
        debug!("rds from scratch: {rds}");
        rds
        
    }
    pub fn from_source(data_source: &DataSource) -> RasterDatasetBuilder {
        RasterDatasetBuilder::from_sources(&vec![data_source.to_owned()])
    }

 pub fn from_item_collection(feature_collection: &ItemCollection) -> Self {
        RasterDatasetBuilder::from_stac_query(feature_collection)
    }

    /// Create a rasterdataset from a stac query. Will find the smallest exted of all the combined images and mosaic if necesary
    pub fn from_stac_query(feature_collection: &ItemCollection) -> Self {
        
      
        let bands_vec = Vec::new();

        // default behaviour
        let composition_bands = UtilsDimension::Time;
        let composition_sources = UtilsDimension::Layer; // as many times as sources
        let geo_transform = GeoTransform::default();
        let image_size = ImageSize::default();
        let block_size = BlockSize::default();
        let overlap_size = 0;
        let first_item_assets = feature_collection.items[0].assets.values().next().unwrap();
        let epsg =
            RasterDatasetBuilder::get_epsg_code(&PathBuf::from(first_item_assets.href.clone()));
        let resolution =
            RasterDatasetBuilder::get_resolution(&PathBuf::from(first_item_assets.href.clone()));
        
        
        let sources: Vec<PathBuf> = Vec::new();
        let mut date_indices: Vec<DateType> = Vec::new();
        let mut bands = HashMap::new();
        for item in feature_collection.items.iter() {
            let date = item.properties.datetime.unwrap();
            date_indices.push(DateType::Date(date.into()));
            for (name, _) in item.assets.iter() {
                let title = name.to_owned();
                let band = 1; // FIX
                bands.insert(title, vec![band]);
            }
        }
        date_indices.sort();

        let date_indices = date_indices.into_iter().collect();
        RasterDatasetBuilder {
            sources,
            bands,
            bands_vec,
            epsg,
            overlap_size,
            block_size,
            composition_bands,
            composition_sources,
            image_size,
            geo_transform,
            date_indices,
            resolution,
            feature_collection: Some(feature_collection.clone()),
        }
    }

    pub fn from_sources(data_sources: &Vec<DataSource>) -> RasterDatasetBuilder {
        
        let mut sources: Vec<PathBuf> = Vec::new();
        let mut bands_vec = Vec::new();
        let bands = HashMap::new();

        let geo_transform = GeoTransform::default();
        let image_size = ImageSize::default();
        let block_size = BlockSize::default();

        let epsg = RasterDatasetBuilder::get_epsg_code(&data_sources[0].source);
        let overlap_size = 0;

        // default behaviour for compositions
        let composition_bands = UtilsDimension::Layer;
        let composition_sources = UtilsDimension::Time; // as many times as sources

        for data_source in data_sources {
            sources.push(data_source.source.clone());
            bands_vec.push(data_source.bands.clone());
        }

        let date_indices = (0..sources.len()).map(DateType::Index).collect();
        let resolution = RasterDatasetBuilder::get_resolution(&data_sources[0].source);
        RasterDatasetBuilder {
            sources,
            bands,
            bands_vec,
            epsg,
            overlap_size,
            block_size,
            composition_bands,
            composition_sources,
            image_size,
            geo_transform,
            date_indices,
            resolution,
            feature_collection: None,
        }
    }

    /// Set the desired epsg code for the rasterdataset.
    pub fn set_epsg(mut self, epsg_code: u32) -> Self {
        self.epsg = epsg_code;
        self
    }

    pub fn set_image_size(mut self, image_size: ImageSize) -> Self {
        self.image_size = image_size;
        self
    }

    //Set the desired geo_transform
    pub fn set_geo_transform(mut self, geo_transform: GeoTransform) -> Self {
        self.geo_transform = geo_transform;
        self
    }

    /// Set the desired resolution for the rasterdataset.
    pub fn set_resolution(mut self, resolution: ImageResolution) -> Self {
        self.resolution = resolution;
        self
    }
    fn get_resolution(source: &PathBuf) -> ImageResolution {
        let source: &Path = Path::new(&source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let geo_transform = ds.geo_transform().unwrap();
        ImageResolution {
            x: geo_transform[1],
            y: geo_transform[5],
        }
    }

    fn get_geotransform(source: &PathBuf) -> GeoTransform {
        let source: &Path = Path::new(&source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let geo_transform = ds.geo_transform().unwrap();
        GeoTransform {
            x_ul: geo_transform[0],
            x_res: geo_transform[1],
            x_rot: geo_transform[2],
            y_ul: geo_transform[3],
            y_rot: geo_transform[4],
            y_res: geo_transform[5],
        }
    }

    fn get_extent(source: &PathBuf, target_epsg: u32) -> Extent {
        let source_ds = Dataset::open(source).unwrap_or_else(|_| panic!("Unable to open {:?}", source));
        
        let ref_orig = source_ds.spatial_ref().unwrap();
        let ref_target = SpatialRef::from_epsg(target_epsg).unwrap();
        
        let gt = Self::get_geotransform(source);
        
        let extent = if ref_orig == ref_target {
            let is = Self::get_image_size(source);
            let xmin = [gt.x_ul];
            let xmax = [xmin[0] + gt.x_res * is.cols as f64];
            let ymax = [gt.y_ul];
            let ymin = [ymax[0] + gt.y_res * is.rows as f64];
            Extent {
                xmin: xmin[0],
                ymin: ymin[0],
                xmax: xmax[0],
                ymax: ymax[0],
                }
            }
            else {
            let new_source = create_temp_file("vrt");
            let mut cmd =Command::new("gdalwarp");
            cmd
                .args(["-t_srs", &format!("EPSG:{}",target_epsg )])
                .arg("-q")
                .arg(source)
                .arg(&new_source);
                
            cmd.spawn()
                .expect("failed to start creating vrt")
                .wait()
                .expect("failed to wait for the vrt");
            let new_src = PathBuf::from(new_source);
            let gt = Self::get_geotransform(&new_src);
            let is = Self::get_image_size(&new_src);
            let xmin = [gt.x_ul];
            let ymax = [gt.y_ul];
            let ymin = [ymax[0] + gt.y_res * is.rows as f64];
            let xmax = [xmin[0] + gt.x_res * is.cols as f64];
                
            
        Extent {
            xmin: xmin[0],
            ymin: ymin[0],
            xmax: xmax[0],
            ymax: ymax[0],
        }    
        

        
        };
        
        extent
    }

    fn get_epsg_code(source: &PathBuf) -> u32 {
        
        let source: &Path = Path::new(source);
        
        let ds: Dataset = Dataset::open(source).unwrap();
        let spatial_ref = ds.spatial_ref().unwrap();
        let epsg_code = spatial_ref.auth_code().unwrap_or(3755); //this is just a workaround. I need to implement OSRFindMatches in gdal
        epsg_code.try_into().unwrap()
    }

    fn get_image_size(source: &PathBuf) -> ImageSize {
        let source: &Path = Path::new(source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let size = ds.raster_size(); // x, y
        ImageSize {
            rows: size.1,
            cols: size.0,
        }
    }

    fn get_blocks(
        &'a self,
        block_shape: RasterDataShape,
        layers: &[Layer],
        epsg_code: usize,
    ) -> Vec<RasterBlock> {
        let n_blocks = self.n_blocks();
        let mut blocks: Vec<RasterBlock> = Vec::new();
        (0..n_blocks).for_each(|i| {
            let block_attributes = self.block_from_id(i, block_shape, layers, epsg_code);
            blocks.push(block_attributes)
        });
        blocks
    }

    fn n_block_cols(&self) -> usize {
        let image_size = self.image_size;
        let block_size = self.block_size;
        round::ceil(image_size.cols as f64 / block_size.cols as f64, 0) as usize
    }

    fn n_block_rows(&self) -> usize {
        let image_size = self.image_size;
        let block_size = self.block_size;
        round::ceil(image_size.rows as f64 / block_size.rows as f64, 0) as usize
    }

    fn n_blocks(&self) -> usize {
        self.n_block_cols() * self.n_block_rows()
    }

    // Images are chuncked in blocks. This function calculates
    // how many columns and rows of blocks an image has.
    // i.e a 1024x1024 image chuncked in 512x512 blocks
    // will have 2 block columns.
    fn block_col_row(&self, id: usize) -> (usize, usize) {
        let block_row = id / self.n_block_cols();
        let block_col = id - (block_row * self.n_block_cols());

        (block_col, block_row)
    }

    /// Creates a RasterBlock object based on the given parameters.
    ///
    /// The function calculates the necessary parameters and constructs a RasterBlock
    /// object using the given inputs and other attributes of the self object.
    ///
    /// # Arguments
    ///
    /// * `id` - The ID of the block.
    /// * `block_shape` - The shape of the raster data.
    /// * `_layers` - A vector of layers (unused in the function).
    /// * `epsg_code` - The EPSG code representing the coordinate reference system.
    ///
    /// # Returns
    ///
    /// A RasterBlock object representing the block.
    ///

    fn block_from_id(
        &'a self,
        id: usize,
        block_shape: RasterDataShape,
        _layers: &[Layer],
        epsg_code: usize,
    ) -> RasterBlock {
        let tile_col = self.block_col_row(id).0;
        let tile_row = self.block_col_row(id).1;
        let overlap = get_overlap(
            self.image_size,
            self.block_size,
            self.block_col_row(id),
            self.overlap_size,
        );

        let ul_x = (self.block_size.cols * tile_col) as isize;
        let ul_y = (self.block_size.rows * tile_row) as isize;

        // now compute with overlap
        let ul_x_overlap = ul_x - overlap.left as isize;
        let ul_y_overlap = ul_y - overlap.top as isize;

        let lr_x_overlap = min(
            self.image_size.cols as isize,
            ul_x + self.block_size.cols as isize + overlap.right as isize,
        );
        let lr_y_overlap = min(
            self.image_size.rows as isize,
            ul_y + self.block_size.rows as isize + overlap.bottom as isize,
        );

        let win_width = lr_x_overlap - ul_x_overlap;
        let win_height = lr_y_overlap - ul_y_overlap;

        let arr_width = win_width;
        let arr_height = win_height;

        let read_window = ReadWindow {
            offset: Offset {
                cols: ul_x_overlap,
                rows: ul_y_overlap,
            },
            size: Size {
                cols: arr_width,
                rows: arr_height,
            },
        };

        let block_geo_transform = self.get_block_gt(read_window);
        let empty_metadata: RasterMetadata = RasterMetadata::new(); 
        RasterBlock {
            block_index: id,
            read_window,
            overlap_size: self.overlap_size,
            geo_transform: block_geo_transform,
            overlap,
            shape: block_shape,
            epsg_code,
            raster_metadata: empty_metadata,
        }
    }

    fn get_block_gt(&self, read_window: ReadWindow) -> GeoTransform {
        let x_ul_image = self.geo_transform.x_ul;
        let y_ul_image = self.geo_transform.y_ul;

        let x_res = self.geo_transform.x_res;
        let y_res = self.geo_transform.y_res;
        let x_pos = read_window.offset.cols;
        let y_pos = read_window.offset.rows;

        let x_ul_block = x_ul_image + x_res * x_pos as f64; // to fix! will break with overlap
        let y_ul_block = y_ul_image + y_res * y_pos as f64; // to fix! will break with overlap
        GeoTransform {
            x_ul: x_ul_block,
            x_res,
            x_rot: self.geo_transform.x_rot,
            y_ul: y_ul_block,
            y_rot: self.geo_transform.x_rot,
            y_res,
        }
    }

    /// sets the block size of the raster dataset
    pub fn block_size(mut self, block_size: BlockSize) -> RasterDatasetBuilder {
        self.block_size = block_size;
        self
    }
    pub fn source_composition_dimension(
        mut self,
        composition_dimension: UtilsDimension,
    ) -> RasterDatasetBuilder {
        self.composition_sources = composition_dimension;
        self
    }

    pub fn band_composition_dimension(
        mut self,
        composition_dimension: UtilsDimension,
    ) -> RasterDatasetBuilder {
        self.composition_bands = composition_dimension;
        self
    }

    pub fn set_date_indices(mut self, date_indices: &[DateType]) -> RasterDatasetBuilder {
        self.date_indices = date_indices.to_vec();
        self
    }

    pub fn overlap_size(mut self, overlap_size: usize) -> RasterDatasetBuilder {
        self.overlap_size = overlap_size;
        self
    }

    pub fn set_template(mut self, other: &RasterDataset) -> RasterDatasetBuilder {
        let current_epsg = RasterDatasetBuilder::get_epsg_code(&self.sources[0]);
        let target_epsg = self.epsg;
        if current_epsg != target_epsg {
            warn!("Warning epsg was set, but does not match the epsg of the template.");
            self.epsg = other.metadata.epsg_code;
        }

        self.image_size = ImageSize {
            rows: other.metadata.shape.rows,
            cols: other.metadata.shape.cols,
        };
        self.geo_transform = other.metadata.geo_transform;

        self
    }
    fn get_max_extent(extents: Vec<Extent>) -> Extent {
        let xmin = extents
            .iter()
            .map(|ex| ex.xmin)
            .min_by(|i, j| i.partial_cmp(j).unwrap())
            .unwrap();
        let ymin = extents
            .iter()
            .map(|ex| ex.ymin)
            .min_by(|i, j| i.partial_cmp(j).unwrap())
            .unwrap();
        let xmax = extents
            .iter()
            .map(|ex| ex.xmax)
            .max_by(|i, j| i.partial_cmp(j).unwrap())
            .unwrap();
        let ymax = extents
            .iter()
            .map(|ex| ex.ymax)
            .max_by(|i, j| i.partial_cmp(j).unwrap())
            .unwrap();
        Extent {
            xmin,
            ymin,
            xmax,
            ymax,
        }
    }
    pub fn bands(mut self, bands: HashMap<String, Vec<usize>>) -> Self {
        self.bands = bands;
        self
    }

    pub fn build(mut self) -> RasterDataset {
        let raster_dataset = match self.feature_collection {
            None => {
                // todo!(); // I think it would be better to use Datasetbuilder to construct a FeatureCollection and just use that!
                let mut layers = Vec::new();
                let mut blocks = Vec::new();
                let mut tmp_layers: Vec<PathBuf> = Vec::new();
                for source in self.sources.iter_mut() {
                    let current_epsg = RasterDatasetBuilder::get_epsg_code(source);
                    let target_epsg = self.epsg;

                    let current_resoultion = RasterDatasetBuilder::get_resolution(source);
                    let target_resolution = self.resolution;

                    if current_epsg != target_epsg {
                        *source = warp(source.to_path_buf(), None, target_epsg);
                        tmp_layers.push(source.clone());
                    }

                    
                    if current_resoultion != target_resolution {
                        *source = change_res(source.to_path_buf(), target_resolution);
                        tmp_layers.push(source.clone());
                    }
                    
                    
                }

                for source in self.sources.iter_mut() {
                    
                    
                    let current_gt = RasterDatasetBuilder::get_geotransform(source);
                    let target_gt = self.geo_transform;
                    
                    
                    
                    

                    if (current_gt != target_gt) & (target_gt != GeoTransform::default()) {
                        let current_extent = RasterDatasetBuilder::get_extent(source, self.epsg );
                        let x_ll = target_gt.x_ul + (self.image_size.cols as f64 * target_gt.x_res);
                        let y_ll = target_gt.y_ul + (self.image_size.rows as f64 * target_gt.y_res);
                    
                        let target_extent = Extent {
                                xmin: target_gt.x_ul,
                                ymin: y_ll,
                                xmax: x_ll,
                                ymax: target_gt.y_ul,
                         };        
                        let te =  if current_extent != target_extent {
                        Extent {
                            xmin: target_gt.x_ul,
                            ymin: y_ll,
                            xmax: x_ll,
                            ymax: target_gt.y_ul,
                        }
                           
                        }    
                        else {
                            current_extent
                        };
             
                        
                        let tr = ImageResolution {
                            x: target_gt.x_res,
                            y: target_gt.y_res,
                        };

                        *source = warp_with_te_tr(source.to_path_buf(), &te, tr);
                        tmp_layers.push(source.clone());
                        
                        
                    }
                }

                let geo_transform = RasterDatasetBuilder::get_geotransform(&self.sources[0]);

                let image_size = RasterDatasetBuilder::get_image_size(&self.sources[0]);

                self.geo_transform = geo_transform;
                self.image_size = image_size;

                let n_rows = image_size.rows;
                let n_cols = image_size.cols;
                for source in self.sources.iter() {
                    let gt = RasterDatasetBuilder::get_geotransform(source);
                    let is = RasterDatasetBuilder::get_image_size(source);

                    if gt != geo_transform {
                        panic!("Sources have different geo_transforms!");
                    }
                    
                    
                    if is != image_size {
                        panic!("Sources have different size!")
                    }
                }
                let mut time_pos = 1;
                let mut layer_pos = 1;

                for (source_idx, source) in self.sources.iter().enumerate() {
                    let bands = self.bands_vec[source_idx].clone();

                    for band in bands.iter() {
                        let source = PathBuf::from(source);
                        let new_source = extract_band(&source, *band);
                        tmp_layers.push(new_source.clone());
                        let source = new_source;
                        let layer = Layer {
                            source,
                            time_pos: time_pos - 1,
                            layer_pos: layer_pos - 1,
                        };
                        layers.push(layer);

                        match self.composition_bands {
                            UtilsDimension::Layer => layer_pos += 1,
                            UtilsDimension::Time => time_pos += 1,
                        }
                    }

                    match self.composition_sources {
                        UtilsDimension::Layer => {
                            if self.composition_bands != UtilsDimension::Layer {
                                layer_pos += 1
                            }
                        }
                        UtilsDimension::Time => {
                            if self.composition_bands != UtilsDimension::Time {
                                time_pos += 1
                            }
                        }
                    }

                    match self.composition_sources {
                        UtilsDimension::Layer => time_pos = 1,
                        UtilsDimension::Time => layer_pos = 1,
                    }
                }

                let n_times = layers.iter().map(|l| l.time_pos).max().unwrap() + 1;
                let n_layers = layers.iter().map(|l| l.layer_pos).max().unwrap() + 1;

                let mut layer_names = Vec::new();
                for layer_index in 0..n_layers {
                    let name = format!("Layer_{:?}", layer_index);
                    layer_names.push(name)
                }

                let block_shape = RasterDataShape {
                    times: n_times,
                    layers: n_layers,
                    rows: n_rows,
                    cols: n_cols,
                };

                blocks.extend(self.get_blocks(block_shape, &layers, self.epsg.try_into().unwrap()));

                let metadata = RasterMetadata {
                    layers,
                    shape: block_shape, //FIXME?
                    block_size: self.block_size,
                    epsg_code: self.epsg,
                    geo_transform,
                    overlap_size: self.overlap_size,
                    date_indices: self.date_indices,
                    layer_indices: layer_names,
                };

                RasterDataset {
                    metadata,
                    blocks,
                    tmp_layers,
                }
            }
            Some(ref feature_collection) => {
                
                
                let mut sources = Vec::new();
                for item in &feature_collection.items {
                    for asset in item.assets.values() {
                        let href = &asset.href;
                        sources.push(PathBuf::from(href));
                    }
                }
                let mut layers: Vec<Layer> = Vec::new();
                let mut tmp_layers: Vec<PathBuf> = Vec::new();
                let mut blocks = Vec::new();
                // They have to have same projection!
                // get the full extent (min xmin and min ymin; max xmax and max ymax)
                // let mut extents = Vec::new();
                
                
                let extents: Vec<_> = sources.par_iter().map(|source| Self::get_extent(source, self.epsg)).collect();

                let mut extent = Self::get_max_extent(extents);
                let original_times_indices = get_sorted_datetimes(feature_collection);
                
                let target_time_indices = unique_datetimes_in_range(original_times_indices);          
                
                let asset_names = get_asset_names(feature_collection);
                

                // asset names go into the rasterdataset sorted alphabetically!
                for (time_idx, time) in target_time_indices.iter().enumerate() {
                    let mut layer_idx = 0;
                    
                    let date_items = get_items_for_date(feature_collection, time);
                    
                        
                    for asset_name in asset_names.iter() {
                        
                        //let asset_bands = self.bands.get(asset_name).unwrap();
                        let asset_bands = [1];
                        for _ in asset_bands.iter() {
                            let sources = get_sources_for_asset(&date_items, asset_name);
                            

                            let date_mosaic_tmp = PathBuf::from(create_temp_file("vrt"));
                            
                            mosaic(&sources, &date_mosaic_tmp, self.epsg).unwrap();
                            let mut single_band = date_mosaic_tmp;
                            let current_epsg = RasterDatasetBuilder::get_epsg_code(&single_band);        
                
                            let target_epsg = self.epsg;
                             if current_epsg != target_epsg {
                                 single_band = warp(single_band, None, target_epsg);
                                 tmp_layers.push(single_band.clone());
                             }
                            

                             let current_resoultion =
                                 RasterDatasetBuilder::get_resolution(&single_band);
                             let target_resolution = self.resolution;
                             let mut tr = if current_resoultion == target_resolution {
                                 current_resoultion
                             } else {
                                 target_resolution
                             };

                             let current_gt = RasterDatasetBuilder::get_geotransform(&single_band);
                             let target_gt = self.geo_transform;

                             if (current_gt != target_gt) & (target_gt != GeoTransform::default()) {
                                 let x_ll = target_gt.x_ul
                                     + (self.image_size.cols as f64 * target_gt.x_res);
                                 let y_ll = target_gt.y_ul
                                     + (self.image_size.rows as f64 * target_gt.y_res);
                                 extent = Extent {
                                     xmin: (target_gt.x_ul * 100.).round() / 100.,
                                     ymin: (y_ll * 100.).round() / 100.,
                                     xmax: (x_ll * 100.).round() / 100.,
                                     ymax: (target_gt.y_ul * 100.).round() / 100.,
                                 };
                                 tr = ImageResolution {
                                     x: (target_gt.x_res * 100.).round() / 100.,
                                     y: (target_gt.y_res * 100.).round() / 100.,
                                 };
                             }

                            single_band = warp_with_te_tr(single_band, &extent, tr);
                            
                            
                            tmp_layers.push(single_band.clone());
                             let layer = Layer {
                                 source: single_band,
                                 time_pos: time_idx,
                                 layer_pos: layer_idx,
                             };
                             layers.push(layer);

                            layer_idx += 1;
                        }
                    }
                }
                    
                // update image size and geotransform fields.
                let image_size = Self::get_image_size(&layers[0].source);
                self.image_size = image_size;
                let geo_transform = Self::get_geotransform(&layers[0].source);
                self.geo_transform = geo_transform;

                // Start computing the metadata
                let n_times = target_time_indices.len();
                // self.date_indices = (0..sources.len()).map(|i| DateType::Index(i)).collect();

                let mut n_layers = 0;
                for (_, v) in self.bands.clone() {
                    n_layers += v.len();
                }
                // let n_layers = layers.iter().map(|l| l.layer_pos).max().unwrap() + 1;
                let block_shape = RasterDataShape {
                    times: n_times,
                    layers: n_layers,
                    rows: image_size.rows,
                    cols: image_size.cols,
                };
                blocks.extend(self.get_blocks(block_shape, &layers, self.epsg.try_into().unwrap()));
                let target_time_indices: Vec<DateType> = target_time_indices.into_iter().map(DateType::Date).collect();
                
                let metadata = RasterMetadata {
                    layers,
                    shape: block_shape, //FIXME?
                    block_size: self.block_size,
                    epsg_code: self.epsg,
                    geo_transform,
                    overlap_size: self.overlap_size,
                    date_indices: target_time_indices,
                    layer_indices: asset_names,
                };

                RasterDataset {
                    metadata,
                    blocks,
                    tmp_layers,
                }
            }
        };
        raster_dataset
    }
}

impl Default for RasterMetadata {
    fn default() -> Self {
        Self::new()
    }
}

impl RasterMetadata {
    pub fn new() -> Self {
        let layers = Vec::new();
        let shape = RasterDataShape { times: 0, layers: 0, rows: 0, cols: 0 };
        let block_size = BlockSize { rows: 0, cols: 0 };
        let epsg_code = 4326;
        let geo_transform = GeoTransform { x_ul: 0., x_res: 0., x_rot:0. , y_ul:0. , y_rot: 0., y_res:0.  };
        let overlap_size = 0;
        let date_indices = Vec::new();
        let layer_indices = Vec::new();
        
        
        RasterMetadata {
           layers,
           shape,
           block_size,
           epsg_code,
           geo_transform,
           overlap_size,
           date_indices,
           layer_indices, 
        }
    }
}

impl RasterDataset {
    /// An iterator over the [RasterBlock]s of a [RasterDataset].

    pub fn iter(&self) -> RasterDatasetIter<'_> {
        let iter_index = 0;
        RasterDatasetIter {
            parent: self,
            block: &self.blocks[iter_index],
            iter_index,
        }
    }

    async fn read_block_hybrid<'a, T>(
    block: RasterBlock,
    metadata: RasterMetadata,
    sender: mpsc::Sender<(usize, Array4<T>, RasterBlock, OwnedSemaphorePermit)>,
    semaphore_data: Arc<Semaphore>,
    progress_bar: Arc<Mutex<Bar>>,
) 
where
        T: gdal::raster::GdalType + Copy + std::fmt::Debug + num_traits::identities::Zero + std::marker::Send + 'a,
    {
        
        let permit = semaphore_data.acquire_owned().await.unwrap();
        let block_id = block.block_index;
        
        let read_window = block.read_window;
        let rows = read_window.size.rows as usize; // + overlap.top + overlap.bottom;
        let cols = read_window.size.cols as usize; //+ overlap.left + overlap.right;
        let data_shape = (
            block.shape.times,
            block.shape.layers,
            rows,
            cols,
        );
        let mut data = Array4::uninit(data_shape);
        for layer in &metadata.layers {
            let raster_path = Path::new(&layer.source);
            let band_index: isize = 1;
            let ds = Dataset::open(raster_path).unwrap();
            let raster_band = ds.rasterband(band_index as usize).unwrap();
            let window = (read_window.offset.cols, read_window.offset.rows);
            let window_size = (cols, rows);
            let array_size = window_size;
            let e_resample_alg = None;
            let layer_data = raster_band
                .read_as::<T>(window, window_size, array_size, e_resample_alg)
                .unwrap().to_array().unwrap();
            

            let slice = s![
                layer.time_pos,
                layer.layer_pos,
                ..,
                .. //overlap.top..read_window.size.rows as usize,
                   //overlap.left..read_window.size.cols as usize,
            ];

            
            layer_data.assign_to(data.slice_mut(slice));
        }
        
          
        let data = data.map(|elem| unsafe { elem.assume_init() });
        update_arc_mutex_progress_bar(progress_bar);
        
        // Send the id, data and semaphore to the channel to notify the CPU-intensive task
    sender
        .send((block_id, data, block,permit))
        .await
        .expect("Failed to send id to channel");
        
    
    }
    #[allow(clippy::too_many_arguments)]    
    fn process_block_hybrid<T,U>(id: usize, worker: fn(&Array4<T>) -> Array4<U>, data: Array4<T>, block: RasterBlock ,permit: OwnedSemaphorePermit, _n_cpus: usize, progress_bar: Arc<Mutex<Bar>>, out_file: &Path) -> Vec<PathBuf>
    where
        T: gdal::raster::GdalType + Copy + std::fmt::Debug + num_traits::identities::Zero,
U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
 
     {
        
        let bid = id;
        
        let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
        let result = worker(&data);
        drop(permit);
        drop(data);
        
        let mut block_fns: Vec<PathBuf> = Vec::new();
        for (tid, result3) in result.axis_iter(Axis(0)).enumerate() {
            let block_fn =
                            out_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
            
            block.write3(result3.to_owned(), &block_fn);
            block_fns.push(block_fn);
        };
        update_arc_mutex_progress_bar(progress_bar);
        block_fns    
    }
    
        

    /// Applays a worker function over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.
    pub async fn process_hybrid<'a, 'b, 'c, T, U>(&'c self, worker: fn(&Array4<T>) -> Array4<U>, n_cpus: usize, n_data_blocks: usize, _out_fn: PathBuf) -> Result<()>
    where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug + std::marker::Send + 'a + 'b + 'static,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug + 'c +'a + 'b + 'static,
    {
        let start_time = Instant::now();
    
        let blocks_to_process: Vec<usize> = (0..self.blocks.len()).collect();
        let n_blocks = self.blocks.len();
       // create progress bars for reading and processing tasks
       let (bar_read, bar_processed) = create_progress_bars(n_blocks);
       // Wrap the progress bars in Arc and Mutex to be able to safely mutate
       // (update) inside the async/thread functions.
       let bar_read = Arc::new(Mutex::new(bar_read));
       let bar_process = Arc::new(Mutex::new(bar_processed));


        
        let (sender, mut receiver) = mpsc::channel::<(usize, Array4<T>, RasterBlock, OwnedSemaphorePermit)>(blocks_to_process.len());
        let sempahore_data = Arc::new(Semaphore::new(n_data_blocks));
        // Run async tasks using Tokio
        for &block_id in &blocks_to_process {
            let sender_clone = sender.clone();
            let block = self.blocks[block_id].clone();
            let metadata = self.metadata.clone();
            task::spawn(Self::read_block_hybrid(
                block,
                metadata.clone(),
                sender_clone,
                sempahore_data.clone(),
                bar_read.clone(),
             ));
        }

        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();
        let mut handles_normal = Vec::new();
        let mut handles_last = Vec::new();
        let tmp_file = PathBuf::from(create_temp_file("vrt"));
        
        while let Some((block_id, data, block,permit)) = receiver.recv().await {
            // Spawn a new thread for each CPU-intensive task if permits are available
            let bar_process = bar_process.clone();
            let tmp_file = tmp_file.clone();
            if handles_normal.len() < n_blocks - 1 {
                pool.spawn(move || {
                    
                    Self::process_block_hybrid(block_id, worker, data, block,permit, n_cpus, bar_process, &tmp_file);
                });
            handles_normal.push(());
            }
            else {
                let handle = pool.install(move || {
                    
                    Self::process_block_hybrid(block_id, worker, data, block,permit, n_cpus, bar_process, &tmp_file) 
                });
                handles_last.push(handle);
            }
            if is_last(&handles_last) {
            let elapsed = start_time.elapsed();
            std::thread::sleep(Duration::from_secs(1));
            eprintln!();

            let msg = format!("\n{}", format!("{:19} : {elapsed:?}", "Total time: ").red());
             println!("{msg}");       
                break;
            }
    }
        // we need to get the files from tmp that match the tmp file and store them as vec of vec; where the first index is time. 
        for time_index in 0..self.metadata.shape.times{
            let match_str =  &format!("/tmp/{}_{:?}*.tif",tmp_file.file_stem().unwrap().to_str().unwrap(), time_index);
            
            let _time_fns = glob(match_str).unwrap();
            
        }
        Ok(())
    }
    pub fn process_t<T, U>(&self, worker: fn(&RasterDataBlock<T>) -> Array4<U>, n_cpus: usize, out_file: &Path)
    where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));
        
        println!("About to start:");
        
        let handle = pool.install(|| {
            self.blocks.to_owned().into_par_iter().map(
                |raster_block: RasterBlock| -> Vec<PathBuf> {
                    //(Vec<usize>, Vec<PathBuf>) {
                    let bid = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_data = self.read_block::<T>(bid);
                    let raster_data_block = RasterDataBlock {
                        data : block_data,
                        metadata : self.metadata.clone()
                    };
                    let result = worker(&raster_data_block);
                    
                    let mut block_fns: Vec<PathBuf> = Vec::new();
                    for (tid, result3) in result.axis_iter(Axis(0)).enumerate() {
                        let block_fn =
                            tmp_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
                        raster_block.write3(result3.to_owned(), &block_fn);
                        block_fns.push(block_fn);
                    }
                    block_fns
                },
            )
        });

        let collected: Vec<Vec<PathBuf>> = handle.collect();
        (0..self.metadata.shape.times)
            .into_par_iter()
            .for_each(|time_index| {
                let mut block_fns = Vec::new();
                for block in collected.iter() {
                    let block_fn = block[time_index].to_owned();
                    block_fns.push(block_fn);
                }
                let tmp_file = PathBuf::from(create_temp_file("vrt"));
                let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
                let time_fn = out_file.with_file_name(format!("{}_{}.tif", file_stem, time_index));
                mosaic(&block_fns, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");
                translate(&tmp_file, &time_fn).expect("Could not translate to geotiff");
                fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
                block_fns
                    .iter()
                    .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
            });
    }
    
    /// Applays a worker function over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.
    pub fn process<T, U>(&self, worker: fn(&Array4<T>) -> Array4<U>, n_cpus: usize, out_file: &Path)
    where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));
        
        let handle = pool.install(|| {
            self.blocks.to_owned().into_par_iter().map(
                |raster_block: RasterBlock| -> Vec<PathBuf> {
                    //(Vec<usize>, Vec<PathBuf>) {
                    let bid = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_data = self.read_block::<T>(bid);
                    let result = worker(&block_data);
                    
                    let mut block_fns: Vec<PathBuf> = Vec::new();
                    for (tid, result3) in result.axis_iter(Axis(0)).enumerate() {
                        let block_fn =
                            tmp_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
                        raster_block.write3(result3.to_owned(), &block_fn);
                        block_fns.push(block_fn);
                    }
                    block_fns
                },
            )
        });

        let collected: Vec<Vec<PathBuf>> = handle.collect();
        (0..self.metadata.shape.times)
            .into_par_iter()
            .for_each(|time_index| {
                let mut block_fns = Vec::new();
                for block in collected.iter() {
                    let block_fn = block[time_index].to_owned();
                    block_fns.push(block_fn);
                }
                let tmp_file = PathBuf::from(create_temp_file("vrt"));
                let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
                let time_fn = out_file.with_file_name(format!("{}_{}.tif", file_stem, time_index));
                mosaic(&block_fns, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");
                translate(&tmp_file, &time_fn).expect("Could not translate to geotiff");
                fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
                block_fns
                    .iter()
                    .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
            });
    }

    // Burn vector values into a rds.
    pub fn rasterize<T>(&self,vector_fn: &PathBuf, column_name: &str, n_cpus: usize,rasterize_options: Option<RasterizeOptions>, out_file: &Path)
    where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + num_traits::ToPrimitive+ Debug + num_traits::cast::NumCast,
    //     U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        debug!("Starting rasterize.");
        std::env::set_var("RAYON_NUM_THREADS", &format!("{n_cpus}"));
        
        let pool = rayon::ThreadPoolBuilder::new()
            .build()
            .unwrap();
        
        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));
              
        
        let handle = pool.install(|| {
            self.blocks.to_owned().into_par_iter().tqdm().map(
                |raster_block: RasterBlock| -> Vec<PathBuf> {
        let dataset = Dataset::open(vector_fn).expect("unable to open dataset");
                    //(Vec<usize>, Vec<PathBuf>) {
                    let mut layer = dataset.layer(0).unwrap();
                    let bid = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let mut result: Array4<i32> = RasterData::zeros((1,1,raster_block.read_window.size.cols as usize, raster_block.read_window.size.rows as usize));
                    for c in result.iter_mut() {*c = 1};
                    // what are my coordinates here?
                    let xmin = raster_block.geo_transform.x_ul;
                    let ymin = raster_block.geo_transform.y_ul;
                    let xmax = xmin + (raster_block.geo_transform.x_res * raster_block.read_window.size.cols as f64); 
                    let ymax = ymin + (raster_block.geo_transform.y_res * raster_block.read_window.size.rows as f64); 
                    
                    
                    // Create the bounding box geometry using the provided coordinates.
                    let geom = gdal::vector::Geometry::from_wkt(&format!(
                        "POLYGON (({} {} , {} {} , {} {} , {} {} , {} {}))",
                        xmin, ymin, xmin, ymax, xmax, ymax, xmax, ymin, xmin, ymin
                    )).unwrap();

                    // Apply the spatial filter to the layer.
                    layer.set_spatial_filter(&geom);

                    // let feature_count = layer.feature_count();
                    let mut geometries = Vec::new();
                    let mut burn_values = Vec::new();
                    
                    for feature in layer.features(){
                        let g = feature.geometry().unwrap().to_owned();
                            // Attempt to get the value of the "BVG1m" field
    let field_index = feature.field_index(&column_name).expect("Invalid column name,");
    if let Some(field_value) = feature.field(field_index).expect("Invalid column name!") {
        let v = match field_value {
            FieldValue::IntegerValue(i) => NumCast::from(i).unwrap(),
             FieldValue::RealValue(r) =>  NumCast::from(r).unwrap(),
            _ => panic!("The field to rasterize has to be numeric"),
        };
        burn_values.push(v);
    } else {
        panic!("{}",format!("Field {column_name} does not exist"));
    }
                        geometries.push(g);
                    }
                        
                    
                    
                    let mut block_fns: Vec<PathBuf> = Vec::new();
                    for (tid, _) in result.axis_iter(Axis(0)).enumerate() {
                        let block_fn =
                            tmp_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
                        
                        rasterize_and_write::<T>(&raster_block, &geometries, burn_values.clone(), rasterize_options, &block_fn).unwrap();
                        block_fns.push(block_fn);
                    }
                    block_fns
                },
            )
            
        });

        let collected: Vec<Vec<PathBuf>> = handle.collect();
        (0..self.metadata.shape.times)
            .into_par_iter()
            .for_each(|time_index| {
                
                let mut block_fns = Vec::new();
                for block in collected.iter() {
                    
                    let block_fn = block[time_index].to_owned();
                    block_fns.push(block_fn);
                }
                
                let tmp_file = PathBuf::from(create_temp_file("vrt"));
                let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
                let time_fn = out_file.with_file_name(format!("{}_{}.tif", file_stem, time_index));
                debug!("Doing mosaic {:?}", tmp_file);
                mosaic(&block_fns, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");
                debug!("Translate to tif {:?}", time_fn);
                translate_with_driver(&tmp_file, &time_fn, "GTiff").expect("Could not translate to geotiff");
                
                fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
                block_fns
                    .iter()
                    .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
            });
    }



    /// Applays a worker function over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.
    pub fn process_with_mask<T, U, V>(&self, other: &RasterDataset, worker: fn(&Array4<T>, &Array4<U>) -> Array4<V>, n_cpus: usize, out_file: &Path)
    where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        V: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    
    {
        std::env::set_var("RAYON_NUM_THREADS", &format!("{n_cpus}"));
        let pool = rayon::ThreadPoolBuilder::new()
            .build()
            .unwrap();
        


        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks.to_owned().into_par_iter()
               .zip(other.blocks.to_owned())
                .map(|(raster_block_data, _raster_block_mask)|  -> Vec<PathBuf> {
                    //(Vec<usize>, Vec<PathBuf>) {
                    let bid = raster_block_data.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_data = self.read_block::<T>(bid);
                    let mask = other.read_block::<U>(bid);
                    
                    let result = worker(&block_data, &mask);
                    let mut block_fns: Vec<PathBuf> = Vec::new();
                    for (tid, result3) in result.axis_iter(Axis(0)).enumerate() {
                        let block_fn =
                            tmp_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
                        raster_block_data.write3(result3.to_owned(), &block_fn);
                        block_fns.push(block_fn);
                    }
                    block_fns
                },
            )
        });

        let collected: Vec<Vec<PathBuf>> = handle.collect();
        (0..self.metadata.shape.times)
            .into_par_iter()
            .for_each(|time_index| {
                let mut block_fns = Vec::new();
                for block in collected.iter() {
                    let block_fn = block[time_index].to_owned();
                    block_fns.push(block_fn);
                }
                let tmp_file = PathBuf::from(create_temp_file("vrt"));
                let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
                let time_fn = out_file.with_file_name(format!("{}_{}.tif", file_stem, time_index));
                mosaic(&block_fns, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");
                translate(&tmp_file, &time_fn).expect("Could not translate to geotiff");
                fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
                block_fns
                    .iter()
                    .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
            });
    }

    pub fn mosiac(&self, n_cpus: usize) {
        std::env::set_var("RAYON_NUM_THREADS", &format!("{n_cpus}"));
        let pool = rayon::ThreadPoolBuilder::new()
            .build()
            .unwrap();
            let tmp_file = PathBuf::from(create_temp_file("vrt"));
        let handle = pool.install(|| {
            self.blocks.to_owned().into_par_iter().map(
                |raster_block: RasterBlock| -> Vec<PathBuf> {
                    let bid = raster_block.block_index;
                    
                    
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();

                    let block_data = self.read_block::<i16>(bid);
                    
                    let mut block_fns: Vec<PathBuf> = Vec::new();
                    for (tid, result3) in block_data.axis_iter(Axis(0)).enumerate() {
                        let block_fn =
                            tmp_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
                        raster_block.write3(result3.to_owned(), &block_fn);
                        block_fns.push(block_fn);
                        
                    }
                    block_fns
 
                    
             }
            )});
       let collected: Vec<Vec<PathBuf>> = handle.collect();
        
       let out_file = PathBuf::from("mosaic.tif");
        (0..self.metadata.shape.times)
            .into_par_iter()
            .for_each(|time_index| {
                let mut block_fns = Vec::new();
                for block in collected.iter() {
                    let block_fn = block[time_index].to_owned();
                    block_fns.push(block_fn);
                }
                let tmp_file = PathBuf::from(create_temp_file("vrt"));
                let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
                let time_fn = out_file.with_file_name(format!("{}_{}.tif", file_stem, time_index));
                mosaic(&block_fns, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");
                info!("Saving mosaic {:?}", time_fn);
                translate(&tmp_file, &time_fn).expect("Could not translate to geotiff");
                fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
                block_fns
                    .iter()
                    .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
            });
        
        
    }


    /// Applays a worker function that reduces the data dimensionality over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.
    ///
    pub fn reduce<T, U>(
        &self,
        worker: fn(&Array4<T>, UtilsDimension) -> Array3<U>,
        dimension: UtilsDimension,
        n_cpus: usize,
        out_file: &Path,
        na_value: u64,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let ext = out_file.extension().and_then(OsStr::to_str).unwrap();

        let n_cpus: String = n_cpus.to_string();
        std::env::set_var("RAYON_NUM_THREADS", n_cpus);

        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = if ext != "vrt" {
            PathBuf::from(create_temp_file("vrt"))
        } else {
            out_file.to_path_buf()
        };

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .map(|raster_block: RasterBlock| -> PathBuf {
                    let id = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let block_data = self.read_block::<T>(id);
                    let result = worker(&block_data, dimension);
                    self.write_window3(id, result, &block_fn, na_value);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();

        mosaic(&collected, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");
        if ext != "vrt" {
            translate(&tmp_file, out_file).expect("Could not translate to geotiff");
            // remove all temp files
            fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
            collected
                .iter()
                .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
        } else {
            fs::rename(tmp_file, out_file).unwrap();
        }
    }

    #[allow(dead_code)]
    fn get_unique_values(&self) -> Vec<i32> {
        let block_to_process: Vec<usize> = (0..self.blocks.len()).collect();
        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        let handle = pool.install(|| {
            block_to_process.into_par_iter().map(|id| {
                let data = self.read_block::<i32>(id);

                let unique_values: HashSet<_> = data.into_iter().collect();

                unique_values
            })
        });
        let collected: Vec<_> = handle.collect();
        let mut unique: HashSet<_> = HashSet::new();
        for set in collected {
            unique.extend(&set);
        }
        let mut unique: Vec<_> = unique.iter().cloned().collect();
        unique.sort();
        unique
    }

    #[cfg(feature = "use_lightgbm")]
    pub fn lightgbm_fit_classifier(
        &self,
        training_data: &PathBuf,
        params: Value,
        class_column: &str,
    ) {
        let data = self.extract_blockwise(training_data, "id", SamplingMethod::Value, None);
        let class = get_class(training_data, "id", class_column);

        let y: Vec<f32> = class.values().map(|v| *v as f32).collect();
        let x: Vec<Vec<f64>> = data
            .into_values()
            .map(|v| v.iter().map(|v| *v as f64).collect())
            .collect();
        let train_dataset = LgbmDataset::from_vec_of_vec(x.clone(), y, true).unwrap();

        let booster = Booster::train(train_dataset, &params).unwrap();

        let _ = booster.save_file("test.mod");
    }

    #[cfg(feature = "use_lightgbm")]
    pub fn lightgbm_predict(&self, out_file: &Path) {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(1)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));
        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .map(|raster_block: RasterBlock| -> PathBuf {
                    let booster = Booster::from_file("test.mod").unwrap();
                    let id = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let block_data = self.read_block::<f64>(id);
                    let features = block_data
                        .t()
                        .into_shape((
                            block_data.shape()[2] * block_data.shape()[3],
                            block_data.shape()[1],
                        ))
                        .unwrap();
                    
                    let mut features_vec = Vec::new();
                    for row in features.rows() {
                        features_vec.push(row.to_vec());
                    }
                    let predictions = booster.predict_from_vec_of_vec(features_vec, true).unwrap();
                    let predictions_class: Vec<u8> =
                        predictions.iter().map(|p| argmax(p) as u8).collect();

                    
                    let result = Array::from_shape_vec(
                        (block_data.shape()[2] * block_data.shape()[3], 1),
                        predictions_class,
                    )
                    .unwrap();
                    raster_block.write_samples_feature(&result, &block_fn, 0);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        let epsg_code = self.metadata.epsg_code;
        mosaic(&collected, &tmp_file, epsg_code).expect("Could not mosaic to vrt");
        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    /// Applays a worker function over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.

    pub fn reduce_row_pixel<T>(
        &self,
        worker: fn(&Array4<T>, UtilsDimension) -> Array2<T>,
        na_value: u64,
        dimension: UtilsDimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let n_cpus: String = n_cpus.to_string();
        std::env::set_var("RAYON_NUM_THREADS", n_cpus);

        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .map(|raster_block: RasterBlock| -> PathBuf {
                    let id = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let block_data = self.read_block::<T>(id);
                    let result = worker(&block_data, dimension);
                    raster_block.write_samples_feature(&result, &block_fn, na_value);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");

        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    #[doc(hidden)]
    pub fn write_window3<T>(&self, block_index: usize, data: Array3<T>, out_fn: &PathBuf, na_value: u64)
    where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + Debug,
    {
        //
        let trimmed = trimm_array3(&data, self.blocks[block_index].overlap_size);

        // create and empty raster with the right size
        let block_geotransform = self.blocks[block_index].geo_transform;
        let block_size: BlockSize = BlockSize {
            // rows: self.blocks[block_index].read_window.size.rows as usize,
            // cols: self.blocks[block_index].read_window.size.cols as usize,
            rows: trimmed.shape()[1],
            cols: trimmed.shape()[2],
        };
        let dataset_options: DatasetOptions = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            allowed_drivers: None,
            open_options: None,
            sibling_files: None,
        };
        let n_bands = data.shape()[0] as isize;
        let epsg_code = self.metadata.epsg_code;
        
        raster_from_size::<T>(out_fn, block_geotransform, epsg_code, block_size, n_bands, na_value as f64);
        let out_ds = Dataset::open_ex(out_fn, dataset_options).unwrap();
        for band in 0..data.shape()[0] {
            let b = (band + 1) as isize;
            let mut out_band = out_ds.rasterband(b as usize).unwrap();                       //out_band.set_no_data_value_u64(Some(na_value)).unwrap();
            let data_vec: Vec<T> = trimmed
                .slice(s![band, .., ..])
                .into_iter()
                .copied() 
                .collect();
            let mut data_buffer = Buffer::new((block_size.cols, block_size.rows), data_vec);
            


            out_band
                .write((0, 0), (block_size.cols, block_size.rows), &mut data_buffer)
                .unwrap();
        }
    }

    pub fn reduce_with_mask<T, U>(
        &self,
        other: &RasterDataset,
        worker: fn(&Array4<T>, &Array4<U>, UtilsDimension) -> Array3<T>,
        dimension: UtilsDimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        // todo!: Using first ds as a templete. This should be actually saved as mrd attributes!

        let n_cpus: String = n_cpus.to_string();
        std::env::set_var("RAYON_NUM_THREADS", n_cpus);

        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .zip(other.blocks.to_owned().into_par_iter())
                .map(|(raster_block_data, _raster_block_mask)| -> PathBuf {
                    let id = raster_block_data.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let block_data = self.read_block::<T>(id);
                    let mask = other.read_block::<U>(id);
                    let result = worker(&block_data, &mask, dimension);
                    raster_block_data.write3(result, &block_fn);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");

        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    pub fn reduce_row_pixel_with_mask<T, U>(
        &self,
        other: &RasterDataset,
        worker: fn(&Array4<T>, &Array4<U>, UtilsDimension) -> Array2<T>,
        na_value: u64, 
        dimension: UtilsDimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        // todo!: Using first ds as a templete. This should be actually saved as mrd attributes!

        let n_cpus: String = n_cpus.to_string();
        std::env::set_var("RAYON_NUM_THREADS", n_cpus);

        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .zip(other.blocks.to_owned().into_par_iter())
                .map(|(raster_block_data, _raster_block_mask)| -> PathBuf {
                    let id = raster_block_data.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!(
                        "{}_{}.tif",
                        file_stem, raster_block_data.block_index
                    ));
                    let block_data = self.read_block::<T>(id);
                    let mask = other.read_block::<U>(id);
                    let result = worker(&block_data, &mask, dimension);
                    raster_block_data.write_samples_feature(&result, &block_fn, na_value);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file, self.metadata.epsg_code).expect("Could not mosaic to vrt");

        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    pub fn geoms_to_global_indices(
        &self,
        geoms: BTreeMap<i64, Vec<(f64, f64, f64)>>,
    ) -> BTreeMap<i64, Index2d> {
        let idx_global: BTreeMap<_, _> = geoms
            .par_iter()
            .map(|(pid, p)| {
                let point: Coordinates = Coordinates {
                    x: p[0].0,
                    y: p[0].1,
                };
                (*pid, self.geo_to_global_rc(point))
            })
            .collect();
        idx_global
    }

    fn geo_to_global_rc(&self, point: Coordinates) -> Index2d {
        let gt = self.metadata.geo_transform.to_array();
        let row = ((point.y - gt[3]) / gt[5]) as usize;
        let col = ((point.x - gt[0]) / gt[1]) as usize;
        Index2d { col, row }
    }
    pub fn block_id_rowcol(&self, pid: i64, index: Index2d) -> (usize, (i64, Index2d)) {
        // find the block id for a global index. Returns block_id, (point_id, row_col)
        let id = self.id_from_indices(index);
        let row_col = self.global_rc_to_block_rc(index);
        (id, (pid, row_col))
    }

    fn global_rc_to_block_rc(&self, global_index: Index2d) -> Index2d {
        let mut block_col = global_index.col % self.metadata.block_size.cols;
        let mut block_row = global_index.row % self.metadata.block_size.rows;

        let block_col_ov = block_col + self.metadata.overlap_size;
        let block_row_ov = block_row + self.metadata.overlap_size;

        if (global_index.col as i16 - block_col_ov as i16) > 0 {
            block_col = block_col_ov;
        };

        if global_index.row as i16 - block_row_ov as i16 > 0 {
            block_row = block_row_ov;
        };

        Index2d {
            col: block_col,
            row: block_row,
        }
    }

    fn id_from_indices(&self, index: Index2d) -> usize {
        let n_block_cols = self.n_block_cols();
        (index.col / self.metadata.block_size.cols)
            + (index.row / self.metadata.block_size.rows) * n_block_cols
    }
    fn n_block_cols(&self) -> usize {
        let cols = self.metadata.shape.cols;
        let block_size = self.metadata.block_size;
        round::ceil(cols as f64 / block_size.cols as f64, 0) as usize
    }

    /// Stack RasterDataset along a dimension.
    ///
    /// # Example:
    /// ```
    /// use std::path::PathBuf;
    /// use eorst::{RasterDatasetBuilder,DataSourceBuilder, utils::Dimension};
    /// let src_fn = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif");
    /// let source_1 = DataSourceBuilder::from_file(&src_fn).build();
    /// let source_2 = source_1.clone();
    /// let mut rds_1 = RasterDatasetBuilder::from_source(&source_1)
    ///     .build();    // [1,6,rows,cols]
    /// let rds_2 = RasterDatasetBuilder::from_source(&source_2)
    ///    .build();    // [1,6,rows,cols]
    /// rds_1.stack(&rds_2, Dimension::Layer);
    ///
    /// let block_id = 0;
    /// let data = rds_1.read_block::<i32>(block_id);
    /// assert_eq!(data.shape(), [1, 12, 1024, 1024]);
    /// ```
    ///
    /// **Note**: this is an efficent way to combine data of **same data type**; if you need to use RasterDatasets with
    /// different datatype using zip would be better. Consider using [RasterDataset::reduce_with_mask] or [RasterDataset::reduce_row_pixel_with_mask]

    pub fn stack(
        &mut self,
        other: &RasterDataset,
        dimension_to_stack: UtilsDimension,
    ) -> &mut RasterDataset {
        match dimension_to_stack {
            UtilsDimension::Layer => {
                // make sure both have same times
                if self.metadata.date_indices != other.metadata.date_indices {
                    panic!(
                        "To stack datasets over Layers, both datasets have to the same time indics"
                    )
                }
                self.metadata
                    .layer_indices
                    .extend_from_slice(&other.metadata.layer_indices)
            }
            UtilsDimension::Time => {
                // make sure both have the same layers
                if self.metadata.layer_indices != other.metadata.layer_indices {
                    panic!("To stack datasets over Time, both have to have the same layers.")
                }
                self.metadata
                    .date_indices
                    .extend_from_slice(&other.metadata.date_indices);
            }
        }
        
        let axis = dimension_to_stack.get_axis();
        
        let max_dim_self = self.metadata.shape[axis];
        self.metadata
            .shape
            .stack(other.metadata.shape, dimension_to_stack);
        //Add layers
        for other_layer in &other.metadata.layers {
            let mut layer = other_layer.clone();
            layer.stack_position(other_layer.to_owned(), dimension_to_stack, max_dim_self);
            self.metadata.layers.push(layer);
        }

        self
    }

    /// Extend a RasterDataset
    ///
    /// Example:
    ///
    ///
    /// ```rust
    ///
    /// use std::path::PathBuf;
    /// use eorst::{DataSource, RasterDatasetBuilder,DataSourceBuilder, utils::Dimension};
    ///
    /// let source = DataSource {
    ///     source: PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"),
    ///     bands: [1, 2, 3, 4, 5, 6].to_vec(),
    ///     };
    ///let source_2 = source.clone();
    ///let mut rds_1 = RasterDatasetBuilder::from_source(&source)
    ///     .band_composition_dimension(Dimension::Layer)
    ///     .build();  // [1,6,rows,cols]
    ///let rds_2 = RasterDatasetBuilder::from_source(&source_2)
    ///     .band_composition_dimension(Dimension::Layer)
    ///     .build();  // [1,6,rows,cols]
    /// rds_1.extend(&rds_2);
    /// let block_id = 0;
    /// let data = rds_1.read_block::<i32>(block_id);
    /// assert_eq!(data.shape(), [2, 6, 1024, 1024]);
    /// ```
    /// **Note**: this is an efficent way to combine data of **same data type**; if you need to use RasterDatasets with
    /// different datatype using zip would be better. Consider using [RasterDataset::reduce_with_mask] or [RasterDataset::reduce_row_pixel_with_mask]
    pub fn extend(&mut self, other: &RasterDataset) -> &mut RasterDataset {
        self.metadata.shape.extend(other.metadata.shape);
        //Add layers
        for layer in &other.metadata.layers {
            self.metadata.layers.push(layer.to_owned());
        }
        self
    }

    /// Samples a RasterDataset with a vector of points.
    ///
    /// The returned value can be either the Value of the pixel if using `SamplingMethod::Value` or the Mode
    /// of n (buffer_size*buffer_suze) pixels surrounding the points is using `SamplingMethod::Mode`.
    ///
    /// Only blocks intersectibg with points to be sampled will be read.
    ///
    /// # Example:
    ///
    /// ```rust
    /// use std::path::PathBuf;
    /// use std::collections::BTreeMap;
    /// use eorst::{RasterDatasetBuilder,DataSourceBuilder, utils::{Dimension, SamplingMethod}};
    ///
    /// let raster_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif");
    /// let vector_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.gpkg");
    /// let data_source = DataSourceBuilder::from_file(&raster_path)
    ///          .bands(vec![1])
    ///          .build();
    /// let rds = RasterDatasetBuilder::from_source(&data_source)
    ///          .build();
    /// let data = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);
    ///
    /// let mut expected: BTreeMap<i16, Vec<i16>> = BTreeMap::new();
    /// expected.insert(1, vec![1910]);
    /// expected.insert(2, vec![957]);
    /// expected.insert(3, vec![1680]);
    /// expected.insert(4, vec![1847]);
    /// expected.insert(5, vec![1746]);
    /// expected.insert(6, vec![624]);
    /// expected.insert(7, vec![818]);
    /// expected.insert(8, vec![1103]);
    ///
    /// assert_eq!(expected, data);
    /// ```
    ///

    // pub fn zonal_histograms(
    //     &self,
    //     other: &RasterDataset,
    //     hist_bins: usize,
    //     hist_min: f32,
    //     hist_max: f32,

    // )
    // -> Result<polars::prelude::DataFrame>
    // {
    //     let mut zones = Vec::new();
    //     let mut times = Vec::new();
    //     let mut layers = Vec::new();
    //     let mut bin_starts = Vec::new();
    //     let mut bin_ends = Vec::new();
    //     let mut counts = Vec::new();

    //     let raw_histograms = self.zonal_histograms_raw(other , hist_bins, hist_min, hist_max);
    //     for (z, histogram) in &raw_histograms {
    //         for col_histogram in histogram {
    //             for hist in col_histogram.1.iter(){
    //                 let tl: Vec<&str> = col_histogram.0.split("-").collect();
    //                 let t = tl[0];
    //                 let l = tl[1];
    //                 zones.push(*z as i32);
    //                 times.push(t);
    //                 layers.push(l);
    //                 bin_starts.push(hist.bin.start());
    //                 bin_ends.push(hist.bin.end());
    //                 counts.push(*hist.value as i32);
    //             }
    //         }
    //     }
    //     let zones = Series::new("zone", &zones);
    //     let times = Series::new("time", &times);
    //     let layers = Series::new("layer", &layers);
    //     let bin_starts = Series::new("bin_star", &bin_starts);
    //     let bin_ends= Series::new("bin_end", &bin_ends);
    //     let counts = Series::new("count", &counts);

    //     let df = DataFrame::new(vec![zones,times, layers, bin_starts, bin_ends, counts])?;
    //     Ok(df)

    // }
    
    
    #[cfg(feature = "use_polars")]
    pub fn zonal_histograms_raster(
        &self,
        other: &RasterDataset,
        hist_bins: usize,
        hist_min: f32,
        hist_max: f32,
    ) -> Result<polars::prelude::DataFrame> {
        let blocks_to_process: Vec<usize> = (0..self.blocks.len()).collect();

        let col_names = self.column_names();
        
        let mut zones_unique = other.get_unique_values();
        let na = 0;
        zones_unique.retain(|&x| x != na);
        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        let handle = pool.install(|| {
            blocks_to_process.into_par_iter()
                .map(
                    |id|
                  -> PathBuf
                {
                    let data = self.read_block::<i16>(id);
                    let zones = other.read_block::<i32>(id);
                    // create a histogram hashmap to store the result
                    let mut histograms_thread = HashMap::new();
                    // create the axes that all the histograms will have
                    let axes_thread = UniformNoFlow::new(hist_bins, hist_min, hist_max);
                    
                    let mut idx = 0;
                    let mut hm = HashMap::new();
                    // Initialize the histogram for each zone and current column.``
                    for column in col_names.iter(){
                            for zone in zones_unique.iter() {
                                let h = ndhistogram!(axes_thread.clone().unwrap());
                                hm.insert(column.clone(), h);
                                // key: zone; value: empty_histogram
                                histograms_thread.insert(zone, hm.clone());
                            };
                        
                    }

                    
                    for time in 0..data.shape()[0] {
                        let data: ArrayView3<_> = data.index_axis(Axis(0), time);
                        for layer in 0..data.shape()[0] {
                            let data: ArrayView2<_> = data.index_axis(Axis(0), layer);                           
                            let column = col_names[idx].clone();
                            idx +=1;

                            if (zones.shape()[0] != 1) | (zones.shape()[1] != 1) {
                                    panic!("Zones raster dataset can only have 1 time and 1 layer");
                                }
                            let zones: ArrayView3<_> = zones.index_axis(Axis(0), 0);
                            let zones: ArrayView2<_> = zones.index_axis(Axis(0), 0);
                                                            // loop trough pixels and update the histogram

                            azip!(( d in &data, z in &zones ) {
                                let zone = *z;
                                if zone != na {
                                let msg = format!("Zone: {zone:?} not found");
                                let histogram = histograms_thread.get_mut(&zone).expect(&msg);
                                let histogram_time_layer = histogram.get_mut(&column).expect("key not available");
                                let data = *d as f32;
                                histogram_time_layer.fill(&data);
                                }
                                    
                             }
                          );
                                
                        }
                    }
                    let id = Uuid::new_v4();
                    // this should be a random filename
                    let histogram_fn =  PathBuf::from(&format!("eorst_{id:?}_{}.parquet", id));
                    // let mut histogram_file = std::fs::File::create(&histogram_fn).unwrap();
                    let mut zones = Vec::new();
                    let mut times = Vec::new();
                    let mut layers = Vec::new();
                    let mut bin_starts = Vec::new();
                    let mut bin_ends = Vec::new();
                    let mut counts = Vec::new();
        
       
        for (z, histogram) in &histograms_thread {
            for col_histogram in histogram {
                for hist in col_histogram.1.iter(){
                    let tl: Vec<&str> = col_histogram.0.split("--").collect();
                    let t = tl[0];
                    let l = tl[1];
                    zones.push(**z as i64);
                    times.push(t);
                    layers.push(l);
                    bin_starts.push(hist.bin.start());
                    bin_ends.push(hist.bin.end());
                    counts.push(*hist.value as i64);
                }
            }
        }
                       
        let zones = Series::new("zone".into(), &zones);
        let times = Series::new("time".into(), &times);
        let layers = Series::new("layer".into(), &layers);
        let bin_starts = Series::new("bin_star".into(), &bin_starts);
        let bin_ends= Series::new("bin_end".into(), &bin_ends);
        let counts = Series::new("count".into(), &counts);
        let vec_series = vec![zones, times, layers, bin_starts, bin_ends, counts];
        let vec_columns: Vec<polars::prelude::Column> = vec_series.iter().map(|s| Column::Series(s.clone().into())).collect();
        
        
                        {
                            let mut df = DataFrame::new(vec_columns).unwrap();
                            
                             save_zonal_histograms(&mut df, &histogram_fn );
                            
                        }                       
        histogram_fn
        })
        });

        let collected_histograms: Vec<_> = handle.collect();
        let df = merge_histogram_dfs(collected_histograms)?;
        Ok(df)
    }

    
    #[cfg(feature = "use_polars")]
    pub fn zonal_histograms_polygons(
        &self,
        polygons: &str,
        hist_bins: usize,
        hist_min: f32,
        hist_max: f32,
    ) 
    -> Result<polars::prelude::DataFrame> 
    {
        
        let blocks_to_process: Vec<usize> = (0..self.blocks.len()).collect();
        let col_names = self.column_names();
        
        
        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        let handle = pool.install(|| {
             blocks_to_process.into_par_iter()
                 .map(
                     |id|
                   -> PathBuf
                 {
                    // rasterize the zones. //
                    let block = self.blocks[id].clone();
                    let target_gt = block.geo_transform;
                    let rows = block.read_window.size.rows;
                    let cols = block.read_window.size.cols;
                    let x_ll = target_gt.x_ul + (cols as f64 * target_gt.x_res);
                    let y_ll = target_gt.y_ul + (rows as f64 * target_gt.y_res);
                    
                    let extent = Extent {
                                    xmin: (target_gt.x_ul * 100.).round() / 100.,
                                    ymin: (y_ll * 100.).round() / 100.,
                                    xmax: (x_ll * 100.).round() / 100.,
                                    ymax: (target_gt.y_ul * 100.).round() / 100.,
                                };
                    let rasterized_fn = format!("rasterized_block_{id:?}.tif");
                    let mut cmd = Command::new("gdal_rasterize");
                    cmd.arg("-q");
                    cmd.args([
                       "-tr",
                       &format!("{}", self.metadata.geo_transform.x_res),
                      &format!("{}", self.metadata.geo_transform.y_res),
                    ]);
                    cmd.args(["-a", "id"]);
    cmd.args([
        "-te",
        &format!("{}", (extent.xmin * 100.).round() / 100.),
        &format!("{}", (extent.ymin * 100.).round() / 100.),
        &format!("{}", (extent.xmax * 100.).round() / 100.),
        &format!("{}", (extent.ymax * 100.).round() / 100.),
    ]);
                        cmd.arg(polygons);
                        cmd.arg(rasterized_fn.clone());
   cmd.spawn()
        .expect("failed to start creating raster")
        .wait()
        .expect("failed to wait for the raster");
                    //
                     let data = self.read_block::<i16>(id);
                     let zones_ds = Dataset::open(rasterized_fn).unwrap();
                     let zones_band = zones_ds.rasterband(1).unwrap();
                     let window = (0,0);
                     let window_size = (cols as usize, rows as usize);
                     let array_size = window_size;
                     let e_resample_alg = None;
                     let zones = zones_band
                            .read_as::<i16>(window, window_size, array_size, e_resample_alg).unwrap().to_array().unwrap();
                     let mut zones_unique: HashSet<_> = zones.clone().into_iter().collect();
                        
                     let na = 0;
                     zones_unique.retain(|&x| x != na);
                        
                        
                    // create a histogram hashmap to store the result
                    let mut histograms_thread = HashMap::new();
                    // create the axes that all the histograms will have
                    let axes_thread = UniformNoFlow::new(hist_bins, hist_min, hist_max);
                    
                    let mut idx = 0;
                    let mut hm = HashMap::new();
                    // initialize the histogram for each zone and current column.``
                    for column in col_names.iter(){
                            for zone in zones_unique.iter() {
                                let h = ndhistogram!(axes_thread.clone().unwrap());
                                hm.insert(column.clone(), h);
                                // key: zone; value: empty_histogram
                                histograms_thread.insert(zone, hm.clone());
                            };
                    }
                        
                    for time in 0..data.shape()[0] {
                            
                        let data: ArrayView3<_> = data.index_axis(Axis(0), time);
                            
                        
                        for layer in 0..data.shape()[0] {
                                
                            let column = col_names[idx].clone();
                            idx +=1;
                            let data: ArrayView2<_> = data.index_axis(Axis(0), layer);                           
                                
                            // loop trough pixels and update the histogram
                            
                            azip!(( d in &data, z in &zones ) {
                                let zone = *z;
                                if zone != na {
                                   let msg = format!("Zone: {zone:?} not found");
                                   let histogram = histograms_thread.get_mut(&zone).expect(&msg);
                                   let histogram_time_layer = histogram.get_mut(&column).expect("key not available");
                                   let data = *d as f32;
                                   histogram_time_layer.fill(&data);
                                }
                             }
                          );
                                
                        }
                            
                    }
                     
                    let uid = Uuid::new_v4();
                        
                    let histogram_fn =  PathBuf::from(&format!("{}_{}.parquet", uid, id));
                        
                    let mut zones = Vec::new();
                    let mut times = Vec::new();
                    let mut layers = Vec::new();
                    let mut bin_starts = Vec::new();
                    let mut bin_ends = Vec::new();
                    let mut counts = Vec::new();
        
       
        for (z, histogram) in &histograms_thread {
            for col_histogram in histogram {
                for hist in col_histogram.1.iter(){
                    let tl: Vec<&str> = col_histogram.0.split("--").collect();
                    let t = tl[0];
                    let l = tl[1];
                    zones.push(**z as i64);
                    times.push(t);
                    layers.push(l);
                    bin_starts.push(hist.bin.start());
                    bin_ends.push(hist.bin.end());
                    counts.push(*hist.value as i64);
                }
            }
        }
                       
        let zones = Series::new("zone".into(), &zones);
        let times = Series::new("time".into(), &times);
        let layers = Series::new("layer".into(), &layers);
        let bin_starts = Series::new("bin_star".into(), &bin_starts);
        let bin_ends= Series::new("bin_end".into(), &bin_ends);
        let counts = Series::new("count".into(), &counts);
     let vec_series = vec![zones, times, layers, bin_starts, bin_ends, counts];
     
        let vec_columns: Vec<polars::prelude::Column> = vec_series.iter().map(|s| Column::Series(s.clone().into())).collect();

        
                        {
                            let mut df = DataFrame::new(vec_columns).unwrap();
                            
                             save_zonal_histograms(&mut df, &histogram_fn );
                            
                        }    
                        
        histogram_fn             
                    }
                )
            }
            );
        let collected_histograms :Vec<PathBuf> = handle.collect();
        let df = merge_histogram_dfs(collected_histograms)?;
        
        Ok(df)
        
    }
    

    pub fn extract_blockwise(
        &self,
        vector_path: &PathBuf,
        id_col_name: &str,
        method: SamplingMethod,
        buffer_size: Option<usize>,
    ) -> BTreeMap<i16, Vec<i16>> {
        debug!("Starting extract.");
        let buffer_size = buffer_size.unwrap_or(0);
        assert!(
            buffer_size <= self.metadata.overlap_size,
            "Buffer size has to be > overlap size"
        );

        assert!(
            self.metadata.shape.times == 1,
            "RasterDatasets with Time component nor supported yet"
        );

        let vector_dataset = Dataset::open(Path::new(vector_path)).unwrap();
        let mut layer = vector_dataset.layer(0).unwrap();
        let mut geoms = BTreeMap::new();

        for feature in layer.features() {
            let mut geom = Vec::new();
            feature.geometry().expect("Geometries").get_points(&mut geom);
            let field_index = feature.field_index(&id_col_name).expect("Bad column name.");
            let pid_filed = feature.field(field_index).unwrap().unwrap();
            let pid = pid_filed.into_int64().unwrap();
            geoms.insert(pid, geom);
        }
        // Find global index for each point. BtreeMap<point_id, global_index>
        let idx_global = self.geoms_to_global_indices(geoms);

        let id_indices: Vec<(usize, (i64, Index2d))> = idx_global
            .par_iter()
            .map(|(pid, index)| self.block_id_rowcol(*pid, *index))
            .collect();

        let block_ids: Vec<_> = idx_global
            .par_iter()
            .map(|(_, index)| self.id_from_indices(*index))
            .collect();

        let blocks_to_process: Vec<_> = block_ids.iter().unique().collect();
        

        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        let handle = pool.install(|| {
            blocks_to_process
                .into_par_iter()
                .map(|id| -> (Vec<usize>, Vec<usize>, Vec<Vec<i16>>) {
                    //id is block id, not point id
                    let mut pos: Vec<Index2d> = Vec::new();
                    let mut idx: Vec<usize> = Vec::new();
                    let mut pids: Vec<usize> = Vec::new();
                    for (pid, p) in id_indices.iter().enumerate() {
                        if p.0 == *id {
                            pos.push(Index2d {
                                col: p.1 .1.col,
                                row: p.1 .1.row,
                            });
                            pids.push(pid);
                            idx.push(p.1 .0 as usize);
                        };
                    }

                    let mut res = Vec::new();
                    let rect: Rectangle = Rectangle {
                        left: buffer_size,
                        top: buffer_size,
                        right: buffer_size,
                        bottom: buffer_size,
                    };

                    // let block_attributes = self.block_from_id(*id);
                    // let read_window = block_attributes.read_window;
                    // let overlap = block_attributes.overlap;
                    let data = self.read_block(*id);
                    
                    let bands = data.shape()[1];
                    for band_n in 0..bands {
                        let mut res_band = Vec::new();
                        let band_data = data.slice(s![0_i32, band_n, .., ..]);
                        for point in pos.iter() {
                            let point_shifted = Index2d {
                                col: point.col, //- self.blocks[*id].overlap.left,
                                row: point.row, // - self.blocks[*id].overlap.top,
                                      };
                            match method {
                                SamplingMethod::Mode => {
                                    let mut window_data: Vec<i16> =
                                        rect_view(&band_data, rect, point_shifted)
                                            .iter()
                                            .copied()
                                            .collect();
                                    window_data.sort_by(|a, b| a.partial_cmp(b).unwrap());
                                    
                                    let median = window_data[window_data.len() / 2];
                                    res_band.push(median);
                                }
                                SamplingMethod::Min => {
                                    let window_data: Vec<i16> =
                                        rect_view(&band_data, rect, point_shifted)
                                            .iter()
                                            .copied()
                                            .collect();
                                    let min = window_data.iter().min().unwrap().to_owned();
                                    res_band.push(min);
                                }

                                SamplingMethod::Value => {
                                    let d = band_data[(point_shifted.row, point_shifted.col)];
                                    res_band.push(d);
                                }
                            }
                        }
                        res.push(res_band);
                    }
                    (pids, idx, res)
                })
        });

        let collected: Vec<_> = handle.collect();
        let pids: Vec<_> = collected.iter().map(|(pid, _, _)| pid).collect();
        let vals: Vec<_> = collected.iter().map(|(_, _, vals)| vals).collect();
        let idxs: Vec<_> = collected.iter().map(|(_, idx, _)| idx).collect();
        let mut results = BTreeMap::new();

        let num_bands = vals[0].len();
        let num_blocks = pids.len();
        for block in 0..num_blocks {
            for i in 0..pids[block].len() {
                let mut vals_point: Vec<i16> = Vec::new();
                let id = idxs[block][i];
                for band in 0..num_bands {
                    vals_point.push(vals[block][band][i]);
                }
                let mut res_point = BTreeMap::new();
                res_point.insert(id as i16, vals_point);
                results.append(&mut res_point);
            }
        }

        results
    }

    pub fn extract(
        &self,
        geometries: &[Geometry],
        point_ids: &[i64],
        method: SamplingMethod,
        buffer_size: Option<usize>,
    ) -> Result<(Array2<i16>, Vec<i64>)> {
        let buffer_size = buffer_size.unwrap_or(0);
        assert!(
            buffer_size <= self.metadata.overlap_size,
            "Buffer size has to be > overlap size"
        );
        let mut geoms = BTreeMap::new();
        for (idx, point_id) in point_ids.iter().enumerate() {
            let geometry = &geometries[idx];

            //let geometry_ref = geometry.get_geometry(0);
           // debug!("geometry_ref {:?}", geometry_ref);
            let point = geometry.get_point(0);

            let (x, y, z) = point;
            let p_vec = vec![(x,y,z)];
            geoms.insert(*point_id, p_vec);
        }
        // Find global index (rows and cold in the full dataset) for each point. BtreeMap<point_id, global_index>
        let idx_global = self.geoms_to_global_indices(geoms.clone());
        // Find in what block the global index will be
        let id_indices: Vec<(usize, (i64, Index2d))> = idx_global
            .par_iter()
            .map(|(pid, index)| self.block_id_rowcol(*pid, *index))
            .collect();
        
        // Find the relative index for each point (i.e row and col realive to the block)
        let block_ids: Vec<_> = idx_global
            .par_iter()
            .map(|(_, index)| self.id_from_indices(*index))
            .collect();

        let blocks_to_process: Vec<_> = block_ids.iter().unique().collect();
        // will go only trough all the block required to sample the points.
        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        let handle = pool.install(|| {
            blocks_to_process
                .into_par_iter()
                // .filter(|id| **id == 8)
                .map(|id| -> (Vec<usize>, Vec<usize>, Vec<Vec<i16>>) {
                    //id is block id, not point: id
                    let mut pos: Vec<Index2d> = Vec::new();
                    let mut idx: Vec<usize> = Vec::new();
                    let mut pids: Vec<usize> = Vec::new();
                    for (pid, p) in id_indices.iter().enumerate() {
                        if p.0 == *id {
                            pos.push(Index2d {
                                col: p.1 .1.col,
                                row: p.1 .1.row,
                            });
                            pids.push(pid);
                            idx.push(p.1 .0 as usize);
                        };
                    }

                    let mut res = Vec::new();
                    let rect: Rectangle = Rectangle {
                        left: buffer_size,
                        top: buffer_size,
                        right: buffer_size,
                        bottom: buffer_size,
                    };

                    let data = self.read_block(*id);
                    let n_times = data.shape()[0];
                    let n_layers = data.shape()[1];
                    // println!("id {idx:?}");
                    for time in 0..n_times {
                        //println!("time: {time:?}");
                        for layer in 0..n_layers {
                            //println!("layer {layer:?}");
                            let mut res_band = Vec::new();
                            let band_data = data.slice(s![time, layer, .., ..]);
                            for point in pos.iter() {
                                
                                let col = point.col.checked_sub(self.blocks[*id].overlap.left);
                                let row = point.row.checked_sub(self.blocks[*id].overlap.top);

                                let col = match col {
                                    Some(x) => x,
                                    _ => point.col
                                };
                                let row = match row {
                                    Some(y) => y,
                                    _ => point.row
                                };

                                let point_shifted = Index2d {
                                    
                                col, row
                                };
                                
                                match method {
                                    SamplingMethod::Mode => {
                                        let mut window_data: Vec<i16> =
                                        rect_view(&band_data, rect, point_shifted)
                                        .iter()
                                        .copied()
                                        .collect();
                                        if window_data.is_empty() {
                                            info!("Hey!");
                                            info!("point {point:?}");
                                            info!("point shifted {point_shifted:?}");
                                            info!("rect  {rect:?}");
                                            info!("band_data  \n{band_data:?}");


                                            };
                                        window_data.sort_by(|a, b| a.partial_cmp(b).unwrap());
                                        // info!("window_data: {window_data:?}");
                                        let median = window_data[window_data.len() / 2];
                                        // info!("median: {median:?}");

                                        res_band.push(median);
                                    }
                                    SamplingMethod::Min => {
                                        let window_data: Vec<i16> =
                                            rect_view(&band_data, rect, point_shifted)
                                                .iter()
                                                .copied()
                                                .collect();
                                        let min = window_data.iter().min().unwrap().to_owned();
                                        res_band.push(min);
                                    }

                                    SamplingMethod::Value => {
                                        let d = band_data[(point_shifted.row, point_shifted.col)];
                                        // println!("sampled_values {d:?}");
                                        res_band.push(d);
                                    }
                                }
                            }
                            res.push(res_band);
                        }
                    }

                    (pids, idx, res)
                })
        });

        let collected: Vec<_> = handle.collect();
        let pids: Vec<_> = collected.iter().map(|(pid, _, _)| pid).collect();
        let idxs: Vec<_> = collected.iter().map(|(_, idx, _)| idx).collect();
        let vals: Vec<_> = collected.iter().map(|(_, _, vals)| vals).collect();
        let mut results = BTreeMap::new();

        let num_bands = vals[0].len();
        let num_blocks = pids.len();
        for block in 0..num_blocks {
            for i in 0..pids[block].len() {
                let mut vals_point: Vec<i16> = Vec::new();
                let id = idxs[block][i];
                for band in 0..num_bands {
                    vals_point.push(vals[block][band][i]);
                }
                let mut res_point = BTreeMap::new();
                res_point.insert(id as i64, vals_point);
                results.append(&mut res_point);
            }
        }
        let k = results.keys().next().unwrap();
        let n_rows = results.len();
        let n_cols = results[k].len();
        //use ndarray::Array;
        // Create an empty Array2 with the specified shape
        let mut array: Array2<i16> = Array::zeros((n_rows, n_cols));
        // Populate the array with values from the HashMap
        for (row_index, values) in results.values().enumerate() {
            for (col_index, value) in values.iter().enumerate() {
                array[[row_index, col_index]] = *value;
            }
        }
        let pids: Vec<i64> = results.into_keys().collect();
        Ok((array, pids))
    }

    /// Create names like time0_layer0, time0_layer1, time1_layer0, etc.
    pub fn column_names(&self) -> Vec<String> {
        let mut col_names = Vec::new();
        for time in &self.metadata.date_indices {
            for layer in &self.metadata.layer_indices {
                let time_str = match time {
                    DateType::Date(date) => format!("{}", date.format("%Y-%m-%d %H:%M:%S UTC%Z")),

                    DateType::Index(index) => format!("time_{}", index),
                };
                let c = format!("{time_str}--{layer}");
                col_names.push(c);
            }
        }
        col_names
    }
    #[cfg(feature = "use_polars")]
    pub fn extract_blockwise_time( 
        &self,
        vector_path: &PathBuf,
        id_col_name: &str,
        method: SamplingMethod,
        buffer_size: Option<usize>,
    ) -> polars::prelude::DataFrame {
        info!("Starting extract blockwise");
        let col_names = self.column_names();
        // let muts = Vec::new();
        // for time in &self.metadata.date_indices {
        //     for layer in &self.metadata.layer_indices {
        //         col_names.push(format!("{time}_{layer}"));
        //     }
        // }

        // {id_1: [val_1, val_2], id_2: [val_1, val_2]  }
        // Get the geometries of the sampling dataset
        let vector_dataset = Dataset::open(Path::new(vector_path)).unwrap();

        let mut layer = vector_dataset.layer(0).unwrap();
        let mut geometries: Vec<Geometry> = Vec::new();
        let mut point_ids: Vec<i64> = Vec::new();



        for feature in layer.features() {
            geometries.push(
                feature
                    .geometry()
                    .expect("Unable to get geometry")
                    .to_owned(),
            );
            let id_col_idx = feature.field_index(id_col_name).expect("Bad column name");
            let pid_field = feature.field(id_col_idx).unwrap().unwrap();
            let pid_field = pid_field.into_int64().unwrap();
            point_ids.push(pid_field);
        }

        let (array, pids) = self
            .extract(&geometries, &point_ids, method, buffer_size)
            .unwrap();


        // Create the dataframe with the sampled valies
        let vec_series = array
                .axis_iter(ndarray::Axis(1))
                .enumerate()
                .map(|(i, col)| {
                    let column_name = col_names[i].to_string();
                    let values: Vec<i16> = col.to_vec();
                    Series::new((&column_name).into(), values)
                })
                .collect::<Vec<Series>>();
            
        let vec_columns: Vec<polars::prelude::Column> = vec_series.iter().map(|s| Column::Series(s.clone().into())).collect();

        let mut sampled: DataFrame = DataFrame::new(
            vec_columns
        )
        .unwrap();
        let ids = Series::new(id_col_name.into(), pids);
        sampled.with_column(ids).unwrap();
        debug!("Sampled \n{:?}", sampled);
        sampled
    }

    #[doc(hidden)]
    pub fn read_block<T>(&self, block_id: usize) -> RasterData<T>
    where
        T: gdal::raster::GdalType + Copy + std::fmt::Debug + num_traits::identities::Zero,
    {
        // the shape of the out array can be constructed
        // of the two first indices in shelf.shape and the size
        // attribute of the read window.
        let block = &self.blocks[block_id];
        let read_window = block.read_window;
        
        let rows = read_window.size.rows as usize; // + overlap.top + overlap.bottom;
        let cols = read_window.size.cols as usize; //+ overlap.left + overlap.right;
        let data_shape = (
            self.metadata.shape.times,
            self.metadata.shape.layers,
            rows,
            cols,
        );
        
        let mut data: RasterData<T> = RasterData::zeros(data_shape);
        
        for layer in &self.metadata.layers {
            let raster_path = Path::new(&layer.source);
            
            let band_index: isize = 1;
            let ds = Dataset::open(raster_path).unwrap();
            let raster_band = ds.rasterband(band_index as usize).unwrap();
            let window = (read_window.offset.cols, read_window.offset.rows);
            let window_size = (cols, rows);
            let array_size = window_size;
            let e_resample_alg = None;
            
            
            let layer_data = raster_band
                .read_as::<T>(window, window_size, array_size, e_resample_alg)
                .unwrap().to_array().unwrap();
            
            

            let slice = s![
                layer.time_pos,
                layer.layer_pos,
                ..,
                .. //overlap.top..read_window.size.rows as usize,
                   //overlap.left..read_window.size.cols as usize,
            ];

            data.slice_mut(slice).assign(&layer_data);

            // if overlap.top > 0 {
            //     let new_slice = s![.., .., block.overlap_size..2 * block.overlap_size, ..];
            //     let new_data = data.slice(new_slice).to_owned();
            //     let data_slice = s![.., .., 0..block.overlap_size, ..];
            //     data.slice_mut(data_slice).assign(&new_data);
            // }
            // if overlap.bottom > 0 {
            //     let max_row = data.shape()[2];
            //     let new_slice = s![
            //         ..,
            //         ..,
            //         max_row - 2 * block.overlap_size..max_row - block.overlap_size,
            //         ..
            //     ];
            //     let new_data = data.slice(new_slice).to_owned();
            //     let data_slice = s![.., .., max_row - block.overlap_size..max_row, ..];
            //     data.slice_mut(data_slice).assign(&new_data);
            // }

            // if overlap.left > 0 {
            //     let new_slice = s![.., .., .., block.overlap_size..2 * block.overlap_size];
            //     let new_data = data.slice(new_slice).to_owned();
            //     let data_slice = s![.., .., .., 0..block.overlap_size];
            //     data.slice_mut(data_slice).assign(&new_data);
            // }

            // if overlap.right > 0 {
            //     let max_col = data.shape()[3];
            //     let new_slice = s![
            //         ..,
            //         ..,
            //         ..,
            //         max_col - 2 * block.overlap_size..max_col - block.overlap_size
            //     ];
            //     let new_data = data.slice(new_slice).to_owned();
            //     let data_slice = s![.., .., .., max_col - block.overlap_size..max_col];
            //     data.slice_mut(data_slice).assign(&new_data);
            //}
        }
        data
    }
}

impl<'a> Iterator for RasterDatasetIter<'a> {
    type Item = Self;

    fn next(&mut self) -> Option<Self::Item> {
        if self.iter_index >= self.parent.blocks.len() {
            None
        } else {
            let item = RasterDatasetIter {
                parent: self.parent,
                block: &self.parent.blocks[self.iter_index],
                iter_index: self.iter_index,
            };
            self.iter_index += 1;
            Some(item)
        }
    }
}

impl<'a> IntoIterator for &'a RasterDatasetIter<'_> {
    type Item = RasterDatasetIter<'a>;
    type IntoIter = RasterDatasetIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        RasterDatasetIter {
            parent: self.parent,
            block: &self.parent.blocks[self.iter_index],
            iter_index: self.iter_index,
        }
    }
}


// // Define a trait with the erode method
// pub trait Erode {
//    fn erode(&self, slice: ndarray::SliceInfo<[ndarray::SliceOrIndex; 3], Ix3, Ix3>) ;

// }
// // Implement the trait for RasterBlockSlice
// impl<'a, T> Erode for RasterBlockSlice<'a, T> {
//     fn erode(&self, slice: Slice) {
//         // Implement your erosion logic here
//         println!("Eroding slice");
//     }
// }


pub type RasterBlockSlice2<T> = Array2<T>;
pub type RasterBlockSlice3<T> = Array3<T>;


// Define a trait with the erode method

// #[cfg(feature = "use_opencv")]
// use crate::utils::{arrayview2_to_mat};




#[cfg(feature = "use_opencv")]
use opencv::imgproc;
#[cfg(feature = "use_opencv")]
use opencv::core;



pub trait Filters<T> {
    fn erode(&self, size: usize) -> Array2<T>;
    fn dilate(&self, size: usize) -> Array2<T>;
    fn median_blur(&self, size: usize) -> Array2<T>;
}

#[cfg(feature = "use_opencv")]
impl<T> Filters<T> for RasterBlockSlice2<T>
where
     T: std::fmt::Debug + opencv::prelude::DataType + gdal::raster::GdalType+ num_traits::Zero + 'static,
 
{
    fn erode(&self, size: usize) -> Array2<T>{
        let data = arrayview2_to_mat::<T>(self.view()).unwrap();
        let mut result = core::Mat::default();
        let kernel =
          imgproc::get_structuring_element(imgproc::MORPH_RECT, core::Size::new(size as i32, size as i32), core::Point::new(-1, -1))
             .unwrap();
        
        imgproc::erode(
                 &data,
                 &mut result,
                 &kernel,
                 core::Point::new(-1, -1),
                 1,
                 core::BORDER_CONSTANT,
                 core::Scalar::all(0.0),
             )
             .unwrap();
             
        mat_to_array2::<T>(&result).unwrap()
    }
    fn dilate(&self, size: usize) -> Array2<T> {
        
        
        let data = arrayview2_to_mat::<T>(self.view()).unwrap();
        let mut result = core::Mat::default();
        
        
        imgproc::median_blur(
                 &data,
                 &mut result,
                 size as i32,
             )
             .unwrap();
        mat_to_array2::<T>(&result).unwrap()
    }
  fn median_blur(&self, size: usize) -> Array2<T>{
      
        let data = arrayview2_to_mat::<T>(self.view()).unwrap();
        let mut result = core::Mat::default();
        imgproc::median_blur(&data, &mut result, size.try_into().unwrap()).unwrap();
        mat_to_array2::<T>(&result).unwrap()
    }
    
    
    
}



impl RasterBlock {
    
    pub fn into_frc<T>(&self, data: &Array2<T>) -> Array3<T> 
    where 
    T: gdal::raster::GdalType + Clone + num_traits::identities::Zero + std::fmt::Debug
    {
        let n_features = data.shape()[1];
        let n_rows = self.read_window.size.rows as usize - self.overlap.top - self.overlap.bottom;
        let n_cols = self.read_window.size.cols as usize; // - self.overlap.left - self.overlap.right;
        let mut result: Array3::<T> = Array3::zeros((n_features, n_rows, n_cols));
        for n_feature in 0..n_features {
            let feature = data.slice(s![.., n_feature]);
            let feature = feature.into_shape((n_rows, n_cols)).unwrap();
            feature.assign_to(result.slice_mut(s![n_feature,..,..]))
        }
        
        result
    }
    
    pub fn write_samples_feature<T>(&self, data: &Array2<T>, file_name: &PathBuf, na: u64)
    where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + Debug,
    {
        let n_bands = data.shape()[1] as isize;
        let block_geotransform = self.geo_transform;
        let epsg_code = self.epsg_code;
        let size_x = self.read_window.size.cols as usize - self.overlap.left - self.overlap.right;
        let size_y = self.read_window.size.rows as usize - self.overlap.top - self.overlap.bottom;
        let block_size: BlockSize = BlockSize {
            rows: size_y,
            cols: size_x,
        };
        
        raster_from_size::<T>(file_name, block_geotransform, epsg_code as u32, block_size, n_bands, na as f64);
        let ds_options = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            ..DatasetOptions::default()
        };
        let out_ds = Dataset::open_ex(Path::new(&file_name), ds_options).unwrap();

        // each feature goes to a band
        for feature in 1..n_bands + 1 {
            let mut out_band = out_ds.rasterband(feature as usize).unwrap();
           // out_band.set_no_data_value_u64(Some(na)).unwrap();
            let out_data = data.slice(s![.., feature - 1]);
            let out_data_u8: Vec<T> = out_data.into_iter().map(|v| *v as T).collect();
            let mut data_buffer = Buffer::new((block_size.cols, block_size.rows), out_data_u8);
            

            out_band
                .write((0, 0), (size_x, size_y), &mut data_buffer)
                .unwrap();
        }
    }

    pub fn write3<T>(&self, data: Array3<T>, out_fn: &PathBuf)
    where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + Debug,
    {
        // create and empty raster with the right size
        let block_geotransform = self.geo_transform;
        let block_size: BlockSize = BlockSize {
            rows: self.read_window.size.rows as usize,
            cols: self.read_window.size.cols as usize,
        };
        let dataset_options: DatasetOptions = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            allowed_drivers: None,
            open_options: None,
            sibling_files: None,
        };
        let n_bands = data.shape()[0] as isize;
        let epsg_code = self.epsg_code;
        let na_value = 0.;
        raster_from_size::<T>(
            out_fn,
            block_geotransform,
            epsg_code.try_into().unwrap(),
            block_size,
            n_bands,
            na_value
        );
        let out_ds = Dataset::open_ex(out_fn, dataset_options).unwrap();
        for band in 0..data.shape()[0] {
            let b = (band + 1) as isize;
            let mut out_band = out_ds.rasterband(b as usize).unwrap();
            let data_vec: Vec<T> = data.slice(s![band, .., ..]).into_iter().copied().collect();
            
            let mut data_buffer = Buffer::new((block_size.cols, block_size.rows), data_vec);
            

            out_band
                .write((0, 0), (block_size.cols, block_size.rows), &mut data_buffer)
                .unwrap();
        }
    }
}
fn rasterize_and_write<T>(raster_block: &RasterBlock, geometries: &Vec<Geometry>, burn_values: Vec<T>, rasterize_options: Option<RasterizeOptions>, out_fn: &PathBuf) -> Result<()>
 where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + num_traits::ToPrimitive + Debug, // raster type
 
{
    
    let block_geotransform = raster_block.geo_transform;
        let block_size: BlockSize = BlockSize {
            rows: raster_block.read_window.size.rows as usize,
            cols: raster_block.read_window.size.cols as usize,
        };
        let dataset_options: DatasetOptions = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            allowed_drivers: None,
            open_options: None,
            sibling_files: None,
        };
        let epsg_code = raster_block.epsg_code;
        let n_bands = 1;
        let na_value = 255.;
        raster_from_size::<T>(
            out_fn,
            block_geotransform,
            epsg_code.try_into().unwrap(),
            block_size,
            n_bands,
            na_value
        );
        let mut out_ds = Dataset::open_ex(out_fn, dataset_options).unwrap();
            // Convert burn_values from &[T] to Vec<f64]
    let burn_values_f64: Vec<f64> = burn_values
        .iter()
        .map(|&value| value.to_f64().expect("Conversion to f64 failed"))
        .collect();
    


    // let geometries_: Vec<_> = geometries
    //     .iter()
    //     .map(|geo| unsafe { geo.c_geometry() })
    //     .collect();
    // println!("Geometries: {:?}", geometries_);
    // for g in geometries_ {
    //     let mut z = Geometry::empty(6).unwrap();
    //     unsafe {z.set_c_geometry(g)};
        
        
        
    // }

    
    
        gdal::raster::rasterize(&mut out_ds, &[1], &geometries, burn_values_f64.as_slice(), rasterize_options ).unwrap();
        Ok(())
}

#[allow(dead_code)]
pub(crate) fn array2_to_nested_vec<T: std::clone::Clone>(arr: &Array2<T>) -> Vec<Vec<T>> {
    let mut res: Vec<Vec<T>> = Vec::new();
    arr.axis_iter(Axis(0))//
        .for_each(|row| res.push(row.to_vec()));
    res

}
#[allow(dead_code)]
pub fn get_class(
    dataset_path: &PathBuf,
    id_column: &str,
    class_column: &str,
) -> BTreeMap<i16, i16> {
    let dataset = Dataset::open(dataset_path).unwrap();
    let mut layer = dataset.layer(0).unwrap();

    let _fields_defn = layer
        .defn()
        .fields()
        .map(|field| (field.name(), field.field_type(), field.width()))
        .collect::<Vec<_>>();
    let mut class: BTreeMap<i16, i16> = BTreeMap::new();
    
    for feature in layer.features() {
        let id_column_idx = feature.field_index(id_column).expect("Bad column name");
        let class_column_idx = feature.field_index(class_column).expect("Bad column name");
        let id = feature
            .field(id_column_idx)
            .unwrap()
            .unwrap()
            .into_int()
            .unwrap();
        let condition = feature
            .field(class_column_idx)
            .unwrap()
            .unwrap_or(FieldValue::IntegerValue(-1))
            .into_int()
            .unwrap();
        class.insert(id as i16, condition as i16);
    }
    class
}

impl Layer {
    pub fn stack_position(
        &mut self,
        original_layer: Layer,
        dimension_to_stack: UtilsDimension,
        max_dim: usize,
    ) -> &mut Layer {
        match dimension_to_stack {
            UtilsDimension::Layer => self.layer_pos = original_layer.layer_pos + max_dim,
            UtilsDimension::Time => self.time_pos = original_layer.time_pos + max_dim,
        }
        self
    }
}



pub fn unique_datetimes_in_range(dates:  Vec<DateTime<FixedOffset>>) -> Vec<DateTime<FixedOffset>> {
    let six_hours = chrono::Duration::hours(6);
    let mut result: Vec<DateTime<FixedOffset>> = vec![];
    let mut last_date = dates[0];
    
    
    for &date in dates[1..].iter() {
        
        if date - last_date <= six_hours {
            
        } else {
            result.push(last_date);
            last_date = date;
        }
    }
    result.push(last_date); // push the last date
    
    result
}

pub fn get_sorted_datetimes(feature_collection: &ItemCollection) -> Vec<DateTime<FixedOffset>> {
    let mut dates: Vec<DateTime<FixedOffset>> = feature_collection
        .items
        .iter()
        .map(|i| DateTime::parse_from_rfc3339(&i.properties.datetime.to_owned().unwrap().to_rfc3339()).unwrap())
        .collect();
    dates.sort();
    
    
    dates

    
    
}

pub fn get_asset_names(feature_collection: &ItemCollection) -> Vec<String> {
    let mut names = Vec::new();
    for item in &feature_collection.items {
        for (_, asset) in item.to_owned().assets {
            
            let eo_bands = &asset.additional_fields["eo:bands"][0];
            let name = eo_bands["common_name"]
                .as_str()
                .unwrap_or_else(|| eo_bands["name"].as_str().unwrap())
                .to_owned();
            let name = if name == "None" {
                eo_bands["common_name"]
                    .as_str()
                    .unwrap_or_else(|| eo_bands["name"].as_str().unwrap())
                    .to_owned()
            } else {
                name
            };
            names.push(name);
        }
    }
    let mut names = names.into_iter().unique().collect::<Vec<String>>();
    names.sort();
    names
}

fn get_name_from_bands(bands: Option<&serde_json::value::Value>) -> Option<String> {
    if let Some(json_array) = bands {
        if let Some(json_object) = json_array.get(0) {
            // Some catalogs will store the layer name under common_name
            if let Some(name_field) = json_object.get("common_name") {
                if let Some(name) = name_field.as_str() {
                    return Some(name.to_owned());
                }
            }
            // and some others, simply under name; So if the above condition fails (no common_name) it falls back
            // to the name.
            if let Some(name_field) = json_object.get("name") {
                if let Some(name) = name_field.as_str() {
                    return Some(name.to_owned());
                }
            }
        }
    }
    None
}
pub fn get_sources_for_asset(
    items: &Vec<stac::Item>,
    asset_name: &str,
) -> Vec<PathBuf> {
    let mut sources = Vec::new();
    
    for item in items {
        for asset in item.assets.values() {
            let asset = asset.clone();
            let bands = asset.additional_fields.get("eo:bands");
            let mut names = Vec::new();
            if let Some(name) = get_name_from_bands(bands) {
                names.push(name);
            } else {
                panic!("Name not found.");
            }

            if names[0] == asset_name  {
                sources.push(PathBuf::from(asset.href));
            }
        }
        
    };
    sources
    
}

pub fn get_asset_href(
    feature_collection: &ItemCollection,
    date_time: &DateTime<FixedOffset>,
    asset_name: &str,
) -> PathBuf {
    let mut found_asset = Asset::new("test");
    for item in &feature_collection.items {
        let date =
            DateTime::parse_from_rfc3339(&item.properties.datetime.to_owned().unwrap().to_rfc3339()).unwrap();
        
        
        for asset in item.assets.values() {
            let asset = asset.clone();
            let bands = asset.additional_fields.get("eo:bands");
            let mut names = Vec::new();
            if let Some(name) = get_name_from_bands(bands) {
                names.push(name);
            } else {
                panic!("Name not found.");
            }

            if (names[0] == asset_name) && (date == *date_time) {
                found_asset = asset.clone();
            }
        }
    }
    PathBuf::from(found_asset.href)
}
pub fn get_items_for_date(
    feature_collection: &ItemCollection,
    date_time: &DateTime<FixedOffset>,
    
) -> Vec<stac::Item>{
    
    let mut found_items = Vec::new();
    
    for item in &feature_collection.items {
        let date =
 DateTime::parse_from_rfc3339(&item.properties.datetime.to_owned().unwrap().to_rfc3339()).unwrap();
        

        let delta = *date_time - date;
        
        
        if delta.abs() <= chrono::Duration::hours(24) {
            
            found_items.push(item.to_owned());
        }
    }
    found_items
    
}

//     let n_items = bands.len();
//     let layer_names = if bands[0].len() == 1 {
//     let n = feature_collection.items[0] //assuming all items will have the same assets
//         .assets
//         .iter()
//         .map(|(k, _)| k.to_owned())
//         .collect::<String>();
//     vec![ n; n_items ];
//     } else {

//         todo!()
//     };

//     let it = feature_collection.items.iter().zip(bands.iter());
//     let mut layers: Vec<Vec<String>> = it
//         .map(|(i, b)| {
//             let name = if b.len() == 1 {
//                 let n = i
//                     .assets
//                     .iter()
//                     .map(|(k, _)| k.to_owned())
//                     .collect::<String>();
//                 vec![vec![n]]
//             } else {
//                 let n = i
//                     .assets
//                     .iter()
//                     .map(|(k, _)| {
//                         b.iter().map(|bn| format!("{}_{}", k.to_owned(), bn) ).collect::<Vec<String>>()
//                         // vec!["1".to_string()]
//                     })
//                     .collect::<Vec<Vec<String>>>();
//                 n;
//                 vec![vec!["1".to_string()]
//             };
//             name
//         })
//         .collect();
//     todo!();
//         // layers.dedup();
//     // layers
// }

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum DateType {
    Date(DateTime<FixedOffset>),
    Index(usize),
}

fn get_overlap(
    image_size: ImageSize,
    block_size: BlockSize,
    block_col_row: (usize, usize),
    overlap_size: usize,
) -> Overlap {
    let mut overlap = Overlap {
        left: overlap_size,
        top: overlap_size,
        right: overlap_size,
        bottom: overlap_size,
    };
    let tile_col = block_col_row.0;
    let tile_row = block_col_row.1;
    let n_rows_tile = (image_size.rows + block_size.rows - 1) / block_size.rows;
    let n_cols_tile = (image_size.cols + block_size.cols - 1) / block_size.cols;
    let is_top = tile_row == 0;
    let is_bottom = tile_row + 1 == n_rows_tile;
    let is_left = tile_col == 0;
    let is_right = tile_col + 1 == n_cols_tile;
    if is_top {
        overlap.top = 0
    };
    if is_bottom {
        overlap.bottom = 0
    };
    if is_left {
        overlap.left = 0
    };
    if is_right {
        overlap.right = 0
    };
    overlap
    //change the logic. If it is in the top row, no upper overlap
    // if bottom row, no bottom overlap and so on.
}

#[allow(dead_code)]
#[cfg(feature = "use_polars")]
fn mean(hist: &VecHistogram<AxesTuple<(UniformNoFlow<f32>,)>, f64>) -> f64 {
    let sum = hist
        .values()
        .map(|it| {
            let x: f64 = *it;
            x
        })
        .fold(0.0, |it, value| it + value);

    let mut mean = 0.;
    for item in hist.iter() {
        let midpoint =
            item.bin.start().unwrap() + (item.bin.end().unwrap() - item.bin.start().unwrap()) / 2.;
        mean += midpoint as f64 * item.value;
    }
    mean / sum
}

pub fn swap_coordinates(gdal_geometry: &Geometry) -> Geometry {
    let wkt = gdal_geometry.wkt().unwrap();
    
    let mut swapped_wkt = String::new();
    swapped_wkt.push_str("POLYGON ((");
    let tokens: Vec<&str> = wkt.split(|c| c == ',' || c == '(' || c == ')').collect();  

    // Split the WKT string by spaces and parenthesesi
    for token in tokens {
        if !token.starts_with("POLY") {
            
        // Split each token by comma to get the coordinate values
        let coordinates: Vec<&str> = token.split(' ').collect();
        if coordinates.len() == 2 {
            // Swap the X and Y coordinates
            let x = coordinates[0].trim();
            let y = coordinates[1].trim();

            // Append the swapped coordinates to the new WKT string
            let to_append =  &format!("{} {} {}, ", y, x, 0);
                
            swapped_wkt.push_str(to_append);
        }
            
        }

        //swapped_wkt.push(','); // Add space between coordinates
    }
    swapped_wkt.push_str("))");
    let mut result = swapped_wkt;

    // Find the last occurrence of a comma
    if let Some(last_comma_index) = result.rfind(',') {
        // Remove the comma from the string
        result.remove(last_comma_index);
    }
    
    Geometry::from_wkt(&result).unwrap()
}

#[cfg(feature = "use_polars")]
fn merge_histogram_dfs(collected_histograms: Vec<PathBuf>) -> Result<DataFrame> {
       let mut df = DataFrame::default();
        for (idx, file) in collected_histograms.iter().enumerate() {
            let file = std::fs::File::open(file).unwrap();
            let reader = ParquetReader::new(file);
            let new = reader.finish().unwrap();
            if idx == 0 {
                df = new;
            } else {
                df.vstack_mut(&new).unwrap();
            };
        }

        let df = df
            .lazy()
            .group_by([
                col("zone"),
                col("time"),
                col("layer"),
                col("bin_star"),
                col("bin_end"),
            ])
            .agg([col("count").sum()])
            .collect()?;
    Ok(df)
}

fn is_last(handle_last: &[Vec<PathBuf>]) -> bool {
    
    
    handle_last.len() == 1
}


fn create_progress_bars(total_length: usize) -> (Bar, Bar) {
    let mut bar_read = tqdm!(
        desc = format!("{:20}", "Read blocks".blue()),
        total = total_length,
        position = 0,
        animation = "firacode",
        force_refresh = true
    );
    bar_read.colour = Some("blue".into());
    bar_read.refresh().unwrap();

    let mut bar_process = tqdm!(
        desc = format!("{:20}", "Processed blocks".green()),
        total = total_length,
        position = 1,
        animation = "firacode",
        force_refresh = true
    );
    bar_process.colour = Some("green".into());
    bar_process.refresh().unwrap();
    (bar_read, bar_process)
}

fn update_arc_mutex_progress_bar(pbrogres_bar: Arc<Mutex<Bar>>) {
    // Unwrap the Arc object
    let progress_bar = pbrogres_bar.as_ref();
    // lock the mutex to allow updating
    let mut progress_bar = progress_bar.lock().unwrap();
    // update progress bar
    progress_bar.update(1).unwrap();
}

fn block_col_row_(id: usize, image_size: ImageSize, block_size: BlockSize) -> (usize, usize)
{
        let block_row = id / n_block_cols_(image_size, block_size);
        let block_col = id - (block_row *n_block_cols_(image_size, block_size));

        (block_col, block_row)
    
}
fn n_block_cols_(image_size: ImageSize, block_size: BlockSize)->usize {
     round::ceil(image_size.cols as f64 / block_size.cols as f64, 0) as usize
}
fn get_block_gt_(read_window: ReadWindow, geo_transform: GeoTransform)-> GeoTransform{
        let x_ul_image = geo_transform.x_ul;
        let y_ul_image = geo_transform.y_ul;

        let x_res = geo_transform.x_res;
        let y_res = geo_transform.y_res;
        let x_pos = read_window.offset.cols;
        let y_pos = read_window.offset.rows;

        let x_ul_block = x_ul_image + x_res * x_pos as f64; // to fix! will break with overlap
        let y_ul_block = y_ul_image + y_res * y_pos as f64; // to fix! will break with overlap
        GeoTransform {
            x_ul: x_ul_block,
            x_res,
            x_rot: geo_transform.x_rot,
            y_ul: y_ul_block,
            y_rot: geo_transform.x_rot,
            y_res,
        }
    
}
fn block_from_id_(
        id: usize,
        block_shape: RasterDataShape,
        _layers: &[Layer],
        epsg_code: usize,
        image_size: ImageSize,
        block_size: BlockSize,
        overlap_size: usize,
        geo_transform: GeoTransform,
        )
     -> RasterBlock {
        let tile_col = block_col_row_(id, image_size, block_size).0;
        let tile_row = block_col_row_(id, image_size, block_size).1;
        let overlap = get_overlap(
            image_size,
            block_size,
            block_col_row_(id, image_size, block_size),
            overlap_size,
        );

        let ul_x = (block_size.cols * tile_col) as isize;
        let ul_y = (block_size.rows * tile_row) as isize;

        // now compute with overlap
        let ul_x_overlap = ul_x - overlap.left as isize;
        let ul_y_overlap = ul_y - overlap.top as isize;
        let lr_x_overlap = min(
            image_size.cols as isize,
            ul_x + block_size.cols as isize + overlap.right as isize,
        );
        
        let lr_y_overlap = min(
            image_size.rows as isize,
            ul_y + block_size.rows as isize + overlap.bottom as isize,
        );

        
        let win_width = lr_x_overlap - ul_x_overlap;
        let win_height = lr_y_overlap - ul_y_overlap;
        

        let arr_width = win_width;
        let arr_height = win_height;

        let read_window = ReadWindow {
            offset: Offset {
                cols: ul_x_overlap,
                rows: ul_y_overlap,
            },
            size: Size {
                cols: arr_width,
                rows: arr_height,
            },
        };

        let block_geo_transform = get_block_gt_(read_window, geo_transform);
        let empty_metadata: RasterMetadata = RasterMetadata::new(); 
        RasterBlock {
            block_index: id,
            read_window,
            overlap_size,
            geo_transform: block_geo_transform,
            overlap,
            shape: block_shape,
            epsg_code,
            raster_metadata: empty_metadata,
        }
    }

